/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T20:18:02
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <memory>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>
#include <sys/socket.h>

#include "Exception.hpp"
#include "networking.hpp"
#include "test_helpers.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestNetNetAddr : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetNetAddr);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(buffer);
    CPPUNIT_TEST(detach);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        net::Addr addr;
        EQ(false, addr.isValid());
        EQ(size_t{0}, addr.size());
        logger::notice() << addr;

        addr = net::Addr{"127.0.0.1", 1234};
        EQ(true, addr.isValid());
        EQ(std::string("127.0.0.1"), addr.host());
        EQ(uint16_t{1234}, addr.port());
        logger::notice() << addr;

        addr = net::Addr{"::1", 1235};
        EQ(true, addr.isValid());
        EQ(std::string("::1"), addr.host());
        EQ(uint16_t{1235}, addr.port());

        addr = net::Addr{"fe80::1", 1235};
        EQ(true, addr.isValid());
        EQ(std::string("fe80::1"), addr.host());
        EQ(uint16_t{1235}, addr.port());
        logger::notice() << addr;

        addr = net::Addr{"garbage", 1234};
        EQ(false, addr.isValid());
        EQ(true, addr.addr() == nullptr);
        EQ(uint16_t{0}, addr.port());
        EQ(std::string(), addr.host());

        addr = net::Addr{"127.garbage", 1234};
        EQ(false, addr.isValid());

        logger::notice() << net::Addr{"fe80::1", 4242};
    }

    void detach()
    {
        net::Addr addr;
        addr.detach();
        EQ(false, addr != net::Addr{});

        addr = net::Addr{"10.8.0.6", 6666};
        EQ(true, addr != net::Addr{});
        EQ(true, net::Addr{} != addr);

        net::Addr addr2 = addr;
        EQ(addr.addr(), addr2.addr());
        addr.detach();
        EQ(true, addr.addr() != addr2.addr());
        EQ(true, addr == addr2);
        reinterpret_cast<sockaddr_in *>(const_cast<sockaddr *>(addr.addr()))
            ->sin_addr.s_addr = htonl(INADDR_ANY);
        EQ(false, addr == addr2);

        addr = net::Addr{"fe80::1", 8888};
        addr2 = addr;
        EQ(addr.addr(), addr2.addr());
        addr.detach();
        EQ(true, addr.addr() != addr2.addr());
        EQ(true, addr == addr2);
        reinterpret_cast<sockaddr_in6 *>(const_cast<sockaddr *>(addr.addr()))
            ->sin6_addr = in6addr_any;
        EQ(false, addr == addr2);
    }

    void buffer()
    {
        buffer_view_t<cow_ptr<buffer_t>> buffer{make_cow<buffer_t>(4, 0)};
        buffer[0] = 127;
        buffer[3] = 1;
        {
            net::Addr addr{net::Addr::fromBuffer(buffer)};
            EQ(true, bool(addr));
            EQ(std::string("127.0.0.1"), addr.host());
            EQ(*make_const(buffer.buffer()), *addr.toBuffer());

            /* direct buffer constructor */
            EQ(true, addr == net::Addr(buffer));

            {
                net::Addr other("127.0.0.2"); // different but still IPv6
                const struct sockaddr *raw = other.addr();
                other = buffer; // explicit affectation
                EQ(true, addr == other);
                EQ(true, other.addr() == raw); // internal mem optimization

                other = net::Addr("fe80::2c00:1");
                raw = other.addr();
                other = buffer;
                EQ(true, addr == other);
                EQ(true, other.addr() != raw);
            }
        }

        buffer = make_cow<buffer_t>(16, 0);
        buffer[0] = 0xfe;
        buffer[1] = 0x80;
        buffer[12] = 44;
        {
            net::Addr addr{net::Addr::fromBuffer(buffer)};
            EQ(true, bool(addr));
            EQ(std::string("fe80::2c00:0"), addr.host());
            EQ(*make_const(buffer.buffer()), *addr.toBuffer());

            /* direct buffer constructor */
            EQ(true, addr == net::Addr(buffer));

            {
                net::Addr other("fe80::2c00:1"); // different but still IPv6
                const struct sockaddr *raw = other.addr();
                other = buffer; // explicit affectation
                EQ(true, addr == other);
                EQ(true, other.addr() == raw); // internal mem optimization

                other = net::Addr("127.0.0.2");
                raw = other.addr();
                other = buffer;
                EQ(true, addr == other);
                EQ(true, other.addr() != raw);
            }
        }

        /* invalid constructs */
        EQ(false, net::Addr(make_cow<buffer_t>()).isValid());
        EQ(false, net::Addr(make_cow<buffer_t>(3, 0)).isValid());
        {
            net::Addr addr{"127.0.0.1"};
            addr = make_cow<buffer_t>(3, 0);
            EQ(false, addr.isValid());
        }

        {
            buffer = make_cow<buffer_t>(3, 0);
            EQ(false, net::Addr("127.0.0.1").toBuffer(buffer));
            EQ(size_t(3), buffer.size());

            buffer = make_cow<buffer_t>(12, 0);
            EQ(false, net::Addr("fe80::2c00:1").toBuffer(buffer));
            EQ(false, net::Addr().toBuffer(buffer));
            EQ(size_t(12), buffer.size());

            EQ(false, bool(net::Addr().toBuffer()));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetNetAddr);
