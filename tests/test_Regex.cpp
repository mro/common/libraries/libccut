/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Regex.hpp"
#include "test_helpers.hpp"

using namespace ccut;

class TestRegex : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRegex);
    CPPUNIT_TEST(match);
    CPPUNIT_TEST(matchIterator);
    CPPUNIT_TEST(search);
    CPPUNIT_TEST(replace);
    CPPUNIT_TEST(replaceGroups);
    CPPUNIT_TEST(flags);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

public:
    void match()
    {
        Regex r("([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)");
        std::vector<std::string> out;
        EQ(true, r.match("1.2.3", out));
        EQ((std::vector<std::string>{"1.2.3", "1", "2", "3"}), out);

        /* capacity is taken in account */
        out = std::vector<std::string>();
        out.reserve(2);
        EQ(true, r.match("1.2.3", out));
        EQ((std::vector<std::string>{"1.2.3", "1"}), out);

        /* not matching */
        out = std::vector<std::string>(1, "test");
        EQ(false, r.match("test", out));
        EQ((std::vector<std::string>{}), out);
    }

    void matchIterator()
    {
        std::string str = "1 2 3 4 5 xxx 6test7 8 9";
        std::vector<std::string> matches;
        Regex r("[0-9]+");

        size_t count = 0;
        for (size_t offset = 0; r.match(str, offset, matches);
             offset += matches[0].size())
        {
            EQ(std::to_string(++count), matches[0]);
        }
        EQ(size_t(9), count);
    }

    void search()
    {
        EQ(true, Regex("([[:digit:]]+)").search("1234"));
        EQ(false, Regex("([[:digit:]]+)").search("test"));
    }

    void replace()
    {
        /* basic */
        EQ(std::string("0es3"),
           Regex("t").replace("test", [](size_t start, size_t) -> std::string {
               return std::string(1, '0' + (start % 10));
           }));

        /* no arguments */
        EQ(std::string("xesx"),
           Regex("t").replace("test", []() -> std::string { return "x"; }));
        EQ(std::string("txxt"),
           Regex("e|s").replace("test", []() -> std::string { return "x"; }));

        /* substring argument */
        EQ(std::string("<this> <is> <a> <test>"),
           Regex("\\w+").replace("this is a test",
                                 [](const std::string &sub) -> std::string {
                                     return "<" + sub + ">";
                                 }));

        /* no match */
        EQ(std::string("example"),
           Regex("xxxx").replace("example",
                                 []() -> std::string { return "xxx"; }));

        /* eol match */
        EQ(std::string("examplexxx"),
           Regex("$").replace("example", []() -> std::string { return "xxx"; }));

        /* empty match (special case) */
        EQ(std::string("_e_x_a_m_p_l_e"),
           Regex("").replace("example", []() -> std::string { return "_"; }));

        /* BOL match (must match only once) */
        EQ(std::string("_example"),
           Regex("^").replace("example", []() -> std::string { return "_"; }));
    }

    void replaceGroups()
    {
        Regex r("[$]\\{(\\w+)\\}|[$](\\w+)");
        auto replace_func = [](const Regex::MatchGroup &matches) -> std::string {
            return "<" + matches.at(1) + "_VALUE>";
        };

        EQ(std::string("echo <HOME_VALUE>"),
           r.replace("echo ${HOME}", replace_func));
        EQ(std::string("echo <HOME_VALUE><TEST_VALUE><TITI_VALUE> xxx"),
           r.replace("echo ${HOME}${TEST}$TITI xxx", replace_func));

        /* getenv example (as in doxygen) */
        Regex envRe("[$][{](\\w+)[}]|[$](\\w+)");
        const std::string ret = envRe.replace(
            "${HOME}$SHELL",
            [](const Regex::MatchGroup &matches) -> std::string {
                const char *value = getenv(matches[1].c_str());
                return value ? value : "";
            },
            2);
        ASSERT_MSG("failed to retrieve env variables: " + ret,
                   ret.find('$') == std::string::npos);
    }

    void flags()
    {
        EQ(false, Regex("test.test", Regex::NewLine).search("test\ntest"));
        EQ(true, Regex("test.test", 0).search("test\ntest"));

        EQ(false, Regex("[0-9]a", Regex::Extended).search("1A"));
        EQ(true, Regex("[0-9]a", Regex::Extended | Regex::ICase).search("1A"));
    }

    void error()
    {
        ASSERT_THROW(Regex("(test"), std::invalid_argument);
        ASSERT_THROW(Regex("([[:digit]]"), std::invalid_argument);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRegex);
