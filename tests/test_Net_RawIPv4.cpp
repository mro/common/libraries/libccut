/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-23T17:17:26
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <chrono>
#include <initializer_list>
#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "async.hpp"
#include "net/ICMP.hpp"
#include "net/icmp/ICMPMessage.hpp"
#include "net/raw.hpp"
#include "net/raw/RawIPMessage.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestNetRawIPv4 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetRawIPv4);
    CPPUNIT_TEST(message);
    CPPUNIT_TEST(invalidMessage);
    CPPUNIT_TEST(socket);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void message()
    {
        const cow_ptr<buffer_t> msg4{
            make_cow<buffer_t>(std::initializer_list<uint8_t>{
                /* ping 8.8.8.8 request */
                0x45, 0x00, 0x00, 0x54, 0x61, 0x98, 0x40, 0x00, 0x40, 0x01,
                0x06, 0xfc, 0xc0, 0xa8, 0x01, 0x5d, 0x08, 0x08, 0x08, 0x08,
                0x08, 0x00, 0xd8, 0x61, 0x00, 0x01, 0x00, 0x09, 0x08, 0x4a,
                0x8d, 0x65, 0x00, 0x00, 0x00, 0x00, 0xc3, 0x11, 0x08, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
                0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
                0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
                0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33,
                0x34, 0x35, 0x36, 0x37})};

        net::RawIPv4Message msg{net::RawIPv4Message::load(msg4)};
        EQ(true, bool(msg));
        EQ(msg.ipSrc(), net::Addr("192.168.1.93"));
        EQ(msg.ipDest(), net::Addr("8.8.8.8"));
        EQ(msg.ipSrc(), msg.from);
        EQ(msg.ipDest(), msg.to);

        EQ(uint8_t(4), msg.ipVersion());
        EQ(uint8_t(5), msg.ipHeaderLength());
        EQ(uint8_t(0), msg.differentiatedServicesCodePoint());
        EQ(uint8_t(0), msg.explicitCongestionNotification());
        EQ(uint16_t(84), msg.totalLength());
        EQ(uint16_t(0x6198), msg.ipIdentification());
        EQ(
            net::RawIPv4Message::IPFlags{
                net::RawIPv4Message::IPFlag::DontFragment},
            msg.ipFlags());
        EQ(uint16_t(0), msg.ipFragmentOffset());
        EQ(uint8_t(64), msg.timeToLive());
        EQ(net::Protocol::ICMP, msg.protocol());
        logger::notice("test") << msg.protocol();
        EQ(uint16_t(0x06fc), msg.ipHeaderChecksum());

        {
            /* rebuild packet from scratch and compare */
            net::RawIPv4Message test{net::Addr("192.168.1.93"),
                                     net::Addr("8.8.8.8"), net::Protocol::ICMP,
                                     64};
            msg.setIpFragmentOffset(0)
                .setTimeToLive(64)
                .setIpFlags(net::RawIPv4Message::IPFlags{
                    net::RawIPv4Message::IPFlag::DontFragment})
                .setIpIdentification(0x6198);
            msg.prepare();
            EQ(uint16_t(0x06fc), msg.ipHeaderChecksum());
            EQ(size_t(4), net::getRawIPVersion(test.data));
        }

        msg.data.reset();
        EQ(false, bool(msg));
    }

    void invalidMessage()
    {
        net::RawIPv4Message msg;
        EQ(false, msg.isValid());

        msg = net::RawIPv4Message{net::Addr{"127.0.0.1"},
                                  net::Addr{"127.0.0.1"}};
        msg.setProtocol(net::Protocol::ICMP);
        msg.prepare();
        EQ(true, msg.isValid());

        {
            net::RawIPv4Message test{msg};

            { /* invalid data size, accessors should throw */
                net::RawIPv4Message test{msg};
                test.data.buffer()->resize(net::RawIPv4Message::MinDataSize - 1);
                EQ(false, bool(test.data));
                EQ(false, test.isValid()); // data is not valid anymore

                test.data = test.data.buffer(); // reset data to new buffer
                                                // boundaries
                EQ(true, bool(test.data));
                EQ(false, test.isValid());

                ASSERT_THROW(test.ipVersion(), Exception);
                ASSERT_THROW(test.ipHeaderLength(), Exception);
                ASSERT_THROW(test.differentiatedServicesCodePoint(), Exception);
                ASSERT_THROW(test.explicitCongestionNotification(), Exception);
                ASSERT_THROW(test.totalLength(), Exception);
                ASSERT_THROW(test.ipIdentification(), Exception);
                ASSERT_THROW(test.ipFlags(), Exception);
                ASSERT_THROW(test.ipFragmentOffset(), Exception);
                ASSERT_THROW(test.timeToLive(), Exception);
                ASSERT_THROW(test.protocol(), Exception);
                ASSERT_THROW(test.ipHeaderChecksum(), Exception);
                ASSERT_THROW(test.ipSrc(), Exception);
                ASSERT_THROW(test.ipDest(), Exception);
            }

            { /* invalid ip version */
                net::RawIPv4Message test{msg};
                test.data[0] = 0x55;
                test.prepare();
                EQ(false, test.isValid());
            }

            { /* invalid checksum */
                net::RawIPv4Message test{msg};
                test.data[2] = 0x55;
                EQ(false, test.isValid());
                test.prepare();
                EQ(true, test.isValid());
            }
        }
    }

    void socket()
    {
        net::RawIPv4Socket server1(net::Protocol::ICMP);
        if (!tryBind(server1))
            return;

        server1.start();
        EQ(false, server1.addr().isV6());

        std::shared_ptr<std::promise<std::string>> prom{
            std::make_shared<std::promise<std::string>>()};

        Connection conn = server1.message.connect(
            [prom](const net::RawIPv4Socket::Message &msg) mutable {
                net::ICMPEchoMessage reply{msg}; // copies from/to
                reply.data = msg.payload();

                if (reply &&
                    (reply.type() == net::ICMPMessage::Type::EchoReply) && prom)
                {
                    // use a const object to not trigger the cow
                    const net::ICMPEchoMessage::data_t payload{reply.payload()};
                    prom->set_value(std::string{payload.begin(), payload.end()});
                    prom.reset(); // reset the prom to not resolve it twice
                                  // (note the mutable on lambda)
                }
            });

        const std::string testStr{"test"};
        net::RawIPv4Message msg(server1.addr(), server1.addr(),
                                net::RawIPv4Message::Protocol::ICMP,
                                net::ICMPMessage::MinDataSize + testStr.size());
        msg.prepare(); // will write from/to in header
        EQ(msg.ipSrc(), net::Addr("127.0.0.1", 0));
        EQ(msg.ipDest(), net::Addr("127.0.0.1", 0));

        {
            net::ICMPEchoMessage icmp;
            icmp.data = msg.payload();
            icmp.data.buffer().setCopyOnWrite(false);

            icmp.setCode(net::ICMPMessage::Code::Echo);
            icmp.setIdentifier(0x42);
            icmp.setSequenceNum(0x12);

            net::ICMPEchoMessage::data_t data{icmp.payload()};
            data.buffer().setCopyOnWrite(false);
            std::copy(testStr.begin(), testStr.end(), data.data());
            icmp.prepare();
        }
        server1.send(msg);

        /* promise should resolve with message from server1 */
        EQ(testStr,
           ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void logger()
    {
        logger::notice("test")
            << "Protocols: " << net::Protocol::ICMP << ", "
            << net::Protocol::IGMP << ", " << net::Protocol::IPinIP << ", "
            << net::Protocol::TCP << ", " << net::Protocol::IGP << ", "
            << net::Protocol::UDP << ", " << net::Protocol::RDP << ", "
            << net::Protocol::IPv6 << ", " << net::Protocol::RAW << ", "
            << net::Protocol::ICMPv6 << ", "
            << static_cast<net::Protocol>(0x42);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetRawIPv4);