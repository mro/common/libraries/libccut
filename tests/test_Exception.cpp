/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-07T13:38:03+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cerrno>
#include <cstring>
#include <map>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "test_helpers.hpp"

using namespace ccut;
static const std::map<std::error_code, std::string> s_errorsMessages = {
    {ErrorCode::Interrupted, ::strerror(EINTR)},
    {ErrorCode::InvalidArguments, ::strerror(EINVAL)},
    {ErrorCode::InternalError, "Internal error"}};

class TestException : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestException);
    CPPUNIT_TEST(category);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST_SUITE_END();

public:
    void category()
    {
        Exception ex(ErrorCode::InvalidArguments);

        EQ(std::string("CCUT"), Exception::Category);
        EQ(Exception::Category,
           std::string(ex.getErrorCode().category().name()));

        EQ(Exception::Category,
           std::string(
               make_error_code(ErrorCode::Interrupted).category().name()));
    }

    void errors()
    {
        for (auto const &em : s_errorsMessages)
        {
            bool threwException = false;
            try
            {
                throw Exception(em.first);
            }
            catch (const Exception &e)
            {
                threwException = true;
                EQ(std::string(em.second), std::string(e.what()));
            }
            ASSERT_MSG("Did not throw exception", threwException);
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestException);
