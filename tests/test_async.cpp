/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-17T21:48:14+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include <chrono>
#include <stdexcept>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "async.hpp"
#include "test_helpers.hpp"

using namespace ccut;
using namespace std::chrono;

class TestAsync : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAsync);
    CPPUNIT_TEST(wait);
    CPPUNIT_TEST(makeFutureError);
    CPPUNIT_TEST(try_get);
    CPPUNIT_TEST_SUITE_END();

public:
    void wait()
    {
        { // lvalue
            std::future<int> f = std::async(std::launch::async,
                                            []() { return 42; });
            EQ(42, ccut::wait(f));
        }

        { // rvalue
            std::promise<int> p;
            ASSERT_THROW(ccut::wait(p.get_future(), milliseconds(10)),
                         std::runtime_error);
        }

        { // lvalue
            std::future<void> f = std::async(std::launch::async, []() {});
            ccut::wait(f);
        }

        {
            std::promise<void> p;
            ASSERT_THROW(ccut::wait(p.get_future(), milliseconds(10)),
                         std::runtime_error);
        }
    }

    void makeFutureError()
    {
        {
            std::future<int> f = ccut::make_future_error<int>(
                std::runtime_error("test"));
            ASSERT_THROW(ccut::wait(f), std::runtime_error);
        }

        {
            std::future<void> f = ccut::make_future_error(
                std::runtime_error("test"));
            ASSERT_THROW(ccut::wait(f), std::runtime_error);
        }
    }

    void try_get()
    {
        {
            std::promise<int> p;

            p.set_value(42);
            EQ(int{42}, ccut::try_get(p.get_future(), -1));

            std::promise<void> pv;
            pv.set_value();
            EQ(true, ccut::try_get(pv.get_future()));
        }

        {
            std::future<int> f{
                ccut::make_future_error<int>(std::runtime_error("test"))};
            EQ(int{-1}, ccut::try_get(f, -1));

            std::future<void> fv{
                ccut::make_future_error<void>(std::runtime_error("test"))};
            EQ(false, ccut::try_get(fv));
        }

        {
            std::promise<int> p;
            EQ(int{-1},
               ccut::try_get(p.get_future(), -1, std::chrono::milliseconds(10),
                             "failed to get promise value"));

            std::promise<void> pv;
            EQ(false,
               ccut::try_get(pv.get_future(), std::chrono::milliseconds(10)));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestAsync);