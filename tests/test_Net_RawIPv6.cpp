/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-29T08:32:52
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <chrono>
#include <initializer_list>
#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "async.hpp"
#include "net/ICMPv6.hpp"
#include "net/raw.hpp"
#include "net/raw/RawIPMessage.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestNetRawIPv6 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetRawIPv6);
    CPPUNIT_TEST(message);
    CPPUNIT_TEST(invalidMessage);
    CPPUNIT_TEST(socket);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void message()
    {
        const cow_ptr<buffer_t> dump{
            make_cow<buffer_t>(std::initializer_list<uint8_t>{
                /* ping fe80::468a:5bff:fe29:fcdd request */
                0x60, 0x0f, 0x7c, 0xb9, 0x00, 0x40, 0x3a, 0x40, 0xfe, 0x80,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46, 0x8a, 0x5b, 0xff,
                0xfe, 0x29, 0xfc, 0xdd, 0xfe, 0x80, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x46, 0x8a, 0x5b, 0xff, 0xfe, 0x29, 0xfc, 0xdd,
                0x80, 0x00, 0xb1, 0xf3, 0x00, 0x04, 0x00, 0x48, 0x1f, 0x77,
                0x8e, 0x65, 0x00, 0x00, 0x00, 0x00, 0x26, 0x71, 0x02, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
                0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
                0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
                0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33,
                0x34, 0x35, 0x36, 0x37

            })};

        net::RawIPv6Message msg{net::RawIPv6Message::load(dump)};
        EQ(true, bool(msg));
        EQ(msg.ipSrc(), net::Addr("fe80::468a:5bff:fe29:fcdd"));
        EQ(msg.ipDest(), net::Addr("fe80::468a:5bff:fe29:fcdd"));
        EQ(msg.ipSrc(), msg.from);
        EQ(msg.ipDest(), msg.to);

        EQ(uint8_t(6), msg.ipVersion());
        EQ(net::RawIPv6Message::ECN::NoECT,
           msg.explicitCongestionNotification());
        EQ(net::RawIPv6Message::DSCP::DefaultForwarding,
           msg.differentiatedServicesCodePoint());
        EQ(uint32_t(0xf7cb9), msg.flowLabel());
        EQ(uint16_t(64), msg.payloadLength());
        EQ(net::Protocol::ICMPv6, msg.nextHeader());
        EQ(uint8_t(0), msg.trafficClass());
        EQ(uint8_t(64), msg.hopLimit());

        {
            /* rebuild packet from scratch and compare */
            net::RawIPv6Message test{net::Addr("fe80::468a:5bff:fe29:fcdd"),
                                     net::Addr("fe80::468a:5bff:fe29:fcdd"),
                                     net::Protocol::ICMPv6, 64};
            test.setExplicitCongestionNotification(
                    net::RawIPv6Message::ECN::NoECT)
                .setDifferentiatedServicesCodePoint(
                    net::RawIPv6Message::DSCP::DefaultForwarding)
                .setFlowLabel(0xf7cb9)
                .setHopLimit(64);
            EQ(size_t(6), net::getRawIPVersion(test.data));

            std::copy(msg.payload().cbegin(), msg.payload().cend(),
                      test.data.begin() + net::RawIPv6Message::MinDataSize);
            test.prepare();
            EQ(msg.data, test.data);
        }

        msg.setExplicitCongestionNotification(
               net::RawIPv6Message::ECN::CongestionExperienced)
            .setDifferentiatedServicesCodePoint(net::RawIPv6Message::DSCP::AF41)
            .setFlowLabel(0x4242)
            .setHopLimit(0xFF)
            .setNextHeader(net::Protocol::TCP);
        EQ(net::RawIPv6Message::ECN::CE, msg.explicitCongestionNotification());
        EQ(net::RawIPv6Message::DSCP::AF41,
           msg.differentiatedServicesCodePoint());
        EQ(uint32_t(0x4242), msg.flowLabel());
        EQ(net::Protocol::TCP, msg.nextHeader());
        EQ(uint8_t(0xFF), msg.hopLimit());

        msg.data.reset();
        EQ(false, bool(msg));
    }

    void invalidMessage()
    {
        net::RawIPv6Message msg;
        EQ(false, msg.isValid());

        msg = net::RawIPv6Message{net::Addr{"fe80::1"}, net::Addr{"fe80::1"}};
        msg.setNextHeader(net::Protocol::ICMP);
        msg.prepare();
        EQ(true, msg.isValid());

        {
            net::RawIPv6Message test{msg};

            { /* invalid data size, accessors should throw */
                net::RawIPv6Message test{msg};
                test.data.buffer()->resize(net::RawIPv6Message::MinDataSize - 1);
                EQ(false, bool(test.data));
                EQ(false, test.isValid()); // data is not valid anymore

                test.data = test.data.buffer(); // reset data to new buffer
                                                // boundaries
                EQ(true, bool(test.data));
                EQ(false, test.isValid());

                ASSERT_THROW(test.ipVersion(), Exception);
                ASSERT_THROW(test.trafficClass(), Exception);
                ASSERT_THROW(test.differentiatedServicesCodePoint(), Exception);
                ASSERT_THROW(test.explicitCongestionNotification(), Exception);
                ASSERT_THROW(test.flowLabel(), Exception);
                ASSERT_THROW(test.payloadLength(), Exception);
                ASSERT_THROW(test.nextHeader(), Exception);
                ASSERT_THROW(test.hopLimit(), Exception);
                ASSERT_THROW(test.ipSrc(), Exception);
                ASSERT_THROW(test.ipDest(), Exception);
            }

            { /* invalid ip version */
                net::RawIPv6Message test{msg};
                test.data[0] = 0x55;
                test.prepare();
                EQ(false, test.isValid());
            }

            { /* can't set ipv4 address in ipv6 message */
                net::RawIPv6Message test{msg};
                test.setIpSrc(net::Addr{"fe80::1"});
                test.setIpSrc(net::Addr()); // valid, clear the address
                ASSERT_THROW(test.setIpSrc(net::Addr{"127.0.0.1"}), Exception);

                test.setIpDest(net::Addr{"fe80::1"});
                test.setIpDest(net::Addr());
                ASSERT_THROW(test.setIpDest(net::Addr{"127.0.0.1"}), Exception);
            }
        }
    }

    void socket()
    {
        net::RawIPv6Socket server1(net::Protocol::ICMPv6);
        if (!tryBind6(server1))
            return;

        server1.start();
        EQ(true, server1.addr().isV6());

        std::shared_ptr<std::promise<std::string>> prom{
            std::make_shared<std::promise<std::string>>()};

        Connection conn = server1.message.connect(
            [prom](const net::RawIPv6Socket::Message &msg) mutable {
                net::ICMPv6Message reply{msg}; // copies from/to
                reply.data = msg.payload();

                if (reply &&
                    (reply.code() == net::ICMPv6Message::Code::EchoReply) &&
                    prom)
                {
                    // use a const object to not trigger the cow
                    const net::ICMPv6Message::data_t payload{reply.payload()};
                    prom->set_value(std::string{payload.begin(), payload.end()});
                    prom.reset(); // reset the prom to not resolve it twice
                                  // (note the mutable on lambda)
                }
            });

        const std::string testStr{"test"};
        net::RawIPv6Message msg(
            server1.addr(), server1.addr(),
            net::RawIPv6Message::Protocol::ICMPv6,
            net::ICMPv6Message::MinDataSize + testStr.size());
        msg.prepare(); // will write from/to in header
        EQ(msg.ipSrc(), net::Addr("::1", 0));
        EQ(msg.ipDest(), net::Addr("::1", 0));

        {
            net::ICMPv6Message icmp{msg.ipSrc(), msg.ipDest()};
            icmp.data = msg.payload();
            icmp.data.buffer().setCopyOnWrite(false);

            icmp.setCode(net::ICMPv6Message::Code::EchoRequest);
            std::copy(testStr.begin(), testStr.end(),
                      icmp.data.data() + net::ICMPv6Message::MinDataSize);
            icmp.prepare();
        }
        server1.send(msg);

        /* promise should resolve with message from server1 */
        EQ(testStr,
           ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void logger()
    {
        logger::notice("test") << "ExplicitCongestionNotification: "
                               << net::RawIPv6Message::ECN::NoECT << " "
                               << net::RawIPv6Message::ECN::ECT1 << " "
                               << net::RawIPv6Message::ECN::ECT2 << " "
                               << net::RawIPv6Message::ECN::CE << " "
                               << static_cast<net::RawIPv6Message::ECN>(0x42);

        logger::notice("test")
            << "DifferentiatedServicesCodePoint: "
            << net::RawIPv6Message::DSCP::DefaultForwarding << " "
            << net::RawIPv6Message::DSCP::ExpeditedForwarding << " "
            << net::RawIPv6Message::DSCP::CS0 << " "
            << net::RawIPv6Message::DSCP::CS1 << " "
            << net::RawIPv6Message::DSCP::CS2 << " "
            << net::RawIPv6Message::DSCP::CS3 << " "
            << net::RawIPv6Message::DSCP::CS4 << " "
            << net::RawIPv6Message::DSCP::CS5 << " "
            << net::RawIPv6Message::DSCP::CS6 << " "
            << net::RawIPv6Message::DSCP::CS7 << " "
            << net::RawIPv6Message::DSCP::AF11 << " "
            << net::RawIPv6Message::DSCP::AF12 << " "
            << net::RawIPv6Message::DSCP::AF13 << " "
            << net::RawIPv6Message::DSCP::AF21 << " "
            << net::RawIPv6Message::DSCP::AF22 << " "
            << net::RawIPv6Message::DSCP::AF23 << " "
            << net::RawIPv6Message::DSCP::AF31 << " "
            << net::RawIPv6Message::DSCP::AF32 << " "
            << net::RawIPv6Message::DSCP::AF33 << " "
            << net::RawIPv6Message::DSCP::AF41 << " "
            << net::RawIPv6Message::DSCP::AF42 << " "
            << net::RawIPv6Message::DSCP::AF43 << " "
            << static_cast<net::RawIPv6Message::DSCP>(0xFF);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetRawIPv6);