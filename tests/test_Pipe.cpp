/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T11:05:30
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Exception.hpp"
#include "Pipe.hpp"
#include "test_helpers.hpp"

using namespace ccut;

class TestPipe : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestPipe);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(flush);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        Pipe p;
        std::vector<char> buffer(10);

        p.write("X");

        EQ(size_t{1}, p.read(buffer.data(), buffer.size()));
        EQ('X', buffer[0]);

        EQ(size_t{0}, p.read(buffer.data(), buffer.size()));

        ::write(p.out(), "X", 1);
        EQ(ssize_t{1}, ::read(p.in(), buffer.data(), buffer.size()));
        EQ('X', buffer[0]);
    }

    void flush()
    {
        Pipe p;
        std::vector<char> buffer(10);
        p.write("X");

        p.flush();
        EQ(size_t{0}, p.read(buffer.data(), buffer.size()));

        p.write(buffer.data(), 1);
        p.flush();
        EQ(size_t{0}, p.read(buffer.data(), buffer.size()));
    }

    void errors()
    {
        Pipe p;
        std::vector<char> buffer(10);

        close(p.in());
        close(p.out());

        ASSERT_THROW(p.write("X"), ccut::Exception);
        ASSERT_THROW(p.flush(), ccut::Exception);
        ASSERT_THROW(p.read(buffer.data(), buffer.size()), ccut::Exception);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestPipe);
