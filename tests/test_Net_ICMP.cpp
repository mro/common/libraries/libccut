/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T14:03:39
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <chrono>
#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "async.hpp"
#include "net/ICMP.hpp"
#include "net/icmp/ICMPEchoMessage.hpp"
#include "net/icmp/ICMPMessage.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class UDPServerError : public net::ICMPSocket
{
public:
    inline int socket() const { return m_socket; }
};

class TestNetICMP : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetICMP);
    CPPUNIT_TEST(echoMessage);
    CPPUNIT_TEST(invalidMessage);
    CPPUNIT_TEST(invalidEchoMessage);
    CPPUNIT_TEST(ipv4);
    CPPUNIT_TEST(ipv6);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void echoMessage()
    {
        const std::string data{"test"};
        net::ICMPEchoMessage msg(net::Addr{"127.0.0.1", 0}, 1, 2, data.size());
        EQ(uint16_t(1), msg.identifier());
        EQ(uint16_t(2), msg.sequenceNum());
        std::copy(data.cbegin(), data.cend(),
                  msg.data.begin() + net::ICMPEchoMessage::MinDataSize);

        msg.prepare();
        EQ(true, msg.isValid());
        EQ(uint16_t(0x2310), msg.checksum());
    }

    void invalidMessage()
    {
        net::ICMPMessage msg;
        EQ(false, msg.isValid());

        msg = net::ICMPMessage{net::Addr{"127.0.0.1"}, net::Addr{"127.0.0.1"}};
        msg.setCode(net::ICMPMessage::Code::Echo);
        msg.prepare();
        EQ(true, msg.isValid());

        { /* invalid data size, accessors should throw */
            net::ICMPMessage test{msg};
            test.data.buffer()->resize(net::ICMPMessage::MinDataSize - 1);
            EQ(false, bool(test.data));
            EQ(false, test.isValid()); // data is not valid anymore

            test.data =
                test.data.buffer(); // reset data to new buffer boundaries
            EQ(true, bool(test.data));
            EQ(false, test.isValid());

            ASSERT_THROW(test.code(), Exception);
            ASSERT_THROW(test.type(), Exception);
            ASSERT_THROW(test.checksum(), Exception);
        }

        { /* invalid checksum */
            net::ICMPMessage test{msg};
            test.data[2] = 42;
            EQ(false, test.isValid());
        }
    }

    void invalidEchoMessage()
    {
        net::ICMPEchoMessage msg;
        EQ(false, msg.isValid());

        msg = net::ICMPEchoMessage{net::Addr{"127.0.0.1"},
                                   net::Addr{"127.0.0.1"}};
        msg.setCode(net::ICMPMessage::Code::Echo);
        msg.prepare();
        EQ(true, msg.isValid());

        {
            net::ICMPEchoMessage test{msg};
            EQ(true, test.isValid());
            test.setCode(net::ICMPMessage::Code::DestinationHostUnknown);
            test.prepare();
            EQ(false, test.isValid()); // not an echo message anymore
        }

        { /* invalid data size, accessors should throw */
            net::ICMPEchoMessage test{msg};
            test.data.buffer()->resize(net::ICMPEchoMessage::MinDataSize - 1);
            EQ(false, bool(test.data));
            EQ(false, test.isValid()); // data is not valid anymore

            test.data =
                test.data.buffer(); // reset data to new buffer boundaries
            EQ(true, bool(test.data));
            EQ(false, test.isValid());

            ASSERT_THROW(test.code(), Exception);
            ASSERT_THROW(test.type(), Exception);
            ASSERT_THROW(test.checksum(), Exception);
            ASSERT_THROW(test.identifier(), Exception);
            ASSERT_THROW(test.sequenceNum(), Exception);
        }
    }

    void ipv4()
    {
        net::ICMPSocket server1;
        server1.bind();
        logger::notice("test")
            << "ICMP socket is " << (server1.isRaw() ? "raw" : "dgram");
        server1.start();
        EQ(false, server1.addr().isV6());

        _msgExchange(server1);
    }

    void ipv6()
    {
        net::ICMPSocket server1;
        try
        {
            server1.bind6();
            FAIL("should have thrown an exception");
        }
        catch (const ccut::Exception &ex)
        {
            EQ(make_error_code(ErrorCode::Runtime), ex.getErrorCode());
            CONTAINS(ex.what(), "not supported");
        }
    }

    void _msgExchange(net::ICMPSocket &server1)
    {
        std::shared_ptr<std::promise<std::string>> prom{
            new std::promise<std::string>()};

        Connection conn = server1.message.connect(
            [prom](const net::ICMPSocket::Message &msg) mutable {
                net::ICMPEchoMessage reply(msg);
                if (!reply.isValid())
                    logger::warning("test") << "reply is not valid";
                else if (reply.type() != net::ICMPMessage::Type::EchoReply)
                    logger::warning("test")
                        << "invalid message type: "
                        << logger::hex(enum_cast(reply.type()), true);
                else if (reply.data.size() <
                         net::ICMPEchoMessage::MinDataSize + 4)
                    logger::warning("test") << "unknown ICMP message";
                else if (prom)
                {
                    net::ICMPMessage::data_t payload{reply.payload()};
                    prom->set_value(std::string{payload.begin(), payload.end()});
                    prom.reset();
                }
            });

        const std::string data{"test"};
        net::ICMPEchoMessage msg(server1.addr(), 1, 2, data.size());
        std::copy(data.cbegin(), data.cend(),
                  msg.data.begin() + net::ICMPEchoMessage::MinDataSize);
        msg.prepare();
        server1.send(msg);

        /* promise should resolve with message from server1 */
        EQ(data, ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void errors()
    {
        net::ICMPSocket server1;
        if (!tryBind(server1, 10000))
            return;

        server1.start();
        ::close(server1.getSocket());

        /* server detects that socket is dead */
        EQ(true,
           waitFor([&server1]() { return server1.getSocket() == -1; }, 5,
                   std::chrono::milliseconds{100}));
    }

    void logger()
    {
        logger::notice("test")
            << "ICMPMessage::Codes: "
            << net::ICMPMessage::Code::DestinationHostUnknown << " "
            << net::ICMPMessage::Code::EchoReply << " "
            << static_cast<net::ICMPMessage::Code>(0xFFFF);

        logger::notice("test")
            << "ICMPMessage::Types: "
            << net::ICMPMessage::Type::DestinationUnreachable << " "
            << net::ICMPMessage::Type::RedirectMessage << " "
            << static_cast<net::ICMPMessage::Type>(0xFF);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetICMP);
