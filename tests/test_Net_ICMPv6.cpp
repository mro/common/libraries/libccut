/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-31T09:33:58
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <chrono>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Buffer.hpp"
#include "Cow.hpp"
#include "Exception.hpp"
#include "async.hpp"
#include "net/ICMPv6.hpp"
#include "net/icmp/ICMPv6Message.hpp"
#include "test_helpers.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestNetICMPv6 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetICMPv6);
    CPPUNIT_TEST(message);
    CPPUNIT_TEST(invalidMessage);
    CPPUNIT_TEST(socket);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void message()
    {
        const cow_ptr<buffer_t> data{
            make_cow<buffer_t>(std::initializer_list<uint8_t>{
                /* ping fe80::468a:5bff:fe29:fcdd request */
                0x80, 0x00, 0xb1, 0xf3, 0x00, 0x04, 0x00, 0x48, 0x1f, 0x77,
                0x8e, 0x65, 0x00, 0x00, 0x00, 0x00, 0x26, 0x71, 0x02, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
                0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
                0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
                0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33,
                0x34, 0x35, 0x36, 0x37})};
        const net::Addr addr{"fe80::468a:5bff:fe29:fcdd", 0};

        net::ICMPv6Message msg(addr, addr, data);
        EQ(true, bool(msg));
        EQ(net::ICMPv6Message::Code::EchoRequest, msg.code());
        EQ(net::ICMPv6Message::Type::EchoRequest, msg.type());
        EQ(uint16_t(0xf3b1), msg.checksum());

        {
            /* rebuild packet from scratch and compare */
            net::ICMPv6Message test{addr, addr};
            test.data = make_cow<buffer_t>(net::ICMPv6Message::MinDataSize +
                                           40 + 16);

            test.setCode(net::ICMPv6Message::Code::EchoRequest);
            test.data.write<uint16_t>(4, 0x0004, true); // identifier
            test.data.write<uint16_t>(6, 0x0048, true); // seqNum

            test.data.write<uint64_t>(
                net::ICMPv6Message::MinDataSize, 1703835423,
                false); // date in little-endian (Linux ping)
            test.data.write<uint64_t>(
                net::ICMPv6Message::MinDataSize + 8, 160038,
                false); // micro-secs timestamp (Linux ping)

            for (size_t i = 0; i < 40; ++i)
                test.data.write<uint8_t>(
                    net::ICMPv6Message::MinDataSize + 16 + i, 0x10 + i);
            test.prepare();

            EQ(msg.checksum(), test.checksum());
            EQ(msg.data, test.data);
        }

        msg.setCode(net::ICMPv6Message::Code::EchoReply);
        EQ(net::ICMPv6Message::Code::EchoReply, msg.code());
        EQ(net::ICMPv6Message::Type::EchoReply, msg.type());
        msg.prepare();
        EQ(uint16_t(0xf3b0), msg.checksum());
    }

    void invalidMessage()
    {
        net::ICMPv6Message msg;
        // default construct is invalid
        EQ(false, msg.isValid());

        msg = net::ICMPv6Message{net::Addr{"fe80::1"}, net::Addr{"fe80::1"}};
        msg.setCode(net::ICMPv6Message::Code::AddressUnreachable);
        msg.prepare();
        EQ(true, msg.isValid());

        { /* addresses must be ipv6 */
            net::ICMPv6Message test{msg};
            test.from = net::Addr{"127.0.0.1"};
            ASSERT_THROW(test.prepare(), Exception);
            EQ(false, test.isValid());
        }

        { /* addresses must be ipv6 */
            net::ICMPv6Message test{msg};
            test.to = net::Addr{"127.0.0.1"};
            ASSERT_THROW(test.prepare(), Exception);
            EQ(false, test.isValid());
        }

        { /* invalid data size, accessors should throw */
            net::ICMPv6Message test{msg};
            test.data.buffer()->resize(net::ICMPv6Message::MinDataSize - 1);
            EQ(false, bool(test.data));
            EQ(false, test.isValid()); // data is not valid anymore

            test.data =
                test.data.buffer(); // reset data to new buffer boundaries
            EQ(true, bool(test.data));
            EQ(false, test.isValid()); // data is too small

            ASSERT_THROW(test.code(), Exception);
            ASSERT_THROW(test.type(), Exception);
            ASSERT_THROW(test.checksum(), Exception);
        }

        {
            /* invalid checksum */
            net::ICMPv6Message test{msg};
            test.data[2] = 42;
            EQ(false, test.isValid());
        }
    }

    void socket()
    {
        net::ICMPv6Socket server1;
        if (!tryBind6(server1))
            return;

        logger::notice("test")
            << "ICMPv6 socket is " << (server1.isRaw() ? "raw" : "dgram");
        server1.start();
        EQ(true, server1.addr().isV6());

        _msgExchange(server1);
    }

    void _msgExchange(net::ICMPv6Socket &server1)
    {
        std::shared_ptr<std::promise<std::string>> prom{
            new std::promise<std::string>()};

        Connection conn = server1.message.connect(
            [prom](const net::ICMPv6Socket::Message &msg) mutable {
                net::ICMPv6Message reply(msg);
                if (!reply.isValid())
                    logger::warning("test") << "reply is not valid";
                else if (reply.code() != net::ICMPv6Message::Code::EchoReply)
                    logger::warning("test")
                        << "invalid message code: "
                        << logger::hex(enum_cast(reply.code()), true);
                else if (reply.data.size() < net::ICMPv6Message::MinDataSize + 4)
                    logger::warning("test") << "invalid size";
                else if (prom)
                {
                    net::ICMPv6Message::data_t payload{reply.payload()};
                    prom->set_value(std::string{payload.begin(), payload.end()});
                    prom.reset();
                }
            });

        const std::string data{"test"};
        net::ICMPv6Message msg(server1.addr(), server1.addr());
        msg.data = make_cow<buffer_t>(net::ICMPv6Message::MinDataSize + 4);
        msg.setCode(net::ICMPv6Message::Code::EchoRequest);
        msg.data.write<uint16_t>(4, 0x0004, true); // identifier
        msg.data.write<uint16_t>(6, 0x0048, true); // seqNum
        std::copy(data.cbegin(), data.cend(),
                  msg.data.data() + net::ICMPv6Message::MinDataSize);
        msg.prepare();
        server1.send(msg);

        /* promise should resolve with message from server1 */
        EQ(data, ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void errors()
    {
        net::ICMPv6Socket server1;
        if (!tryBind6(server1, 10000))
            return;

        server1.start();
        ::close(server1.getSocket());

        /* server detects that socket is dead */
        EQ(true,
           waitFor([&server1]() { return server1.getSocket() == -1; }, 5,
                   std::chrono::milliseconds{100}));
    }

    void logger()
    {
        logger::notice("test")
            << "ICMPv6Message::Codes: "
            << net::ICMPv6Message::Code::RedirectMessage << " "
            << net::ICMPv6Message::Code::NoRouteToDestination << " "
            << static_cast<net::ICMPv6Message::Code>(0xFFFF);

        logger::notice("test")
            << "ICMPv6Message::Types: "
            << net::ICMPv6Message::Type::DestinationUnreachable << " "
            << net::ICMPv6Message::Type::RedirectMessage << " "
            << static_cast<net::ICMPv6Message::Type>(0xFF);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetICMPv6);
