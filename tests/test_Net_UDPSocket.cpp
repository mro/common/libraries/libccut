/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T17:20:04
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <chrono>
#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "async.hpp"
#include "networking.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class UDPServerError : public net::UDPSocket
{
public:
    inline int socket() const { return m_socket; }
};

class TestNetUDPSocket : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetUDPSocket);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(servers);
    CPPUNIT_TEST(ipv6);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        net::UDPSocket server1;

        server1.bind();
        server1.start();

        std::shared_ptr<std::promise<std::string>> prom{
            new std::promise<std::string>()};

        Connection conn = server1.message.connect(
            [prom](const net::UDPSocket::Message &msg) {
                if (!msg.data)
                    return;
                prom->set_value(std::string(msg.data.begin(), msg.data.end()));
            });

        net::UDPSocket client;

        const std::string data("test");
        /* sending a message from client to server1 */
        client.send(
            net::UDPSocket::Message(server1.addr(), data.begin(), data.end()));

        /* promise should resolve with message from server1 */
        EQ(std::string{"test"},
           ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void servers()
    {
        net::UDPSocket server1;

        server1.bind();
        EQ(std::string("127.0.0.1"), server1.addr().host());
        EQ(true, server1.addr().port() != 0);
        server1.start();

        Connection conn = server1.message.connect(
            [&server1](const net::UDPSocket::Message &msg) {
                logger::info("tests") << "message received";
                server1.send(msg.makeReply("world", 5));
            });
        /* first server started, always replying "world" */

        net::UDPSocket server2;
        server2.setBufferSize(4096);
        EQ(size_t{4096}, server2.getBufferSize());

        std::shared_ptr<std::promise<std::string>> prom{
            new std::promise<std::string>()};

        server2.bind();
        EQ(true, server2.addr().port() != 0);
        EQ(true, server2.addr().port() != server1.addr().port());
        server2.start();

        Connection conn2 = server2.message.connect(
            [prom](const net::UDPSocket::Message &msg) {
                logger::info("tests") << "reply received";
                if (!msg.data)
                    return;
                prom->set_value(std::string(msg.data.begin(), msg.data.end()));
            });
        /* second server started, resolving promise with message payload */
        logger::info("tests") << "servers started";

        const std::string data("test");
        /* sending a message from server2 to server1 */
        server2.send(
            net::UDPSocket::Message(server1.addr(), data.begin(), data.end()));

        /* promise should resolve with message from server1 */
        EQ(std::string{"world"},
           ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void ipv6()
    {
        net::UDPSocket server1;

        if (!tryBind6(server1))
            return;

        EQ(std::string("::1"), server1.addr().host());
        server1.start();

        std::shared_ptr<std::promise<std::string>> prom{
            new std::promise<std::string>()};

        Connection conn = server1.message.connect(
            [prom](const net::UDPSocket::Message &msg) {
                if (!msg.data)
                    return;
                prom->set_value(std::string(msg.data.begin(), msg.data.end()));
            });

        net::UDPSocket client;

        const std::string data("test");
        /* sending a message from client to server1 */
        client.send(
            net::UDPSocket::Message(server1.addr(), data.begin(), data.end()));

        /* promise should resolve with message from server1 */
        EQ(std::string{"test"},
           ccut::wait(prom->get_future(), std::chrono::milliseconds{500}));
    }

    void errors()
    {
        net::UDPSocket server1;
        server1.bind(10000);

        {
            net::UDPSocket server2;
            ASSERT_THROW_MSG("port already used", server2.bind(10000),
                             ccut::Exception);
            server2.start();
            /* leave some time for thread to really start */
            std::this_thread::sleep_for(std::chrono::milliseconds{100});
        }
        server1.start();
        ::close(server1.getSocket());

        /* server detects that socket is dead */
        EQ(true,
           waitFor([&server1]() { return server1.getSocket() == -1; }, 5,
                   std::chrono::milliseconds{100}));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetUDPSocket);
