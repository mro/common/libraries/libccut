/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-14T17:03:06+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "test_helpers.hpp"
#include "utils.hpp"

using namespace ccut;

class TestUtils : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestUtils);
    CPPUNIT_TEST(split);
    CPPUNIT_TEST(splitRange);
    CPPUNIT_TEST(startsWith);
    CPPUNIT_TEST(trim);
    CPPUNIT_TEST(urlencode);
    CPPUNIT_TEST(flip_pair);
    CPPUNIT_TEST(flip_map);
    CPPUNIT_TEST(keys);
    CPPUNIT_TEST_SUITE_END();

public:
    void split()
    {
        const struct
        {
            std::string str;
            char sep;
            std::vector<std::string> ret;
        } tests[] = {{"test|toto|", '|', {"test", "toto", ""}},
                     {"|test|toto||", '|', {"", "test", "toto", "", ""}},
                     {"", '|', {""}},
                     {"|", '|', {"", ""}}};

        for (size_t i = 0; i < (sizeof tests / sizeof tests[0]); ++i)
        {
            std::vector<std::string> ret;
            EQ(tests[i].ret, ccut::split(tests[i].str, tests[i].sep, ret));
        }
    }

    void splitRange()
    {
        const struct
        {
            std::string str;
            char sep;
            std::string::size_type begin;
            std::string::size_type end;
            std::vector<std::string> ret;
        } tests[] = {{"test|toto|", '|', 0, 10, {"test", "toto", ""}},
                     {"test|toto|", '|', 4, 8, {"", "tot"}},
                     {"test|toto|", '|', 4, 0, {}},
                     {"test|toto|", '|', 4, 4, {""}},
                     {"test|toto|", '|', 4, 5, {"", ""}}};

        for (size_t i = 0; i < (sizeof tests / sizeof tests[0]); ++i)
        {
            std::vector<std::string> ret;
            EQ(tests[i].ret,
               ccut::split(tests[i].str.cbegin() + tests[i].begin,
                           tests[i].str.cbegin() + tests[i].end, tests[i].sep,
                           ret));
        }
    }

    void startsWith()
    {
        const struct
        {
            std::string str;
            std::string prefix;
            bool ret;
        } tests[]{{"test", "t", true},
                  {"test", "o", false},
                  {"test", "test1234", false},
                  {"test", "", true}};

        for (size_t i = 0; i < (sizeof tests / sizeof tests[0]); ++i)
        {
            EQ(tests[i].ret, ccut::startsWith(tests[i].str, tests[i].prefix));
        }
    }

    void trim()
    {
        const struct
        {
            std::string str;
            std::string ret;
        } tests[]{{" test ", "test"},
                  {" te st", "te st"},
                  {"te st ", "te st"},
                  {"te  st\r\n", "te  st"},
                  {"", ""},
                  {"  \r\n\t  ", ""}};

        for (size_t i = 0; i < (sizeof tests / sizeof tests[0]); ++i)
        {
            EQ(tests[i].ret, ccut::trim(tests[i].str));
        }
    }

    void urlencode()
    {
        const struct
        {
            std::string str;
            std::string encoded;
        } tests[]{
            {"test", "test"},
            {"A !#$%&'()*+,/:;=?@[]Z",
             "A%20%21%23%24%25%26%27%28%29%2A%2B%2C%2F%3A%3B%3D%3F%40%5B%5DZ"},
            {"%", "%25"},
            {"grbl://1/axis/X", "grbl%3A%2F%2F1%2Faxis%2FX"}};

        for (size_t i = 0; i < (sizeof tests / sizeof tests[0]); ++i)
        {
            EQ(tests[i].encoded, ccut::urlencode(tests[i].str));
            EQ(tests[i].str, ccut::urldecode(tests[i].encoded));
        }

        /* don't touch what you don't know */
        EQ(std::string("%"), ccut::urldecode("%"));
        EQ(std::string("%ZZ"), ccut::urldecode("%ZZ"));
        EQ(std::string("%80%OO"), ccut::urldecode("%80%OO"));
    }

    void flip_pair()
    {
        std::pair<std::string, int> p{"test", 1};
        std::pair<int, std::string> p2{ccut::flip(p)};
        EQ(p.first, p2.second);
        EQ(p.second, p2.first);
    }

    void flip_map()
    {
        std::map<std::string, int> map{{"test", 42}, {"toto", 44}};
        EQ(std::string("test"), ccut::flip(map)[42]);
        EQ(std::string("toto"), ccut::flip(map)[44]);
        EQ(size_t(2), ccut::flip(map).size());
    }

    void keys()
    {
        std::map<std::string, int> map{{"test", 42}, {"toto", 44}};
        std::set<std::string> k = ccut::keys(map);
        EQ(true, k.count("test") == 1);
        EQ(true, k.count("toto") == 1);
        EQ(size_t(2), k.size());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestUtils);