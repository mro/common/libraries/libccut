
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Singleton.hpp"
#include "test_helpers.hpp"

// include this only in cpp file
#include "Singleton.hxx"

using namespace ccut;

/**
 * @brief legacy singleton (default type)
 * @details do prefer shared or automatic singletons
 */
class MyTestSingleton : public Singleton<MyTestSingleton>
{
public:
    MyTestSingleton() : value(42) {}

    int value;
};

/**
 * @brief shared-pointer based singleton
 * @details manually manage lifespan of this singleton using `destroy`
 */
class MySharedSingleton :
    public Singleton<MySharedSingleton, SingletonType::shared>
{
public:
    MySharedSingleton() : value(42) {}

    int value;
};

/**
 * @brief weak-pointer based singleton
 * @details this singleton will be automatically released once no users need it
 * anymore
 */
class MyAutoSingleton :
    public Singleton<MyAutoSingleton, SingletonType::automatic>
{
public:
    MyAutoSingleton() : value(42) {}

    int value;
};

class TestSingleton : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSingleton);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(shared);
    CPPUNIT_TEST(automatic);
    CPPUNIT_TEST_SUITE_END();

public:
    void tearDown() { MyTestSingleton::destroy(); }

    void simple()
    {
        CPPUNIT_ASSERT_EQUAL(42, MyTestSingleton::instance().value);

        MyTestSingleton::instance().value = 44;
        CPPUNIT_ASSERT_EQUAL(44, MyTestSingleton::instance().value);
        CPPUNIT_ASSERT_EQUAL(&MyTestSingleton::instance(),
                             &MyTestSingleton::instance());

        MyTestSingleton::destroy();
        CPPUNIT_ASSERT_EQUAL(42, MyTestSingleton::instance().value);
    }

    void shared()
    {
        CPPUNIT_ASSERT_EQUAL(42, MySharedSingleton::instance()->value);
        std::weak_ptr<MySharedSingleton> weak;

        {
            MySharedSingleton::Shared s{MySharedSingleton::instance()};
            EQ(true, bool(s));
            weak = s;
        }
        EQ(false, weak.expired());

        {
            MySharedSingleton::Shared s{MySharedSingleton::instance()};
            EQ(true, weak.lock() == s);
            EQ(true, MySharedSingleton::instance() == s);

            MySharedSingleton::destroy(); /* this will issue a warning */
            EQ(false, MySharedSingleton::instance() == s);
        }
        MySharedSingleton::destroy();
        EQ(true, weak.expired());
    }

    void automatic()
    {
        CPPUNIT_ASSERT_EQUAL(42, MyAutoSingleton::instance()->value);
        std::weak_ptr<MyAutoSingleton> weak;

        {
            MyAutoSingleton::Shared s{MyAutoSingleton::instance()};
            EQ(true, bool(s));
            EQ(true, MyAutoSingleton::instance() == s);
            weak = s;
            EQ(false, weak.expired());
        }
        EQ(true, weak.expired());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSingleton);
