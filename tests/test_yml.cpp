/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-25T10:47:41+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "ryml_internals.hpp"
#include "test_helpers.hpp"
#include "yml.hpp"

using namespace ccut;

class TestRyml : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRyml);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(parseErrors);
    CPPUNIT_TEST(extractErrors);
    CPPUNIT_TEST(overflowErrors);
    CPPUNIT_TEST(get);
    CPPUNIT_TEST(default_to);
    CPPUNIT_TEST(default_to_key);
    CPPUNIT_TEST(create_yml);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        yml::Tree tree = yml::parse_in_arena("foo: 1");
        yml::NodeRef root = tree.rootref();
        EQ(true, root.is_map());

        int i;
        root["foo"] >> i;
        EQ(1, i);
    }

    void parseErrors()
    {
        ASSERT_THROW(yml::parse_in_arena("foo: ["), Exception);
    }

    void extractErrors()
    {
        yml::Tree tree = yml::parse_in_arena("foo: 1");
        yml::NodeRef root = tree.rootref();
        EQ(true, root.is_map());

        int i;
        ASSERT_THROW((root["test"] >> i), Exception);
    }

    void overflowErrors()
    {
        // SKIP: fix pending upstream
        return;
        uint8_t i;
        ASSERT_THROW(yml::parse_in_arena("1024").rootref() >> i, Exception);
    }

    void get()
    {
        yml::Tree tree = yml::parse_in_arena("foo: { bar: [ { baz: 2 } ] }");
        yml::NodeRef root = tree.rootref();

        EQ(true, yml::get(root, "foo", "bar").valid());
        EQ(true, yml::get(root, "foo", "bar", 0, "baz").valid());
        EQ(false, yml::get(root, "foo", "bar", 1, "baz").valid());
        EQ(false, yml::get(root, 12, "foo", "bar", 1, "baz").valid());
    }

    void default_to()
    {
        yml::Tree tree = yml::parse_in_arena("foo: { bar: 2 }");
        yml::NodeRef root = tree.rootref();

        int i;
        yml::get(root, "foo", "bar") >> yml::default_to(i, 12);
        EQ(int(2), i);
        yml::get(root, "fax", "baz") >> yml::default_to(i, 12);
        EQ(int(12), i);
    }

    void default_to_key()
    {
        yml::Tree tree = yml::parse_in_arena("foo: { bar: 2 }");
        yml::NodeRef root = tree.rootref();
        std::string value;
        root.first_child() >> yml::default_to(yml::key(value), "invalid");
        EQ(std::string("foo"), value);

        yml::get(root, "foo", "bar") >>
            yml::default_to(yml::key(value), "invalid");
        EQ(std::string("bar"), value);

        yml::get(root, "fax", "baz") >>
            yml::default_to(yml::key(value), "invalid");
        EQ(std::string("invalid"), value);
    }

    void create_yml()
    {
        yml::Tree tree;
        yml::NodeRef root = tree.rootref();
        root |= yml::MAP;

        root["str"] |= yml::VALQUO;
        root["str"] << "1234";

        root["toto"] |= yml::MAP;
        root["toto"]["tata"] << 44;
        root["tata"] << 42;
        root["titi"] |= yml::SEQ;
        root["titi"][0] << 42;
        logger::notice() << yml::emitrs_json<std::string>(tree);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRyml);
