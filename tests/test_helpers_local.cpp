/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-10T13:27:09+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "test_helpers_local.hpp"

#include <sstream>

#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "net/raw/RawIPMessage.hpp"

CPPUNIT_NS_BEGIN

    CPPUNIT_TRAITS_OVERRIDE(std::vector<std::int32_t>)
    {
        std::ostringstream oss;

        oss << "std::vector({";
        bool first = true;
        for (std::int32_t i : t)
        {
            if (first)
                oss << i;
            else
                oss << ", " << i;
            first = false;
        }
        oss << "})";
        return oss.str();
    }

#define CPPUNIT_USE_LOGGER(T)                    \
    CPPUNIT_TRAITS_OVERRIDE(T)                   \
    {                                            \
        logger::Logger log;                      \
        log.writer.buf.str("");                  \
        log << t;                                \
        const std::string ret{log.writer.str()}; \
        log.writer.buf.str("");                  \
        return ret;                              \
    }

    CPPUNIT_USE_LOGGER(ccut::net::Addr)
    CPPUNIT_USE_LOGGER(ccut::net::Protocol)
    CPPUNIT_USE_LOGGER(ccut::net::ICMPv6Message::Code)
    CPPUNIT_USE_LOGGER(ccut::net::ICMPv6Message::Type)
    CPPUNIT_USE_LOGGER(ccut::net::ICMPMessage::Type)
    CPPUNIT_USE_LOGGER(ccut::net::ICMPMessage::Code)
    CPPUNIT_USE_LOGGER(ccut::net::RawIPv6Message::DSCP)
    CPPUNIT_USE_LOGGER(ccut::net::RawIPv6Message::ECN)
    CPPUNIT_USE_LOGGER(ccut::buffer_t)
    CPPUNIT_USE_LOGGER(ccut::cow_ptr<ccut::buffer_t>)
    CPPUNIT_USE_LOGGER(ccut::buffer_view_t<ccut::cow_ptr<ccut::buffer_t>>)
CPPUNIT_NS_END

static bool shouldSkip(const ccut::Exception &ex, bool isV6 = false)
{
    if (ex.getErrorCode() == ccut::ErrorCode::Denied ||
        ex.getErrorCode() == ccut::ErrorCode::NotPermitted)
    {
        logger::warning("test") << "can't run test: permission denied";
        logger::warning("test")
            << "try `sudo setcap cap_net_raw=ep ./tests/test_all` or "
               "run the tests as root.";
        logger::warning("test") << "skipping test";
        return true;
    }
    else if (ex.getErrorCode() == ccut::ErrorCode::NotSupported)
    {
        logger::warning("test") << "can't run test: not supported";
        logger::warning("test") << "kernel headers are too old, it must be "
                                   ">=4.5 for raw sockets";
        logger::warning("test") << "skipping test";
        return true;
    }

    std::string what{ex.what()};
    std::transform(what.begin(), what.end(), what.begin(),
                   [](char c) { return std::tolower(c); });

    if (isV6 &&
        what.find("cannot assign requested address") != std::string::npos)
    {
        logger::warning("tests")
            << "failed to bind ipv6, probably not supported (not enabled "
               "in containers)";
        logger::warning("test") << "skipping test";
        return true;
    }

    logger::warning() << "error-code:" << ex.getErrorCode();
    return false;
}

bool tryBind(ccut::net::BaseSocket &socket, uint16_t port, bool localhost)
{
    try
    {
        socket.bind(port, localhost);
    }
    catch (ccut::Exception &ex)
    {
        if (shouldSkip(ex))
            return false;
        throw;
    }
    return true;
}

bool tryBind6(ccut::net::BaseSocket &socket, uint16_t port, bool localhost)
{
    try
    {
        socket.bind6(port, localhost);
    }
    catch (ccut::Exception &ex)
    {
        if (shouldSkip(ex, true))
            return false;
        throw;
    }
    return true;
}
