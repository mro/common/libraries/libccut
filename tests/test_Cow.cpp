/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-15T12:19:03
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <memory>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Cow.hpp"
#include "test_helpers.hpp"

using namespace ccut;

struct CustomCowObject
{
    int i;
};

class TestCow : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestCow);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(customConstruct);
    CPPUNIT_TEST(makeConst);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        cow_ptr<int> cow;
        EQ(false, bool(cow));

        cow.reset(new int(42));
        EQ(true, bool(cow));

        {
            const cow_ptr<int> another{cow};
            EQ(int(42), *another);
            EQ(int(42), *another.operator->());
            EQ(long(2), cow.use_count());
        }

        EQ(long(1), cow.use_count());

        {
            cow_ptr<int> another{cow};

            /* do prefer explicit make_const calls this style */
            EQ(int(42), *(make_const(another)));
            EQ(int(42), *make_const(another).operator->());
            EQ(make_const(cow).get(), make_const(another).get());
            EQ(long(2), cow.use_count());

            (*another)++;
            EQ(long(1), cow.use_count());
            EQ(int(42), *cow);
            EQ(int(43), *another);

            another = cow;
            EQ(long(2), cow.use_count());
            EQ(int(42), *(make_const(another)));
        }

        {
            cow_ptr<int> another{new int(42)};
            another.reset();
            EQ(false, bool(another));
        }

        EQ(true, (std::is_assignable<cow_ptr<int>, cow_ptr<int>>::value));
        // forbid conversions to shared_ptr
        EQ(false,
           (std::is_assignable<std::shared_ptr<int>, cow_ptr<int>>::value));
    }

    void customConstruct()
    {
        cow_ptr<CustomCowObject> cow{make_cow<CustomCowObject>()};
        cow->i = 44;

        {
            cow_ptr<CustomCowObject> another{cow};
            EQ(make_const(cow)->i, make_const(another)->i);
            EQ(long(2), cow.use_count());

            another->i = 3;
            EQ(long(1), cow.use_count());
        }
    }

    void makeConst()
    {
        cow_ptr<int> cow{make_cow<int>(1)};
        const cow_ptr<int> another{cow};

        /** ensure that already const can be made const */
        EQ(*make_const(cow), *make_const(another));
        EQ(long(2), cow.use_count());

        EQ(*cow, *another);
        EQ(long(1), cow.use_count());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestCow);
