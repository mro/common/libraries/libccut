/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-12T18:21:24+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include <algorithm>
#include <cstring>
#include <memory>

#include <cppunit/TestFixture.h>
#include <logger/Logger.hpp>

#include "LineBuffer.hpp"
#include "test_helpers_local.hpp"

class TestLineBuffer : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestLineBuffer);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(overflow);
    CPPUNIT_TEST(capacity);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        ccut::LineBuffer ln;

        {
            std::strncpy(ln.end(), "test\n", ln.remaining());
            ln.fwd(5);

            std::string line;
            EQ(true, ln.takeLine(line));
            EQ(std::string("test"), line);

            EQ(false, ln.takeLine(line));
        }

        {
            std::string test = "two\ntoto\nta";
            std::for_each(test.begin(), test.end(),
                          [&ln](char c) { ln.add(c); });

            EQ(true, ln.takeLine(test));
            EQ(std::string("two"), test);

            EQ(true, ln.takeLine(test));
            EQ(std::string("toto"), test);

            EQ(false, ln.takeLine(test));
            EQ(false, ln.hasLine());

            EQ(true, ln.add('\n'));
            EQ(true, ln.hasLine());
            EQ(true, ln.takeLine(test));
            EQ(std::string("ta"), test);
        }
    }

    void overflow()
    {
        ccut::LineBuffer ln;
        while (ln.remaining())
            EQ(false, ln.add('t'));
        ln.add('t');
        ln.add('\n');

        std::string test;
        EQ(true, ln.takeLine(test));
        EQ(std::string("t"), test);
    }

    void capacity()
    {
        ccut::LineBuffer ln{128};

        EQ(size_t{128}, ln.capacity());
        EQ(size_t{0}, ln.size());
        EQ(true, ln.empty());

        ln.add('X');
        EQ(false, ln.empty());
        EQ(size_t{1}, ln.size());

        while (ln.remaining())
            EQ(false, ln.add('t'));
        EQ(ln.size(), ln.capacity());

        ln.reset();
        EQ(true, ln.empty());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLineBuffer);