/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-14T19:17:34
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <memory>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Buffer.hpp"
#include "Cow.hpp"
#include "test_helpers.hpp"

// using namespace ccut;
using ccut::buffer_t;
using ccut::buffer_view_t;
using ccut::cow_ptr;
using ccut::make_cow;

class TestBuffer : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBuffer);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(cow_view);
    CPPUNIT_TEST(shared_view);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        buffer_t buffer(12);
        EQ(size_t(12), buffer.size());

        buffer.write(0, uint32_t(0x12345678), false);
        EQ(uint8_t(0x78), buffer.read<uint8_t>(0));
        EQ(uint8_t(0x78), buffer.read(0));
        EQ(uint16_t(0x1234), buffer.read<uint16_t>(2));
        EQ(uint32_t(0x12345678), buffer.read<uint32_t>(0));

        buffer.write(4, uint32_t(0x12345678), true);
        EQ(uint8_t(0x12), buffer.read(4));
        EQ(uint16_t(0x3412),
           buffer.read<uint16_t>(4)); // default to little-endian
        EQ(uint16_t(0x1234), buffer.read<uint16_t>(4, true));
        EQ(uint32_t(0x78563412), buffer.read<uint32_t>(4));
        EQ(uint32_t(0x12345678), buffer.read<uint32_t>(4, true));
    }

    void cow_view()
    {
        cow_ptr<buffer_t> buffer = make_cow<buffer_t>(8);
        buffer->write(4, uint32_t(0x12345678));

        {
            buffer_view_t<decltype(buffer)> view{buffer};
            EQ(make_const(buffer)->size(), view.size());

            view.setOffset(4);
            EQ(size_t(4), view.size());
            EQ(uint32_t(0x12345678), view.read<uint32_t>(0));

            EQ(long(2), buffer.use_count());
        }

        {
            /* set-size modifies iterators */
            buffer_view_t<decltype(buffer)> view{buffer, 4};
            view.write<uint32_t>(0, 0x55555555);
            view.setSize(2);
            EQ(true, bool(view));

            size_t ret = 0;
            for (uint8_t value : view)
                ret += value;
            EQ(size_t(0x55 * 2), ret);

            view.setSize(0);
            EQ(true, bool(view));
            EQ(true, view.begin() == view.end());
        }

        {
            /* boolean operator indicates view sanity */
            buffer_view_t<decltype(buffer)> view{buffer, 4};
            view.setSize(42);
            EQ(false, bool(view));

            view.setSize(1);
            view.setOffset(42);
            EQ(false, bool(view));

            view.setSize(1);
            view.setOffset(0);
            EQ(true, bool(view));
            view.setBuffer(decltype(buffer)());
            EQ(false, bool(view));

            view.buffer() = buffer;
            EQ(true, bool(view));
        }

        {
            /* modifying a view detaches the internal buffer */
            buffer_view_t<decltype(buffer)> view{buffer, 4};

            EQ(size_t(4), view.size());
            EQ(uint32_t(0x12345678), view.read<uint32_t>(0));
            view.write<uint32_t>(0, 0x55555555);

            // buffer is left untouched
            EQ(uint32_t(0x12345678), make_const(buffer)->read<uint32_t>(4));
            EQ(uint32_t(0x55555555), view.buffer()->read<uint32_t>(4));
            EQ(long(1), buffer.use_count());
        }
    }

    void shared_view()
    {
        std::shared_ptr<buffer_t> buffer = std::make_shared<buffer_t>(8);
        buffer->write(4, uint32_t(0x12345678));

        {
            buffer_view_t<decltype(buffer)> view{buffer};
            EQ(make_const(buffer)->size(), view.size());

            view.setOffset(4);
            EQ(size_t(4), view.size());
            EQ(uint32_t(0x12345678), view.read<uint32_t>(0));

            EQ(long(2), buffer.use_count());
        }

        {
            /* modifying a view detaches the internal buffer */
            buffer_view_t<decltype(buffer)> view{buffer, 4};

            EQ(size_t(4), view.size());
            EQ(uint32_t(0x12345678), view.read<uint32_t>(0));
            view.write<uint32_t>(0, 0x55555555);

            EQ(uint32_t(0x55555555), make_const(buffer)->read<uint32_t>(4));
            EQ(uint32_t(0x55555555), view.buffer()->read<uint32_t>(4));
            EQ(long(2), buffer.use_count());
        }
    }

    void logger()
    {
        {
            cow_ptr<buffer_t> cow;
            std::shared_ptr<buffer_t> shared;
            buffer_t buffer;

            logger::notice("test") << "uninit-buffers: ";
            logger::notice("test") << "  cow_ptr: " << cow;
            logger::notice("test") << "  shared_ptr: " << shared;
            logger::notice("test") << "  empty buffer: " << buffer;

            cow = make_cow<buffer_t>(8, 'A');
            shared = std::make_shared<buffer_t>(8, 'A');
            buffer = buffer_t(8, 'A');
            logger::notice("test") << "init-buffers: ";
            logger::notice("test") << "  cow_ptr: " << cow;
            logger::notice("test") << "  shared_ptr: " << shared;
            logger::notice("test") << "  empty buffer: " << buffer;
        }

        { /* buffer_view_t */
            buffer_view_t<cow_ptr<buffer_t>> cow;
            buffer_view_t<std::shared_ptr<buffer_t>> shared;

            logger::notice("test") << "uninit-views: ";
            logger::notice("test") << "  cow_ptr: " << cow;
            logger::notice("test") << "  shared_ptr: " << shared;

            cow = make_cow<buffer_t>(80, 'A');
            shared = std::make_shared<buffer_t>(80, 'A');
            logger::notice("test") << "init-views: ";
            logger::notice("test") << "  cow_ptr: " << cow;
            logger::notice("test") << "  shared_ptr: " << shared;
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBuffer);
