/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-30T08:46:32
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <memory>
#include <thread>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Exception.hpp"
#include "async.hpp"
#include "net/Message.hpp"
#include "networking.hpp"
#include "test_helpers.hpp"

using namespace ccut;

class TestNetMessage : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestNetMessage);
    CPPUNIT_TEST(cksum);
    CPPUNIT_TEST_SUITE_END();

public:
    void cksum()
    {
        const std::vector<uint8_t> test{0x12, 0x34, 0x56, 0x78,
                                        0x12, 0x34, 0x56, 0x78};

        EQ(uint16_t(0x5397), net::cksum(test.data(), 4));
        EQ(uint16_t(0x5397), net::cksum(test.begin(), test.begin() + 4));

        EQ(uint16_t(0xA72E), net::cksum(test.begin(), test.end()));
        EQ(uint16_t(0xA72E), net::cksum(test.begin() + 4, test.end(), 0x5397));

        uint16_t ret = 0xFFFF;
        for (size_t i = 0; i < test.size(); i += 2)
        {
            /* incremental checksum, must be fed with a even number of bytes */
            ret = net::cksum(test.begin() + i, test.begin() + i + 2, ret);
        }
        EQ(uint16_t(0xA72E), ret);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestNetMessage);