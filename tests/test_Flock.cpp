/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-01T14:58:01+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <mutex>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <errno.h>
#include <logger/Logger.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "Exception.hpp"
#include "Flock.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestFlock : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestFlock);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(stl);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

    char tmpFileLock[30];

public:
    void setUp() override
    {
        memset(tmpFileLock, 0, sizeof(tmpFileLock));
        strncpy(tmpFileLock, "/tmp/myTmpLock-XXXXXX", 21);
        errno = 0;
        if (mkstemp(tmpFileLock) < 1)
            logger::error() << "Creation of temp file failed with error "
                            << strerror(errno);
        else
            logger::debug() << "Created tmp file " << tmpFileLock;
    }

    void tearDown() override
    {
        errno = 0;
        if (unlink(tmpFileLock) != 0)
            logger::error() << "Error deleting " << tmpFileLock << "file"
                            << strerror(errno);
        else
            logger::debug()
                << "File " << tmpFileLock << " successfully deleted";
    }

    void simple()
    {
        std::string fileLock(tmpFileLock);
        Flock l(Flock::Shared, fileLock);
        EQ(fileLock, l.fileName());
        EQ(Flock::Shared, l.mode());
        EQ(false, l.isLocked());

        l.lock();
        EQ(true, l.isLocked());

        // already locked should not do anything
        EQ(true, l.try_lock());
        l.lock();

        {
            Flock other(Flock::Shared, fileLock);
            EQ(true, other.try_lock());
        }

        {
            Flock other(Flock::Exclusive, fileLock);
            EQ(false, other.try_lock());
            EQ(false, other.isLocked());
            l.unlock();

            EQ(true, other.try_lock());
            EQ(true, other.isLocked());

            EQ(false, l.try_lock());
        }

        EQ(true, l.try_lock());
    }

    void stl()
    {
        std::string fileLock(tmpFileLock);
        Flock l(Flock::Shared, fileLock);

        {
            std::unique_lock<Flock> locker(l);
            EQ(true, l.isLocked());
            locker.unlock();

            EQ(true, locker.try_lock());
        }
        EQ(false, l.isLocked());

        {
            std::lock_guard<Flock> locker(l);
            EQ(true, l.isLocked());
        }
        EQ(false, l.isLocked());
    }

    void error()
    {
        std::string fileLockNotExist = std::string(tmpFileLock) +
            "/doesnotexist/lock";
        CPPUNIT_ASSERT_THROW(Flock(Flock::Shared, fileLockNotExist),
                             ccut::Exception);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestFlock);
