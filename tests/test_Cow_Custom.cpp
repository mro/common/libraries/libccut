/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T10:44:32
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include <memory>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "Cow.hpp"
#include "test_helpers.hpp"

using namespace ccut;

struct CowDetachOverride
{
    int i;
};

static std::atomic<int> s_counter;

namespace ccut {
template<>
void Cow<CowDetachOverride>::detach()
{
    if (!this->operator bool() || this->use_count() <= 1)
        return;
    s_counter++;
    this->reset(
        new CowDetachOverride(*std::shared_ptr<CowDetachOverride>::get()));
}
} // namespace ccut

struct CowCopyOverride
{
    int i;
};

namespace ccut {
template<>
CowCopyOverride *Cow<CowCopyOverride>::copy(const CowCopyOverride &other)
{
    s_counter++;
    return new CowCopyOverride(other);
}
} // namespace ccut

class TestCowCustom : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestCowCustom);
    CPPUNIT_TEST(customDetach);
    CPPUNIT_TEST(customCopy);
    CPPUNIT_TEST_SUITE_END();

public:
    void customDetach()
    {
        s_counter = 0;
        cow_ptr<CowDetachOverride> cow = make_cow<CowDetachOverride>();
        cow->i = 44;

        EQ(int(0), int(s_counter));

        {
            cow_ptr<CowDetachOverride> another{cow};
            EQ(int(0), int(s_counter));

            another.detach();
            EQ(int(1), int(s_counter));
        }
    }

    void customCopy()
    {
        s_counter = 0;
        cow_ptr<CowCopyOverride> cow{make_cow<CowCopyOverride>()};
        cow->i = 44;

        EQ(int(0), int(s_counter));

        {
            cow_ptr<CowCopyOverride> another{cow};
            EQ(int(0), int(s_counter));

            another.detach();
            EQ(int(1), int(s_counter));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestCowCustom);
