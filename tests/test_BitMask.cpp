/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-09-27T09:26:33
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <cerrno>
#include <cstring>
#include <map>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <logger/Logger.hpp>

#include "BitMask.hpp"
#include "test_helpers.hpp"

using ccut::BitMask;

enum class MyEnum : uint8_t
{
    BIT0 = 0x01,
    BIT1 = 0x02,
    BIT2 = 0x04
};

template<>
struct ccut_enable_bitmask<MyEnum> : std::true_type
{};

std::string to_string(const MyEnum &e)
{
    switch (e)
    {
    case MyEnum::BIT0: return "BIT0";
    case MyEnum::BIT1: return "BIT1";
    case MyEnum::BIT2: return "BIT2";
    default: return "UNKNOWN";
    }
}

void bitmask_func(const ccut::BitMask<MyEnum> &mask)
{
    logger::notice() << "bitmask_func called: " << mask;
}

class TestBitMask : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBitMask);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(conversions);
    CPPUNIT_TEST(logger);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        BitMask<MyEnum> mask;
        EQ(false, bool(mask));

        mask &= MyEnum::BIT1;
        mask |= MyEnum::BIT0;
        EQ(true, bool(mask & MyEnum::BIT0));
        EQ(false, bool(mask & MyEnum::BIT1));
        EQ(uint8_t(0x01), mask.bits());

        EQ(true, mask == MyEnum::BIT0);
        EQ(false, mask == MyEnum::BIT1);
        EQ(true, mask != MyEnum::BIT1);

        /* mask operations and equality */
        EQ(true, ((MyEnum::BIT0 | MyEnum::BIT1) & MyEnum::BIT0) == mask);
    }

    void conversions()
    {
        EQ(true, bool(MyEnum::BIT0 | MyEnum::BIT1));
        EQ(false, bool(MyEnum::BIT0 & MyEnum::BIT1));

        bitmask_func(MyEnum::BIT0);
    }

    void logger()
    {
        logger::notice()
            << "This is an enum: " << (MyEnum::BIT0 | MyEnum::BIT1);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBitMask);
