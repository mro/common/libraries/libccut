/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-20T22:57:03+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>
#include <thread>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Signal.hpp"
#include "test_helpers_local.hpp"

using namespace ccut;

class TestSignal : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestSignal);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(scope);
    CPPUNIT_TEST(signalScope);
    CPPUNIT_TEST(signalCopy);
    CPPUNIT_TEST(disconnectWait);
    CPPUNIT_TEST(disconnectWaitReentrant);
    CPPUNIT_TEST(disconnectAll);
    CPPUNIT_TEST(disconnectAllReentrant);
    CPPUNIT_TEST(loopSend);
    CPPUNIT_TEST(perf);
    CPPUNIT_TEST_SUITE_END();

public:
    void simple()
    {
        Signal<int, int> sig;
        bool called = false;

        Connection conn = sig.connect([&called](int, int) { called = true; });
        EQ(true, conn.isConnected());

        sig(12, 14);
        EQ(true, called);
        called = false;
        sig(12, 14);
        EQ(true, called);

        conn.disconnect();
        EQ(false, conn.isConnected());
        called = false;
        sig(12, 14);
        EQ(false, called);
    }

    void scope()
    {
        Signal<> sig;
        bool called = false;
        {
            Connection con = sig.connect([&called]() { called = true; });

            sig();
            EQ(true, called);
        }

        called = false;
        sig();
        EQ(false, called);
    }

    void signalScope()
    {
        Connection con;
        {
            bool called = false;
            Signal<> sig;
            sig.connect(con, [&called]() { called = true; });
            EQ(true, con.isConnected());
            sig();
            EQ(true, called);
        }
        EQ(false, con.isConnected());
    }

    void signalCopy()
    {
        Connection con;
        {
            bool called = false;
            Signal<> sig;
            sig.connect(con, [&called]() { called = true; });

            sig();
            EQ(true, called);

            {
                called = false;
                Signal<> sig2 = sig;

                sig2();
                EQ(true, called);
            }
            EQ(true, con.isConnected());
        }
        EQ(false, con.isConnected());
    }

    void disconnectWait()
    {
        Signal<> sig;
        std::mutex mutex;
        std::condition_variable cond;

        {
            bool called = false;

            std::unique_lock<std::mutex> lock(mutex);
            Connection conn = sig.connect([&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    cond.notify_all();
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                called = true;
            });

            std::future<void> f = std::async(std::launch::async,
                                             [&sig]() { sig(); });
            cond.wait(lock);
            conn.disconnect(true); // waiting
            EQ(true, called);
            EQ(std::future_status::ready,
               f.wait_for(std::chrono::milliseconds(1)));
        }

        {
            bool called = false;

            std::unique_lock<std::mutex> lock(mutex);
            Connection conn = sig.connect([&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    cond.notify_all();
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                called = true;
            });

            std::future<void> f = std::async(std::launch::async,
                                             [&sig]() { sig(); });
            cond.wait(lock);
            conn.disconnect(false); // not waiting
            EQ(false, called);
            EQ(std::future_status::timeout,
               f.wait_for(std::chrono::milliseconds(1)));

            f.wait(); // do not race
        }
    }

    void disconnectWaitReentrant()
    {
        Signal<> sig;
        std::mutex mutex;
        std::condition_variable cond;

        {
            bool called = false;
            std::atomic<int> count(0);

            Connection conn = sig.connect([&]() {
                if (count++ == 0)
                {
                    std::unique_lock<std::mutex> lock(mutex);

                    std::future<void> f = std::async(std::launch::async,
                                                     [&sig]() { sig(); });
                    /* first call, wait for cond */
                    cond.wait(lock);
                    conn.disconnect(true); // waiting
                    EQ(true, called);
                    EQ(std::future_status::ready,
                       f.wait_for(std::chrono::milliseconds(1)));
                }
                else
                {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        cond.notify_all();
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    // this call should not block since another thread is
                    // already waiting
                    conn.disconnect(true);
                    called = true;
                }
            });

            sig();
        }
    }

    void disconnectAll()
    {
        Signal<> sig;
        std::mutex mutex;
        std::condition_variable cond;

        {
            bool called_start = false;
            bool called_end = false;

            Connection conn1 = sig.connect([&]() { called_start = true; });
            Connection conn2 = sig.connect([&]() { sig.disconnect(true); });
            Connection conn3 = sig.connect([&]() { called_end = true; });

            sig();
            EQ(true, called_start);
            EQ(false, called_end);
            EQ(false, conn3.isConnected());
        }
    }

    void disconnectAllReentrant()
    {
        Signal<> sig;
        std::mutex mutex;
        std::condition_variable cond;

        bool called = false;
        std::atomic<int> count(0);

        Connection conn = sig.connect([&]() {
            if (count++ == 0)
            {
                std::unique_lock<std::mutex> lock(mutex);

                std::future<void> f = std::async(std::launch::async,
                                                 [&sig]() { sig(); });
                /* first call, wait for cond */
                cond.wait(lock);
                sig.disconnect(true); // waiting
                EQ(false, conn.isConnected());
                EQ(true, called);
            }
            else
            {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    cond.notify_all();
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                called = true;
                // this call should not block since another thread is
                // already waiting
                sig.disconnect(true);
            }
        });

        sig();
        EQ(false, conn.isConnected());
    }

    void loopSend()
    {
        const std::size_t loopCount = 1000;
        const std::size_t listenerCount = 1000;
        Signal<> sig;
        std::mutex mutex;
        std::condition_variable cond;

        std::vector<std::size_t> count(loopCount, 0);
        std::vector<Connection> connections(loopCount);

        std::atomic<bool> running(true);
        std::thread th([&running, &sig]() {
            while (running)
            {
                sig();
                std::this_thread::yield();
            }
        });
        std::thread th2([&running, &sig]() {
            while (running)
            {
                sig();
                std::this_thread::yield();
            }
        });

        try
        {
            for (std::size_t i = 0; i < listenerCount; ++i)
            {
                sig.connect(
                    connections[i],
                    [i, loopCount, &count, &connections, &cond, &mutex]() {
                        if (++count[i] >= loopCount)
                        {
                            connections[i].disconnect();
                            std::unique_lock<std::mutex> lock(mutex);
                            cond.notify_all();
                        }
                    });
            }

            {
                std::unique_lock<std::mutex> lock(mutex);
                for (std::size_t i = 0; i < listenerCount; ++i)
                {
                    EQ(true,
                       cond.wait_for(lock, std::chrono::seconds(5),
                                     [i, &connections]() {
                                         return !connections[i].isConnected();
                                     }));
                }
            }
            running.store(false);
            th.join();
            th2.join();
        }
        catch (...)
        {
            running.store(false);
            th.join();
            th2.join();
            throw;
        }
    }

    void perf()
    {
        const std::size_t loopCount = 1000;
        const std::size_t listenerCount = 1000;
        Signal<> sig;
        std::vector<Connection> connections;
        std::size_t calls = 0;

        for (std::size_t i = 0; i < listenerCount; ++i)
        {
            connections.push_back(sig.connect([&calls]() { ++calls; }));
        }
        sig();

        std::cout << "\n\n";
        calls = 0;
        {
            std::function<void()> fun = [&calls]() { ++calls; };

            auto start = std::chrono::steady_clock::now();
            for (std::size_t i = 0; i < loopCount; ++i)
            {
                for (std::size_t j = 0; j < listenerCount; ++j)
                {
                    fun();
                }
            }
            auto end = std::chrono::steady_clock::now();
            EQ(loopCount * listenerCount, calls);
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
                end - start);
            std::cout << "Direct Calls took: " << duration.count() << "µs "
                      << uintmax_t((loopCount * 1e6) / duration.count())
                      << " messages/second" << std::endl;
        }

        calls = 0;
        {
            auto start = std::chrono::steady_clock::now();
            for (std::size_t i = 0; i < loopCount; ++i)
            {
                sig();
            }
            auto end = std::chrono::steady_clock::now();

            EQ(loopCount * listenerCount, calls);
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
                end - start);
            std::cout << "Signals took: " << duration.count() << "µs "
                      << uintmax_t((loopCount * 1e6) / duration.count())
                      << " messages/second" << std::endl;
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestSignal);
