/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-20T16:42:00+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#ifndef SIGNAL_HXX__
#define SIGNAL_HXX__

#include <iostream>
#include <mutex>

#include "Signal.hpp"
#include <logger/Logger.hpp>

namespace ccut {

/**
 * @brief signal internals
 */
struct SignalData
{
public:
    ~SignalData(); // for the shared_ptr

protected:
    template<typename... A>
    friend class Signal;
    friend class Connection;

    void gc();
    void disconnect(bool wait);

    std::mutex lock;
    std::condition_variable cond;
    std::set<std::thread::id> tids;
    std::list<std::shared_ptr<BaseListener>> list;
};


template<typename... Args>
Signal<Args...>::Signal() : m_data(std::make_shared<SignalData>())
{}

template<typename... Args>
Signal<Args...>::Listener::Listener(std::function<void(Args...)> fun) :
    BaseListener(), call(fun)
{}

template<typename... Args>
void Signal<Args...>::operator()(Args... args)
{
    std::vector<std::weak_ptr<BaseListener>> list;

    {
        std::unique_lock<std::mutex> lock(m_data->lock);
        list.swap(m_cache);
        const std::size_t sz = m_data->list.size();

        if (sz > list.capacity())
        {
            lock.unlock();
            list.reserve(sz);
            lock.lock();
        }

        list.assign(m_data->list.begin(), m_data->list.end());
        m_data->tids.insert(std::this_thread::get_id());
    }

    bool clean = false;
    for (std::weak_ptr<BaseListener> &wc : list)
    {
        std::shared_ptr<BaseListener> c(wc.lock());
        if (!c)
            continue;
        else if (!(*c))
            clean = true;
        else
            static_cast<Listener &>(*c).call(std::forward<Args>(args)...);
    }

    {
        std::lock_guard<std::mutex> lock(m_data->lock);
        if (clean)
            m_data->gc();
        list.swap(m_cache);
        m_data->tids.erase(std::this_thread::get_id());
    }
    m_data->cond.notify_all();
}

template<typename... Args>
void Signal<Args...>::disconnect(bool wait)
{
    m_data->disconnect(wait);
}

template<typename... Args>
Connection Signal<Args...>::connect(std::function<void(Args...)> fun)
{
    std::lock_guard<std::mutex> lock(m_data->lock);
    m_data->list.emplace_back(std::make_shared<Listener>(fun));
    return Connection(m_data, m_data->list.back());
}

template<typename... Args>
Connection &Signal<Args...>::connect(Connection &conn,
                                     std::function<void(Args...)> fun)
{
    std::lock_guard<std::mutex> lock(m_data->lock);
    m_data->list.emplace_back(std::make_shared<Listener>(fun));
    conn = std::move(Connection(m_data, m_data->list.back()));
    return conn;
}

} // namespace ccut

#endif
