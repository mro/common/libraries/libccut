/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-25T11:17:22+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#define RYML_SINGLE_HDR_DEFINE_NOW
#include "yml.hpp"

#include <logger/Logger.hpp>

#include "Exception.hpp"

namespace ccut {
namespace yml {

struct Init
{
    Init() { init(); }
};

static Init i;

static void yamlError(const char *msg,
                      size_t msg_len,
                      Location,
                      void * /*user_data*/)
{
    std::string errMsg(msg, msg + msg_len);

    logger::error("ccut:yml") << "yml error: " << errMsg;
    throw Exception(ErrorCode::InvalidArguments, errMsg);
}

void init()
{
    set_callbacks(Callbacks(nullptr, nullptr, nullptr, yamlError));
    logger::info("ccut:yml") << "initialized";
}

Exception make_error(const Parser &parser,
                     const NodeRef &ref,
                     const std::string &message)
{
    if (ref.valid())
    {
        Location loc = parser.location(ref);
        std::ostringstream oss;
        oss << parser.filename().str << ":" << loc.line << ":" << loc.col
            << ": " << message;
        return Exception(ErrorCode::InvalidArguments, oss.str());
    }
    return Exception(ErrorCode::InvalidArguments, message);
}

} // namespace yml
} // namespace ccut
