/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-17T21:47:18+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#ifndef CCUT_ASYNC_HPP__
#define CCUT_ASYNC_HPP__

#include <chrono>
#include <future>
#include <stdexcept>

#include <logger/Logger.hpp>

namespace ccut {

/**
 * @brief wait for a future to resolve
 * @details this adds timeout to default wait, this method uses const to accept
 * rvalues
 * @param  f  future to resolve
 * @param  ms timeout delay
 * @return the future resolved value
 * @throws std::runtime_error on error
 */
template<typename T>
T wait(const std::future<T> &f,
       const std::chrono::milliseconds &ms = std::chrono::milliseconds(5000))
{
    if (f.wait_for(ms) != std::future_status::timeout)
        return const_cast<typename std::future<T> &>(f).get();
    else
        throw std::runtime_error("timeout");
}

inline void wait(
    const std::future<void> &f,
    const std::chrono::milliseconds &ms = std::chrono::milliseconds(5000))
{
    if (f.wait_for(ms) != std::future_status::timeout)
        const_cast<std::future<void> &>(f).get();
    else
        throw std::runtime_error("timeout");
}

/**
 * @brief create an erroneous future
 * @details mainly useful for early checks
 * @param  ex exception object to set
 * @return a future in error state
 */
template<typename T = void, typename E>
std::future<T> make_future_error(const E &ex)
{
    std::promise<T> p;
    p.set_exception(std::make_exception_ptr(ex));
    return p.get_future();
}

/**
 * @brief safely get future result
 *
 * @param f future to resolve
 * @param def default value returned
 * @param msg error message
 * @param ms timeout delay
 * @return T
 */
template<typename T>
T try_get(const std::future<T> &f,
          const T &def,
          const std::chrono::milliseconds &ms = std::chrono::milliseconds(5000),
          const std::string &msg = "failed to get promise value") noexcept

{
    try
    {
        return ccut::wait(f, ms);
    }
    catch (std::exception &ex)
    {
        logger::error("ccut::async") << msg << ": " << ex.what();
    }
    catch (...)
    {
        logger::error("ccut::async") << msg;
    }
    return def;
}

/**
 * @brief safely get future result
 *
 * @param f future to resolve
 * @param msg error message
 * @param ms timeout delay
 * @return true if promise succeeded
 */
inline bool try_get(
    const std::future<void> &f,
    const std::chrono::milliseconds &ms = std::chrono::milliseconds(5000),
    const std::string &msg = "failed to get promise value") noexcept
{
    try
    {
        ccut::wait(f, ms);
        return true;
    }
    catch (std::exception &ex)
    {
        logger::error("ccut::async") << msg << ": " << ex.what();
    }
    catch (...)
    {
        logger::error("ccut::async") << msg;
    }
    return false;
}

} // namespace ccut

#endif
