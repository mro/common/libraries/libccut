/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-20T16:42:00+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include "Signal.hpp"

#include <algorithm>

namespace ccut {

BaseListener::BaseListener() : active(true) {}

void SignalData::SignalData::gc()
{
    list.remove_if([](const std::shared_ptr<BaseListener> &c) { return !(*c); });
}

void SignalData::disconnect(bool wait)
{
    std::unique_lock<std::mutex> l(lock);

    for (const std::shared_ptr<BaseListener> &listener : list)
        listener->active.store(false);
    list.clear();

    if (wait)
    {
        /* this thread won't run anything else, let's simply remove it from tids
         */
        if (tids.erase(std::this_thread::get_id()) != 0)
            cond.notify_all();
        cond.wait(l, [this]() { return tids.size() == 0; });
    }
}

SignalData::~SignalData()
{
    disconnect(true);
}

Connection::Connection() {}

Connection::Connection(const std::shared_ptr<SignalData> &sig,
                       const std::shared_ptr<BaseListener> &listener) :
    m_signal(sig),
    m_listener(listener)
{}

Connection::~Connection()
{
    disconnect();
}

void Connection::swap(Connection &other)
{
    m_signal.swap(other.m_signal);
    m_listener.swap(other.m_listener);
}

bool Connection::isConnected() const
{
    std::shared_ptr<SignalData> signal(m_signal.lock());
    std::shared_ptr<BaseListener> listener(m_listener.lock());

    if (signal && listener)
    {
        const bool ret = *listener;
        {
            std::unique_lock<std::mutex> lock(signal->lock);
            listener.reset();
        }
        signal->cond.notify_all();
        return ret;
    }
    else
        return false;
}

void Connection::disconnect(bool wait)
{
    std::shared_ptr<SignalData> signal(m_signal.lock());
    std::shared_ptr<BaseListener> listener(m_listener.lock());

    if (!signal || !listener)
    {
        return;
    }
    if (listener->active.exchange(false) == false)
    {
        {
            std::unique_lock<std::mutex> lock(signal->lock);
            listener.reset();
        }
        signal->cond.notify_all();
        return; // was already inactive, let's not wait again
    }

    if (wait)
    {
        std::unique_lock<std::mutex> lock(signal->lock);
        const bool inHandler = signal->tids.count(std::this_thread::get_id()) !=
            0;
        signal->gc();

        /*
         + 1 ref here
         + 1 ref if current thread is in handler
         */
        signal->cond.wait(lock, [&listener, &inHandler]() {
            return inHandler ? (listener.use_count() <= 2) :
                               (listener.use_count() <= 1);
        });
    }
}

} // namespace ccut
