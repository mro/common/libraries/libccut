/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-12-20T16:25:41
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef CCUT_NETWORKING_HPP__
#define CCUT_NETWORKING_HPP__

#include "net/Addr.hpp"
#include "net/ICMP.hpp"
#include "net/ICMPv6.hpp"
#include "net/Message.hpp"
#include "net/Socket.hpp"
#include "net/UDP.hpp"
#include "net/raw.hpp"

#endif