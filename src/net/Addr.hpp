/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T09:21:23
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef CCUT_NET_HPP__
#define CCUT_NET_HPP__

#include <cstdint>
#include <memory>

#include <logger/Logger.hpp>
#include <netinet/in.h>
#include <sys/socket.h>

#include "../Buffer.hpp"
#include "../Cow.hpp"

namespace ccut {
namespace net {

/**
 * @brief Network Address class
 * @details wraps posix sockaddr struct
 */
class Addr
{
public:
    /**
     * @brief Construct an invalid Address
     */
    Addr() = default;
    Addr(const Addr &) = default;
    Addr(Addr &&) = default;
    Addr &operator=(const Addr &) = default;
    Addr &operator=(Addr &&) = default;

    /**
     * @brief Construct an address from string
     * @details string must be either ipv4 (ex: 192.168.0.1) or ipv6 (ex: ::1)
     * @details constructs an invalid address on error
     */
    // cppcheck-suppress noExplicitConstructor
    Addr(const std::string &addr, uint16_t port = 0);

    /**
     * @brief Construct an typed address
     *
     * @param inet network type (AF_INET6 or AF_INET)
     * @param host address to parse
     * @param port port
     * @details constructs an invalid address on error
     */
    Addr(int inet, const std::string &host, uint16_t port = 0);

    bool operator==(const Addr &other) const;
    inline bool operator!=(const Addr &other) const
    {
        return !(*this == other);
    }

    /**
     * @brief detach the underlying shared_ptr
     * @details mainly for internal use
     */
    void detach();

    /**
     * @brief check if address is valid
     */
    inline bool isValid() const { return bool(m_addr); }
    inline operator bool() const { return bool(m_addr); }

    /**
     * @brief get associated port
     */
    uint16_t port() const;

    /**
     * @brief get associated host
     *
     * @return std::string
     */
    std::string host() const;

    /**
     * @brief get underlying sockaddr struct
     */
    inline const struct sockaddr *addr() const { return m_addr.get(); }

    /**
     * @brief get underlying sockaddr struct size
     */
    size_t size() const;

    /**
     * @brief return true if address is an IPv6 address
     */
    bool isV6() const;

    /**
     * @brief Buffer conversion constructor
     * @details port is always 0 initialized
     * @param buffer buffer to create socket from (must be 4 or 16 bytes long)
     */
    explicit Addr(const buffer_view_t<cow_ptr<buffer_t>> &buffer);

    /**
     * @brief Buffer affectation
     * @details port is always 0 initialized, this construct brings a small
     * optimization if Addr was already from the same family.
     *
     * @param buffer buffer to create socket from (must be 4 or 16 bytes long)
     * @return Addr& self pointer
     */
    Addr &operator=(const buffer_view_t<cow_ptr<buffer_t>> &buffer);

    static inline Addr fromBuffer(const buffer_view_t<cow_ptr<buffer_t>> &buffer)
    {
        return Addr(buffer);
    }

    bool toBuffer(buffer_view_t<cow_ptr<buffer_t>> &buffer) const;
    cow_ptr<buffer_t> toBuffer() const;

protected:
    std::shared_ptr<struct sockaddr> m_addr;
};

typedef Addr address_t;

const logger::Logger &operator<<(const logger::Logger &logger,
                                 const ccut::net::address_t &addr);

} // namespace net
} // namespace ccut

#endif
