/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T12:06:12
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef IPSOCKET_HXX__
#define IPSOCKET_HXX__

#include <logger/Logger.hpp>
#include <poll.h>
#include <unistd.h>

#include "../Buffer.hpp"
#include "../Exception.hpp"
#include "Socket.hpp"

namespace ccut {
namespace net {

template<typename M>
Socket<M>::Socket(const std::string &name) : BaseSocket(name)
{}

template<typename M>
Socket<M>::~Socket()
{
    stop();
    if (m_socket > 0)
        ::close(m_socket);
}

template<typename M>
void Socket<M>::send(const Message &msg)
{
    if (m_socket < 0)
    {
        if (msg.to.isValid() && msg.to.addr()->sa_family == AF_INET6)
            bind6();
        else
            bind();
    }

    ssize_t sz = sendto(m_socket, msg.data.data(), msg.data.size(), 0,
                        msg.to.addr(), msg.to.size());
    if (sz < 0)
    {
        ccut::Exception err = ccut::make_errno_exception();
        logger::error("ccut:net:socket") << "failed to send: " << err.what();
        throw err;
    }
}

template<typename M>
void Socket<M>::thread_func()
{
    if (m_socket < 0)
    {
        logger::notice("ccut:net:socket") << "Socket started but socket closed";
        return;
    }

    address_t from{m_addr};
    cow_ptr<buffer_t> buffer{make_cow<buffer_t>()};

    while (m_started.load() && m_socket > 0)
    {
        struct pollfd pfds[2] = {{m_socket, POLLIN | POLLERR, 0},
                                 {m_wakePipe.in(), POLLIN | POLLERR, 0}};
        if (::poll(pfds, 2, -1) < 0)
            continue;

        if (pfds[1].revents & POLLIN)
            m_wakePipe.flush();

        if (pfds[0].revents & (POLLNVAL | POLLERR) ||
            pfds[1].revents & (POLLNVAL | POLLERR))
        {
            logger::error("ccut:net:socket") << "socket error, closing";
            ::close(m_socket);
            m_socket = -1; /* will come back on next bind/open */
            return;
        }

        if (pfds[0].revents & POLLIN)
        {
            buffer->resize(m_bufferSize);
            from.detach();

            socklen_t slen = from.size();
            ssize_t sz = recvfrom(m_socket, buffer->data(), buffer->size(), 0,
                                  const_cast<struct sockaddr *>(from.addr()),
                                  &slen);
            if (sz < 0)
            {
                ccut::Exception ex{ccut::make_errno_exception()};
                logger::error("ccut:net:socket")
                    << "failed to receive message: " << ex.what();
            }
            else
            {
                buffer->resize(sz);
                Message m{processMessage(from, m_addr, buffer)};
                if (m.isValid())
                    message(m);
            }
        }
    }
}

template<typename M>
typename Socket<M>::Message Socket<M>::processMessage(
    const address_t &from,
    const address_t &to,
    const cow_ptr<buffer_t> &buffer)
{
    Message msg(from, to, buffer);
    logger::debug("ccut:net") << "message received " << from << " -> " << to
                              << " size:" << (buffer ? buffer->size() : -1);
    return msg;
}

} // namespace net
} // namespace ccut

#endif
