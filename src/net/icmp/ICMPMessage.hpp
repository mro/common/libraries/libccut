/*
** Copyright (C) 2024 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-01-02T08:46:26
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef ICMPMESSAGE_HPP__
#define ICMPMESSAGE_HPP__

#include "../Message.hpp"
#include "ICMPv6Message.hpp"

namespace ccut {
namespace net {

struct ICMPMessage : public net::Message
{
    using Message::Message;

    ICMPMessage() = default;

    /**
     * @brief downcast a Message to an ICMPMessage
     */
    // cppcheck-suppress noExplicitConstructor
    ICMPMessage(const Message &message) : Message(message) {}

    /**
     * @brief Type/SubType enumeration
     */
    enum class Type : uint8_t
    {
        EchoReply = 0x00,
        DestinationUnreachable = 0x03,
        SourceQuench = 0x04,
        RedirectMessage = 0x05,
        Echo = 0x08,
        RouterAdvertisement = 0x09,
        RouterSolicitation = 0x0A,
        TimeExceeded = 0x0B,
        BadIPHeader = 0x0C,
        Timestamp = 0x0D,
        TimestampReply = 0x0E,
        InformationRequest = 0x0F,
        InformationReply = 0x10,
        AddressMaskRequest = 0x11,
        AddressMaskReply = 0x12,
        ExtendedEchoRequest = 0x2A,
        ExtendedEchoReply = 0x2B
    };

    enum class Code : uint16_t
    {
        EchoReply = 0x0000,
        DestinationNetworkUnreachable = 0x0300,
        DestinationHostUnreachable = 0x0301,
        DestinationProtocolUnreachable = 0x0302,
        DestinationPortUnreachable = 0x0303,
        FragmentationRequired = 0x0304,
        SourceRouteFailed = 0x0305,
        DestinationNetworkUnknown = 0x0306,
        DestinationHostUnknown = 0x0307,
        SourceHostIsolated = 0x0308,
        NetworkAdministrativelyProhibited = 0x0309,
        HostAdministrativelyProhibited = 0x030A,
        NetworkUnreachableForToS = 0x030B,
        HostUnreachableForToS = 0x030C,
        CommunicationAdministrativelyProhibited = 0x030D,
        HostPrecedenceViolation = 0x030E,
        PrecedenceCutoffInEffect = 0x030F,
        SourceQuench = 0x0400, /* deprecated */
        RedirectDatagramForTheNetwork = 0x0500,
        RedirectDatagramForTheHost = 0x0501,
        RedirectDatagramForTheToSNetwork = 0x0502,
        RedirectDatagramForTheToSHost = 0x0503,
        AlternateHostAddress = 0x0600, /* deprecated */
        Echo = 0x0800,
        RouterAdvertisement = 0x0900,
        RouterSolicitation = 0x0A00,
        TTLExpiredInTransit = 0x0B00,
        FragmentReassemblyTimeExceeded = 0x0B01,
        BadIPHeader = 0x0C00,
        BadIPHeaderMissingRequiredOption = 0x0C01,
        BadIPHeaderLength = 0x0C02,
        Timestamp = 0x0D00,
        TimestampReply = 0x0E00,
        InformationRequest = 0x0F00, /* deprecated */
        InformationReply = 0x1000,   /* deprecated */
        AddressMaskRequest = 0x1100, /* deprecated */
        AddressMaskReply = 0x1200,   /* deprecated */
        ExtendedEchoRequest = 0x2A00,
        ExtendedEchoReply = 0x2B00,
        ExtendedEchoMalformedQuery = 0x2B01,
        ExtendedEchoNoSuchInterface = 0x2B02,
        ExtendedEchoNoSuchTableEntry = 0x2B03,
        ExtendedEchoMultipleInterfacesSatisfyQuery = 0x2B04,
    };

    /**
     * @brief Get the message Type associated with the given code
     */
    static inline Type getType(Code code)
    {
        return static_cast<Type>((enum_cast(code) >> 8) & 0xFF);
    }

    /**
     * @brief get message type
     * @throws ccut::Exception when called on invalid message
     * @return Type
     */
    Code code() const;

    /**
     * @brief get message type
     * @throws ccut::Exception when called on invalid message
     * @return Type
     */
    inline Type type() const { return getType(code()); }

    /**
     * @brief set the message type
     * @nothrow
     */
    ICMPMessage &setCode(Code code);

    /**
     * @brief retrieve checksum in packet
     * @details use `prepare` to update the checksum
     *
     * @return uint16_t
     */
    uint16_t checksum() const;

    /**
     * @brief update checksum and prepare packet
     */
    ICMPMessage &prepare();

    bool isValid() const override;

    inline data_t payload() const { return data.sub(MinDataSize); }

    static constexpr size_t MinDataSize = 8;

protected:
    void init(size_t payloadSize = 0);
};

std::string to_string(ICMPMessage::Code code);

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPMessage::Code code);

std::string to_string(ICMPMessage::Type type);

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPMessage::Type type);

} // namespace net
} // namespace ccut

#endif