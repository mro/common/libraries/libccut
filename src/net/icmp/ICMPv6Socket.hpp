/*
** Copyright (C) 2024 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-01-02T09:03:52
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef ICMPV6SOCKET_HPP__
#define ICMPV6SOCKET_HPP__

#include "../Socket.hpp"

namespace ccut {
namespace net {

struct ICMPv6Message;

class ICMPv6Socket : public Socket<ICMPv6Message>
{
public:
    explicit ICMPv6Socket(const std::string &name = "ICMPv6Socket");
    virtual ~ICMPv6Socket() = default;

    /**
     * @brief check if socket is opened in raw mode
     * @return true if socket is opened in RAW mode
     * @return false if socket is opened in DGRAM (Linux only) or closed
     */
    bool isRaw() const;

protected:
    int makeSocket(bool v6) override;
};

} // namespace net
} // namespace ccut

#endif