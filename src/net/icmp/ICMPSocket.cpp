/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-14T16:19:08
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "ICMPSocket.hpp"

#include <cstdint>
#include <cstring>
#include <map>
#include <type_traits>

#include <netinet/in.h>
#include <sys/socket.h>

#include "../../Exception.hpp"
#include "../../utils.hpp"
#include "../Addr.hpp"
#include "../raw/RawIPMessage.hpp"
#include "ICMPMessage.hpp"
#include "ICMPv6Message.hpp"
#include "ICMPv6Socket.hpp"

namespace ccut {
namespace net {

static const std::string s_logCat{"ccut:net:icmp"};

static bool isRawSocket(int sock)
{
    int type = -1;
    socklen_t len = sizeof(type);
    if (getsockopt(sock, SOL_SOCKET, SO_TYPE, &type, &len) != 0)
        throw ccut::make_errno_exception();

    return type == SOCK_RAW;
}

ICMPSocket::ICMPSocket(const std::string &name) : Socket(name), m_isRaw{false}
{}

bool ICMPSocket::isRaw()
{
    if (m_socket < 0)
        return false;

    return isRawSocket(m_socket);
}

int ICMPSocket::makeSocket(bool ipv6)
{
    int ret;

    if (ipv6)
        throw Exception(ErrorCode::Runtime,
                        "ICMP not supported in ipv6, use `bind4`");

    m_isRaw = true;
    ret = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (ret < 0 && errno == EPERM)
    {
        /* in DATAGRAM mode only echo requests are permitted, seqNum and
         * identifier, along with checksum will be overriden by kernel */
        ret = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
        if (ret >= 0)
        {
            m_isRaw = false;
            logger::warning(s_logCat)
                << "failed to open RAW socket, falling back on DGRAM, ICMP "
                   "support will be limited";
            logger::info(s_logCat) << "try `setcap cap_net_raw=ep <exe>` on "
                                      "run this process as root";
        }
        else
            errno = EPERM; // restore initial error
    }

    return ret;
}

ICMPSocket::Message ICMPSocket::processMessage(const address_t &from,
                                               const address_t &to,
                                               const cow_ptr<buffer_t> &buffer)
{
    logger::debug("ccut:net") << "message received " << from << " -> " << to
                              << " size:" << (buffer ? buffer->size() : -1);
    if (m_isRaw)
    {
        RawIPv4Message raw(from, to, buffer);
        if (!raw)
            logger::error("ccut:net") << "invalid raw ipv4 packet";
        else if (raw.protocol() != RawIPv4Message::Protocol::ICMP)
            logger::error("ccut:net")
                << "invalid protocol for ICMP message: " << raw.protocol();
        else
            return raw.message<ICMPMessage>();
    }
    else
        return ICMPMessage(from, to, buffer);

    return ICMPMessage();
}

ICMPv6Socket::ICMPv6Socket(const std::string &name) : Socket(name) {}

bool ICMPv6Socket::isRaw() const
{
    if (m_socket < 0)
        return false;

    return isRawSocket(m_socket);
}

int ICMPv6Socket::makeSocket(bool ipv6)
{
    int ret;

    if (!ipv6)
        throw Exception(ErrorCode::Runtime,
                        "ICMPv6 not supported in ipv4, use `bind6`");

    ret = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if (ret < 0 && errno == EPERM)
    {
        /* in DATAGRAM mode only echo requests are permitted, seqNum and
         * identifier, along with checksum will be overriden by kernel */
        ret = socket(AF_INET6, SOCK_DGRAM, IPPROTO_ICMPV6);
        if (ret >= 0)
        {
            logger::warning(s_logCat)
                << "failed to open RAW socket, falling back on DGRAM, ICMP "
                   "support will be limited";
            logger::info(s_logCat) << "try `setcap cap_net_raw=ep <exe>` on "
                                      "run this process as root";
        }
        else
            errno = EPERM; // restore initial error
    }

    return ret;
}

} // namespace net
} // namespace ccut