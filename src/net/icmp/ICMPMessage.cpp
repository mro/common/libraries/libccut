/*
** Copyright (C) 2024 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-01-02T08:53:59
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "ICMPMessage.hpp"

#include <map>

#include "../../Exception.hpp"
#include "ICMPEchoMessage.hpp"

namespace ccut {
namespace net {

static const std::string s_logCat{"ccut:net:icmp"};

#define TYPE_OFFSET 0
#define CODE_OFFSET 1
#define CKSUM_OFFSET 2
#define IDENTIFIER_OFFSET 4
#define SEQNUM_OFFSET 6

constexpr size_t ICMPMessage::MinDataSize;

static const std::map<ICMPMessage::Type, std::string> s_typeToStr{{
    {ICMPMessage::Type::EchoReply, "EchoReply"},
    {ICMPMessage::Type::DestinationUnreachable, "DestinationUnreachable"},
    {ICMPMessage::Type::SourceQuench, "SourceQuench"},
    {ICMPMessage::Type::RedirectMessage, "RedirectMessage"},
    {ICMPMessage::Type::Echo, "Echo"},
    {ICMPMessage::Type::RouterAdvertisement, "RouterAdvertisement"},
    {ICMPMessage::Type::RouterSolicitation, "RouterSolicitation"},
    {ICMPMessage::Type::TimeExceeded, "TimeExceeded"},
    {ICMPMessage::Type::BadIPHeader, "BadIPHeader"},
    {ICMPMessage::Type::Timestamp, "Timestamp"},
    {ICMPMessage::Type::TimestampReply, "TimestampReply"},
    {ICMPMessage::Type::InformationRequest, "InformationRequest"},
    {ICMPMessage::Type::InformationReply, "InformationReply"},
    {ICMPMessage::Type::AddressMaskRequest, "AddressMaskRequest"},
    {ICMPMessage::Type::AddressMaskReply, "AddressMaskReply"},
    {ICMPMessage::Type::ExtendedEchoRequest, "ExtendedEchoRequest"},
    {ICMPMessage::Type::ExtendedEchoReply, "ExtendedEchoReply"},
}};

static const std::map<ICMPMessage::Code, std::string> s_codeToStr{{
    {ICMPMessage::Code::EchoReply, "EchoReply"},
    {ICMPMessage::Code::DestinationNetworkUnreachable,
     "DestinationNetworkUnreachable"},
    {ICMPMessage::Code::DestinationHostUnreachable,
     "DestinationHostUnreachable"},
    {ICMPMessage::Code::DestinationProtocolUnreachable,
     "DestinationProtocolUnreachable"},
    {ICMPMessage::Code::DestinationPortUnreachable,
     "DestinationPortUnreachable"},
    {ICMPMessage::Code::FragmentationRequired, "FragmentationRequired"},
    {ICMPMessage::Code::SourceRouteFailed, "SourceRouteFailed"},
    {ICMPMessage::Code::DestinationNetworkUnknown, "DestinationNetworkUnknown"},
    {ICMPMessage::Code::DestinationHostUnknown, "DestinationHostUnknown"},
    {ICMPMessage::Code::SourceHostIsolated, "SourceHostIsolated"},
    {ICMPMessage::Code::NetworkAdministrativelyProhibited,
     "NetworkAdministrativelyProhibited"},
    {ICMPMessage::Code::HostAdministrativelyProhibited,
     "HostAdministrativelyProhibited"},
    {ICMPMessage::Code::NetworkUnreachableForToS, "NetworkUnreachableForToS"},
    {ICMPMessage::Code::HostUnreachableForToS, "HostUnreachableForToS"},
    {ICMPMessage::Code::CommunicationAdministrativelyProhibited,
     "CommunicationAdministrativelyProhibited"},
    {ICMPMessage::Code::HostPrecedenceViolation, "HostPrecedenceViolation"},
    {ICMPMessage::Code::PrecedenceCutoffInEffect, "PrecedenceCutoffInEffect"},
    {ICMPMessage::Code::SourceQuench, "SourceQuench"},
    {ICMPMessage::Code::RedirectDatagramForTheNetwork,
     "RedirectDatagramForTheNetwork"},
    {ICMPMessage::Code::RedirectDatagramForTheHost,
     "RedirectDatagramForTheHost"},
    {ICMPMessage::Code::RedirectDatagramForTheToSNetwork,
     "RedirectDatagramForTheToSNetwork"},
    {ICMPMessage::Code::RedirectDatagramForTheToSHost,
     "RedirectDatagramForTheToSHost"},
    {ICMPMessage::Code::AlternateHostAddress, "AlternateHostAddress"},
    {ICMPMessage::Code::Echo, "Echo"},
    {ICMPMessage::Code::RouterAdvertisement, "RouterAdvertisement"},
    {ICMPMessage::Code::RouterSolicitation, "RouterSolicitation"},
    {ICMPMessage::Code::TTLExpiredInTransit, "TTLExpiredInTransit"},
    {ICMPMessage::Code::FragmentReassemblyTimeExceeded,
     "FragmentReassemblyTimeExceeded"},
    {ICMPMessage::Code::BadIPHeader, "BadIPHeader"},
    {ICMPMessage::Code::BadIPHeaderMissingRequiredOption,
     "BadIPHeaderMissingRequiredOption"},
    {ICMPMessage::Code::BadIPHeaderLength, "BadIPHeaderLength"},
    {ICMPMessage::Code::Timestamp, "Timestamp"},
    {ICMPMessage::Code::TimestampReply, "TimestampReply"},
    {ICMPMessage::Code::InformationRequest, "InformationRequest"},
    {ICMPMessage::Code::InformationReply, "InformationReply"},
    {ICMPMessage::Code::AddressMaskRequest, "AddressMaskRequest"},
    {ICMPMessage::Code::AddressMaskReply, "AddressMaskReply"},
    {ICMPMessage::Code::ExtendedEchoRequest, "ExtendedEchoRequest"},
    {ICMPMessage::Code::ExtendedEchoReply, "ExtendedEchoReply"},
    {ICMPMessage::Code::ExtendedEchoMalformedQuery,
     "ExtendedEchoMalformedQuery"},
    {ICMPMessage::Code::ExtendedEchoNoSuchInterface,
     "ExtendedEchoNoSuchInterface"},
    {ICMPMessage::Code::ExtendedEchoNoSuchTableEntry,
     "ExtendedEchoNoSuchTableEntry"},
    {ICMPMessage::Code::ExtendedEchoMultipleInterfacesSatisfyQuery,
     "ExtendedEchoMultipleInterfacesSatisfyQuery"},
}};

std::string to_string(ICMPMessage::Code code)
{
    const std::map<ICMPMessage::Code, std::string>::const_iterator it =
        s_codeToStr.find(code);
    return (it != s_codeToStr.cend()) ? it->second : std::string();
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPMessage::Code code)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(code)};
    if (value.empty())
        return (logger << "Code(" << logger::hex(enum_cast(code), true) << ")");
    else
        return (logger << value);
}

std::string to_string(ICMPMessage::Type type)
{
    const std::map<ICMPMessage::Type, std::string>::const_iterator it =
        s_typeToStr.find(type);
    return (it != s_typeToStr.cend()) ? it->second : std::string();
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPMessage::Type type)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(type)};
    if (value.empty())
        return (logger << "Type(" << logger::hex(enum_cast(type), true) << ")");
    else
        return (logger << value);
}

ICMPMessage::Code ICMPMessage::code() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid ICMP message");

    return static_cast<ICMPMessage::Code>(data.read<uint16_t>(0, true));
}

ICMPMessage &ICMPMessage::setCode(Code code)
{
    init();

    data.write<uint16_t>(0, enum_cast(code), true);
    return *this;
}

bool ICMPMessage::isValid() const
{
    if (!Message::isValid())
        return false;
    else if (!data || data.size() < MinDataSize)
        return false;
    else
    {
        uint16_t ret = net::cksum(data.data(), data.size());
        if (ret != 0)
        {
            logger::warning(s_logCat)
                << "invalid ICMP checksum: " << checksum();
            return false;
        }
    }
    return true;
}

void ICMPMessage::init(size_t payloadSize)
{
    if (!data)
        data = make_cow<buffer_t>(MinDataSize + payloadSize);
    else if (data.size() < MinDataSize + payloadSize)
    {
        data.buffer()->resize(MinDataSize + payloadSize);
        data.reset(data.buffer()); /* use the whole buffer */
    }
}

uint16_t ICMPMessage::checksum() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid ICMP message");

    return data.read<uint16_t>(CKSUM_OFFSET,
                               (BYTE_ORDER == LITTLE_ENDIAN) ? false : true);
}

ICMPMessage &ICMPMessage::prepare()
{
    if (data && data.size() >= MinDataSize)
    {
        data.write<uint16_t>(CKSUM_OFFSET, 0);
        uint16_t sum = net::cksum(make_const(data).data(), data.size());

        /** always write it in native byte ordering */
        data.write<uint16_t>(CKSUM_OFFSET, sum,
                             (BYTE_ORDER == LITTLE_ENDIAN) ? false : true);
    }
    return *this;
}

ICMPEchoMessage::ICMPEchoMessage(const address_t &to,
                                 uint16_t identifier,
                                 uint16_t seqNum,
                                 size_t payloadSize) :
    ICMPMessage(to)
{
    init(payloadSize);
    setCode(Code::Echo);
    setIdentifier(identifier);
    setSequenceNum(seqNum);
}

uint16_t ICMPEchoMessage::identifier() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid ICMP message");

    return (data[IDENTIFIER_OFFSET] << 8) | (data[IDENTIFIER_OFFSET + 1]);
}

ICMPEchoMessage &ICMPEchoMessage::setIdentifier(uint16_t value)
{
    init();

    data.write<uint16_t>(IDENTIFIER_OFFSET, value, true);
    return *this;
}

uint16_t ICMPEchoMessage::sequenceNum() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid ICMP message");

    return (data[SEQNUM_OFFSET] << 8) | (data[SEQNUM_OFFSET + 1]);
}

ICMPEchoMessage &ICMPEchoMessage::setSequenceNum(uint16_t value)
{
    init();

    data.write<uint16_t>(SEQNUM_OFFSET, value, true);
    return *this;
}

bool ICMPEchoMessage::isValid() const
{
    if (!ICMPMessage::isValid())
        return false;
    else
    {
        Type t = type();
        return (t == Type::Echo || t == Type::EchoReply);
    };
}

} // namespace net
} // namespace ccut
