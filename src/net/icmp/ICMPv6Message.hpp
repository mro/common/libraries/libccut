/*
** Copyright (C) 2024 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-01-02T08:51:13
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef ICMPV6MESSAGE_HPP__
#define ICMPV6MESSAGE_HPP__

#include "../Message.hpp"

namespace ccut {
namespace net {

struct ICMPv6Message : public net::Message
{
    using Message::Message;

    ICMPv6Message() = default;

    /**
     * @brief downcast a Message to an ICMPv6Message
     */
    // cppcheck-suppress noExplicitConstructor
    ICMPv6Message(const Message &message) : Message(message) {}

    /**
     * @brief Type/SubType enumeration
     */
    enum class Type : uint8_t
    {
        DestinationUnreachable = 0x1,
        PacketTooBig = 0x2,
        TimeExceeded = 0x3,
        ParameterProblem = 0x4,
        EchoRequest = 0x80,
        EchoReply = 0x81,
        MulticastListenerQuery = 0x82,
        MulticastListenerReport = 0x83,
        MulticastListenerDone = 0x84,
        RouterSolicitation = 0x85,
        RouterAdvertisement = 0x86,
        NeighborSolicitation = 0x87,
        NeighborAdvertisement = 0x88,
        RedirectMessage = 0x89,
        RouterRenumbering = 0x8A,
        InformationQuery = 0x8B,
        InformationResponse = 0x8C,
        InverseNeighborDiscoverySolicitation = 0x8D,
        InverseNeighborDiscoveryAdvertisement = 0x8E,
        MulticastListenerDiscoveryReport = 0x8F,
        HomeAgentAddressDiscoveryRequest = 0x90,
        HomeAgentAddressDiscoveryReply = 0x91,
        MobilePrefixSolicitation = 0x92,
        MobilePrefixAdvertisement = 0x93,
        CertificationPathSolicitation = 0x94,
        CertificationPathAdvertisement = 0x95,
        MulticastRouterAdvertisement = 0x96,
        MulticastRouterSolicitation = 0x97,
        MulticastRouterTermination = 0x98,
        RPLControlMessage = 0x99
    };

    enum class Code : uint16_t
    {
        NoRouteToDestination = 0x0100,
        CommunicationProhibited = 0x0101,
        BeyondScopeOfSourceAddress = 0x0102,
        AddressUnreachable = 0x0103,
        PortUnreachable = 0x0104,
        SourceAddressFailedPolicy = 0x0105,
        RejectRouteToDestination = 0x0106,
        ErrorInSourceRoutingHeader = 0x0107,
        PacketTooBig = 0x0200,
        HopLimitExceeded = 0x0300,
        FragmentReassemblyTimeExceeded = 0x0301,
        ErroneousHeaderFieldEncountered = 0x0400,
        UnrecognizedNextHeaderTypeEncountered = 0x0401,
        UnrecognizedIPv6OptionEncountered = 0x0402,
        EchoRequest = 0x8000,
        EchoReply = 0x8100,
        MulticastListenerQuery = 0x8200,
        MulticastListenerReport = 0x8300,
        MulticastListenerDone = 0x8400,
        RouterSolicitation = 0x8500,
        RouterAdvertisement = 0x8600,
        NeighborSolicitation = 0x8700,
        NeighborAdvertisement = 0x8800,
        RedirectMessage = 0x8900,
        RouterRenumberingCommand = 0x8A00,
        RouterRenumberingResult = 0x8A01,
        RouterRenumberingSequenceNumberReset = 0x8AFF,
        InformationQueryAddress = 0x8B00,
        InformationQueryName = 0x8B01,
        InformationQueryAddress4 = 0x8B02,
        InformationResponse = 0x8C00,
        InformationResponseDenied = 0x8C01,
        InformationResponseUnknown = 0x8C02,
        InverseNeighborDiscoverySolicitation = 0x8D00,
        InverseNeighborDiscoveryAdvertisement = 0x8E00,
        MulticastListenerDiscoveryReport = 0x8F00,
        HomeAgentAddressDiscoveryRequest = 0x9000,
        HomeAgentAddressDiscoveryReply = 0x9100,
        MobilePrefixSolicitation = 0x9200,
        MobilePrefixAdvertisement = 0x9300,
        CertificationPathSolicitation = 0x9400,
        CertificationPathAdvertisement = 0x9500,
        MulticastRouterAdvertisement = 0x9600,
        MulticastRouterSolicitation = 0x9700,
        MulticastRouterTermination = 0x9800,
        RPLControlMessage = 0x9900
    };

    /**
     * @brief Get the message Type associated with the given code
     */
    static inline Type getType(Code code)
    {
        return static_cast<Type>((enum_cast(code) >> 8) & 0xFF);
    }

    /**
     * @brief get the message type and code
     * @throws ccut::Exception when called on invalid message
     * @return Code
     */
    Code code() const;

    /**
     * @brief get message type
     * @throws ccut::Exception when called on invalid message
     * @return Type
     */
    inline Type type() const { return getType(code()); }

    /**
     * @brief set the message type
     * @nothrow
     */
    ICMPv6Message &setCode(Code code);

    /**
     * @brief retrieve checksum in packet
     * @details use `prepare` to update the checksum
     *
     * @return uint16_t
     */
    uint16_t checksum() const;

    /**
     * @brief update checksum and prepare packet
     */
    ICMPv6Message &prepare();

    bool isValid() const override;

    inline data_t payload() const { return data.sub(MinDataSize); }

    static constexpr size_t MinDataSize = 8;

protected:
    void init();
    uint16_t pseudoHeaderChecksum() const;
};

std::string to_string(ICMPv6Message::Code code);

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPv6Message::Code code);

std::string to_string(ICMPv6Message::Type type);

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPv6Message::Type type);

} // namespace net
} // namespace ccut

#endif