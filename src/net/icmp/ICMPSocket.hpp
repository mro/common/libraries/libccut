/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-12T19:29:24
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef ICMPSOCKET_HPP__
#define ICMPSOCKET_HPP__

#include "../Socket.hpp"

namespace ccut {
namespace net {

struct ICMPMessage;

/**
 * @brief ICMP communication socket
 * @details this socket can only be used in ipv4
 * @details to use the entire ICMP protocol your exe will require
 * `cap_net_raw=ep` permission: `setcap cap_net_raw=ep {exe}`
 */
class ICMPSocket : public Socket<ICMPMessage>
{
public:
    explicit ICMPSocket(const std::string &name = "ICMPSocket");
    virtual ~ICMPSocket() = default;

    /**
     * @brief check if socket is opened in raw mode
     * @return true if socket is opened in RAW mode
     * @return false if socket is opened in DGRAM (Linux only) or closed
     */
    bool isRaw();

protected:
    int makeSocket(bool v6) override;
    Message processMessage(const address_t &from,
                           const address_t &to,
                           const cow_ptr<buffer_t> &buffer) override;

    bool m_isRaw;
};

} // namespace net
} // namespace ccut

#endif