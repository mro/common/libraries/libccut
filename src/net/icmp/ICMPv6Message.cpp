/*
** Copyright (C) 2024 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2024-01-02T08:58:31
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "ICMPv6Message.hpp"

#include <map>

#include "../../Exception.hpp"
#include "../raw/RawIPMessage.hpp"

namespace ccut {
namespace net {

static const std::string s_logCat{"ccut:net:icmp"};

constexpr size_t ICMPv6Message::MinDataSize;

#define TYPE_OFFSET 0
#define CODE_OFFSET 1
#define CKSUM_OFFSET 2
#define IDENTIFIER_OFFSET 4
#define SEQNUM_OFFSET 6

static const std::map<ICMPv6Message::Code, std::string> s_codeToStr{
    {{ICMPv6Message::Code::NoRouteToDestination, "NoRouteToDestination"},
     {ICMPv6Message::Code::CommunicationProhibited, "CommunicationProhibited"},
     {ICMPv6Message::Code::BeyondScopeOfSourceAddress,
      "BeyondScopeOfSourceAddress"},
     {ICMPv6Message::Code::AddressUnreachable, "AddressUnreachable"},
     {ICMPv6Message::Code::PortUnreachable, "PortUnreachable"},
     {ICMPv6Message::Code::SourceAddressFailedPolicy,
      "SourceAddressFailedPolicy"},
     {ICMPv6Message::Code::RejectRouteToDestination, "RejectRouteToDestination"},
     {ICMPv6Message::Code::ErrorInSourceRoutingHeader,
      "ErrorInSourceRoutingHeader"},
     {ICMPv6Message::Code::PacketTooBig, "PacketTooBig"},
     {ICMPv6Message::Code::HopLimitExceeded, "HopLimitExceeded"},
     {ICMPv6Message::Code::FragmentReassemblyTimeExceeded,
      "FragmentReassemblyTimeExceeded"},
     {ICMPv6Message::Code::ErroneousHeaderFieldEncountered,
      "ErroneousHeaderFieldEncountered"},
     {ICMPv6Message::Code::UnrecognizedNextHeaderTypeEncountered,
      "UnrecognizedNextHeaderTypeEncountered"},
     {ICMPv6Message::Code::UnrecognizedIPv6OptionEncountered,
      "UnrecognizedIPv6OptionEncountered"},
     {ICMPv6Message::Code::EchoRequest, "EchoRequest"},
     {ICMPv6Message::Code::EchoReply, "EchoReply"},
     {ICMPv6Message::Code::MulticastListenerQuery, "MulticastListenerQuery"},
     {ICMPv6Message::Code::MulticastListenerReport, "MulticastListenerReport"},
     {ICMPv6Message::Code::MulticastListenerDone, "MulticastListenerDone"},
     {ICMPv6Message::Code::RouterSolicitation, "RouterSolicitation"},
     {ICMPv6Message::Code::RouterAdvertisement, "RouterAdvertisement"},
     {ICMPv6Message::Code::NeighborSolicitation, "NeighborSolicitation"},
     {ICMPv6Message::Code::NeighborAdvertisement, "NeighborAdvertisement"},
     {ICMPv6Message::Code::RedirectMessage, "RedirectMessage"},
     {ICMPv6Message::Code::RouterRenumberingCommand, "RouterRenumberingCommand"},
     {ICMPv6Message::Code::RouterRenumberingResult, "RouterRenumberingResult"},
     {ICMPv6Message::Code::RouterRenumberingSequenceNumberReset,
      "RouterRenumberingSequenceNumberReset"},
     {ICMPv6Message::Code::InformationQueryAddress, "InformationQueryAddress"},
     {ICMPv6Message::Code::InformationQueryName, "InformationQueryName"},
     {ICMPv6Message::Code::InformationQueryAddress4, "InformationQueryAddress4"},
     {ICMPv6Message::Code::InformationResponse, "InformationResponse"},
     {ICMPv6Message::Code::InformationResponseDenied,
      "InformationResponseDenied"},
     {ICMPv6Message::Code::InformationResponseUnknown,
      "InformationResponseUnknown"},
     {ICMPv6Message::Code::InverseNeighborDiscoverySolicitation,
      "InverseNeighborDiscoverySolicitation"},
     {ICMPv6Message::Code::InverseNeighborDiscoveryAdvertisement,
      "InverseNeighborDiscoveryAdvertisement"},
     {ICMPv6Message::Code::MulticastListenerDiscoveryReport,
      "MulticastListenerDiscoveryReport"},
     {ICMPv6Message::Code::HomeAgentAddressDiscoveryRequest,
      "HomeAgentAddressDiscoveryRequest"},
     {ICMPv6Message::Code::HomeAgentAddressDiscoveryReply,
      "HomeAgentAddressDiscoveryReply"},
     {ICMPv6Message::Code::MobilePrefixSolicitation, "MobilePrefixSolicitation"},
     {ICMPv6Message::Code::MobilePrefixAdvertisement,
      "MobilePrefixAdvertisement"},
     {ICMPv6Message::Code::CertificationPathSolicitation,
      "CertificationPathSolicitation"},
     {ICMPv6Message::Code::CertificationPathAdvertisement,
      "CertificationPathAdvertisement"},
     {ICMPv6Message::Code::MulticastRouterAdvertisement,
      "MulticastRouterAdvertisement"},
     {ICMPv6Message::Code::MulticastRouterSolicitation,
      "MulticastRouterSolicitation"},
     {ICMPv6Message::Code::MulticastRouterTermination,
      "MulticastRouterTermination"},
     {ICMPv6Message::Code::RPLControlMessage, "RPLControlMessage"}}};

static const std::map<ICMPv6Message::Type, std::string> s_typeToStr{
    {ICMPv6Message::Type::DestinationUnreachable, "DestinationUnreachable"},
    {ICMPv6Message::Type::PacketTooBig, "PacketTooBig"},
    {ICMPv6Message::Type::TimeExceeded, "TimeExceeded"},
    {ICMPv6Message::Type::ParameterProblem, "ParameterProblem"},
    {ICMPv6Message::Type::EchoRequest, "EchoRequest"},
    {ICMPv6Message::Type::EchoReply, "EchoReply"},
    {ICMPv6Message::Type::MulticastListenerQuery, "MulticastListenerQuery"},
    {ICMPv6Message::Type::MulticastListenerReport, "MulticastListenerReport"},
    {ICMPv6Message::Type::MulticastListenerDone, "MulticastListenerDone"},
    {ICMPv6Message::Type::RouterSolicitation, "RouterSolicitation"},
    {ICMPv6Message::Type::RouterAdvertisement, "RouterAdvertisement"},
    {ICMPv6Message::Type::NeighborSolicitation, "NeighborSolicitation"},
    {ICMPv6Message::Type::NeighborAdvertisement, "NeighborAdvertisement"},
    {ICMPv6Message::Type::RedirectMessage, "RedirectMessage"},
    {ICMPv6Message::Type::RouterRenumbering, "RouterRenumbering"},
    {ICMPv6Message::Type::InformationQuery, "InformationQuery"},
    {ICMPv6Message::Type::InformationResponse, "InformationResponse"},
    {ICMPv6Message::Type::InverseNeighborDiscoverySolicitation,
     "InverseNeighborDiscoverySolicitation"},
    {ICMPv6Message::Type::InverseNeighborDiscoveryAdvertisement,
     "InverseNeighborDiscoveryAdvertisement"},
    {ICMPv6Message::Type::MulticastListenerDiscoveryReport,
     "MulticastListenerDiscoveryReport"},
    {ICMPv6Message::Type::HomeAgentAddressDiscoveryRequest,
     "HomeAgentAddressDiscoveryRequest"},
    {ICMPv6Message::Type::HomeAgentAddressDiscoveryReply,
     "HomeAgentAddressDiscoveryReply"},
    {ICMPv6Message::Type::MobilePrefixSolicitation, "MobilePrefixSolicitation"},
    {ICMPv6Message::Type::MobilePrefixAdvertisement,
     "MobilePrefixAdvertisement"},
    {ICMPv6Message::Type::CertificationPathSolicitation,
     "CertificationPathSolicitation"},
    {ICMPv6Message::Type::CertificationPathAdvertisement,
     "CertificationPathAdvertisement"},
    {ICMPv6Message::Type::MulticastRouterAdvertisement,
     "MulticastRouterAdvertisement"},
    {ICMPv6Message::Type::MulticastRouterSolicitation,
     "MulticastRouterSolicitation"},
    {ICMPv6Message::Type::MulticastRouterTermination,
     "MulticastRouterTermination"},
    {ICMPv6Message::Type::RPLControlMessage, "RPLControlMessage"},
};

std::string to_string(ICMPv6Message::Code code)
{
    const std::map<ICMPv6Message::Code, std::string>::const_iterator it =
        s_codeToStr.find(code);
    return (it != s_codeToStr.cend()) ? it->second : std::string();
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPv6Message::Code code)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(code)};
    if (value.empty())
        return (logger << "Code(" << logger::hex(enum_cast(code), true) << ")");
    else
        return (logger << value);
}

std::string to_string(ICMPv6Message::Type type)
{
    const std::map<ICMPv6Message::Type, std::string>::const_iterator it =
        s_typeToStr.find(type);
    return (it != s_typeToStr.cend()) ? it->second : std::string();
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 ICMPv6Message::Type type)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(type)};
    if (value.empty())
        return (logger << "Type(" << logger::hex(enum_cast(type), true) << ")");
    else
        return (logger << value);
}

std::string to_string(ICMPv6Message::Type type);

ICMPv6Message::Code ICMPv6Message::code() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Invalid ICMPv6 message");

    return static_cast<ICMPv6Message::Code>(data.read<uint16_t>(0, true));
}

ICMPv6Message &ICMPv6Message::setCode(Code code)
{
    init();

    data.write<uint16_t>(0, enum_cast(code), true);
    return *this;
}

bool ICMPv6Message::isValid() const
{
    if (!Message::isValid())
        return false;
    else if (!data || data.size() < MinDataSize)
        return false;
    else if (!from.isValid() || !from.isV6() || !to.isValid() || !to.isV6())
        return false;
    else
    {
        uint16_t ret = net::cksum(data.data(), data.size(),
                                  pseudoHeaderChecksum());
        if (ret != 0)
        {
            logger::warning(s_logCat)
                << "invalid ICMPv6 checksum: " << logger::hex(checksum(), true);
            return false;
        }
    }
    return true;
}

void ICMPv6Message::init()
{
    if (!data)
        data = make_cow<buffer_t>(MinDataSize);
    else if (data.size() < MinDataSize)
    {
        data.buffer()->resize(MinDataSize);
        data.reset(data.buffer()); /* use the whole buffer */
    }
}

uint16_t ICMPv6Message::checksum() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Invalid ICMPv6 message");

    return data.read<uint16_t>(CKSUM_OFFSET,
                               (BYTE_ORDER == LITTLE_ENDIAN) ? false : true);
}

uint16_t ICMPv6Message::pseudoHeaderChecksum() const
{
    uint16_t ret;

    if (!from.isValid() || !from.isV6())
        throw ccut::Exception(
            ccut::ErrorCode::Runtime,
            "Can't prepare ICMPv6 message: `from` not an ipv6 address");
    else if (!to.isValid() || !to.isV6())
        throw ccut::Exception(
            ccut::ErrorCode::Runtime,
            "Can't prepare ICMPv6 message: `to` not an ipv6 address");

    ret = net::cksum(
        &reinterpret_cast<const sockaddr_in6 *>(from.addr())->sin6_addr, 16);
    ret = net::cksum(
        &reinterpret_cast<const sockaddr_in6 *>(to.addr())->sin6_addr, 16, ret);

    uint32_t value = htobe<uint32_t>(data.size());
    ret = net::cksum(&value, 4, ret);

    value = htobe<uint32_t>(enum_cast(Protocol::ICMPv6));
    ret = net::cksum(&value, 4, ret);

    return ret;
}

ICMPv6Message &ICMPv6Message::prepare()
{
    if (data && data.size() >= MinDataSize)
    {
        data.write<uint16_t>(CKSUM_OFFSET, 0);

        uint16_t sum = pseudoHeaderChecksum();
        sum = net::cksum(data.cbegin(), data.cend(), sum);

        /** always write it in native byte ordering */
        data.write<uint16_t>(CKSUM_OFFSET, sum,
                             (BYTE_ORDER == LITTLE_ENDIAN) ? false : true);
    }
    return *this;
}

} // namespace net
} // namespace ccut