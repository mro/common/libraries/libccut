/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T09:50:18
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Addr.hpp"

#include <algorithm>
#include <chrono>
#include <cstring>
#include <thread>
#include <vector>

#include <arpa/inet.h>
#include <logger/Logger.hpp>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../Exception.hpp"

namespace ccut {
namespace net {

static struct sockaddr *make4(const std::string &str, uint16_t port)
{
    sockaddr_in *ret = new sockaddr_in;
    memset(ret, 0, sizeof(sockaddr_in));
    if (inet_pton(AF_INET, str.data(), &ret->sin_addr) <= 0)
    {
        logger::error("ccut::net") << "invalid ipv4 addr: " << str;
        delete ret;
        return nullptr;
    }
    ret->sin_family = AF_INET;
    ret->sin_port = htons(port);
    return reinterpret_cast<sockaddr *>(ret);
}

static struct sockaddr *make6(const std::string &str, uint16_t port)
{
    sockaddr_in6 *ret = new sockaddr_in6;
    memset(ret, 0, sizeof(sockaddr_in6));
    if (inet_pton(AF_INET6, str.data(), &ret->sin6_addr) <= 0)
    {
        logger::error("ccut::net") << "invalid ipv6 addr: " << str;
        delete ret;
        return nullptr;
    }
    ret->sin6_family = AF_INET6;
    ret->sin6_port = htons(port);
    return reinterpret_cast<sockaddr *>(ret);
}

Addr::Addr(const std::string &addr, uint16_t port)
{
    if (addr.find('.') != std::string::npos)
        m_addr.reset(make4(addr, port));
    else
        m_addr.reset(make6(addr, port));
}

Addr::Addr(int inet, const std::string &addr, uint16_t port)
{
    if (inet == AF_INET6)
        m_addr.reset(make6(addr, port));
    else
        m_addr.reset(make4(addr, port));
}

bool Addr::operator==(const Addr &other) const
{
    if (!m_addr)
        return !other.m_addr;
    else if (!other.m_addr)
        return false;
    return (m_addr->sa_family == other.m_addr->sa_family) &&
        (port() == other.port()) && (host() == other.host());
}

void Addr::detach()
{
    if (!m_addr || m_addr.use_count() <= 1)
        return;
    switch (m_addr->sa_family)
    {
    case AF_INET:
    {
        struct sockaddr_in *sock = new sockaddr_in;
        *sock = reinterpret_cast<const sockaddr_in &>(*m_addr);
        m_addr.reset(reinterpret_cast<sockaddr *>(sock));
        break;
    }
    case AF_INET6:
    {
        struct sockaddr_in6 *sock = new sockaddr_in6;
        *sock = reinterpret_cast<const sockaddr_in6 &>(*m_addr);
        m_addr.reset(reinterpret_cast<sockaddr *>(sock));
        break;
    }
    default: break;
    }
}

uint16_t Addr::port() const
{
    if (!m_addr)
        return 0;
    switch (m_addr->sa_family)
    {
    case AF_INET:
        return ntohs(reinterpret_cast<const sockaddr_in &>(*m_addr).sin_port);
    case AF_INET6:
        return ntohs(reinterpret_cast<const sockaddr_in6 &>(*m_addr).sin6_port);
    default: return 0;
    }
}

std::string Addr::host() const
{
    if (!m_addr)
        return std::string();
    switch (m_addr->sa_family)
    {
    case AF_INET:
    {
        std::vector<char> buf;
        buf.reserve(INET_ADDRSTRLEN);
        const char *ret = inet_ntop(
            AF_INET,
            &reinterpret_cast<const sockaddr_in *>(m_addr.get())->sin_addr,
            buf.data(), buf.capacity());
        return ret ? std::string(ret) : std::string();
    }
    case AF_INET6:
    {
        std::vector<char> buf;
        buf.reserve(INET6_ADDRSTRLEN);
        const char *ret = inet_ntop(
            AF_INET6,
            &reinterpret_cast<const sockaddr_in6 *>(m_addr.get())->sin6_addr,
            buf.data(), buf.capacity());
        return ret ? std::string(ret) : std::string();
    }
    default: return std::string();
    }
}

size_t Addr::size() const
{
    if (!m_addr)
        return 0;
    switch (m_addr->sa_family)
    {
    case AF_INET: return sizeof(sockaddr_in);
    case AF_INET6: return sizeof(sockaddr_in6);
    default: return 0;
    }
}

bool Addr::isV6() const
{
    if (!m_addr)
        return false;
    switch (m_addr->sa_family)
    {
    case AF_INET: return false;
    case AF_INET6: return true;
    default: return false;
    }
}

bool Addr::toBuffer(buffer_view_t<cow_ptr<buffer_t>> &buffer) const
{
    if (!m_addr)
        return false;

    switch (m_addr->sa_family)
    {
    case AF_INET:
        if (buffer.size() < 4)
            return false;
        memcpy(buffer.data(),
               &reinterpret_cast<const sockaddr_in *>(m_addr.get())->sin_addr,
               4);
        return true;
    case AF_INET6:
        if (buffer.size() < 16)
            return false;
        memcpy(buffer.data(),
               &reinterpret_cast<const sockaddr_in6 *>(m_addr.get())->sin6_addr,
               16);
        return true;
    default: return false;
    }
}

cow_ptr<buffer_t> Addr::toBuffer() const
{
    if (!m_addr)
        return cow_ptr<buffer_t>();

    switch (m_addr->sa_family)
    {
    case AF_INET:
    {
        buffer_view_t<cow_ptr<buffer_t>> ret{make_cow<buffer_t>(4, 0)};
        if (toBuffer(ret))
            return ret.buffer();
    }
    case AF_INET6:
    {
        buffer_view_t<cow_ptr<buffer_t>> ret{make_cow<buffer_t>(16, 0)};
        if (toBuffer(ret))
            return ret.buffer();
    }
    }
    return cow_ptr<buffer_t>();
}

Addr::Addr(const buffer_view_t<cow_ptr<buffer_t>> &buffer)
{
    *this = buffer;
}

Addr &Addr::operator=(const buffer_view_t<cow_ptr<buffer_t>> &buffer)
{
    if (buffer.size() >= 16)
    {
        detach();
        sockaddr_in6 *ret;
        if (!m_addr || m_addr->sa_family != AF_INET6)
        {
            ret = new sockaddr_in6;
            m_addr.reset(reinterpret_cast<sockaddr *>(ret));
        }
        else /* already IPv6, let's optimize */
            ret = reinterpret_cast<sockaddr_in6 *>(m_addr.get());

        memset(ret, 0, sizeof(sockaddr_in6));
        memcpy(&ret->sin6_addr, buffer.data(), 16);
        ret->sin6_family = AF_INET6;
        ret->sin6_port = 0;
    }
    else if (buffer.size() >= 4)
    {
        detach();
        sockaddr_in *ret;
        if (!m_addr || m_addr->sa_family != AF_INET)
        {
            ret = new sockaddr_in;
            m_addr.reset(reinterpret_cast<sockaddr *>(ret));
        }
        else
            ret = reinterpret_cast<sockaddr_in *>(m_addr.get());

        memset(ret, 0, sizeof(sockaddr_in));
        memcpy(&ret->sin_addr.s_addr, buffer.data(), 4);
        ret->sin_family = AF_INET;
        ret->sin_port = 0;
    }
    else
        *this = Addr();
    return *this;
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 const ccut::net::Addr &addr)
{
    if (logger.isEnabled())
    {
        if (!addr.isValid())
            logger << "invalid";
        else if (addr.addr()->sa_family == AF_INET6)
            // IPv6 URI notation as per RFC3986
            logger << "[" << addr.host() << "]:" << addr.port();
        else
            logger << addr.host() << ":" << addr.port();
    }
    return logger;
}

} // namespace net
} // namespace ccut