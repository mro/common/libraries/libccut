/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-12-20T16:11:52
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef CCUT_NET_UDPSOCKET_HPP__
#define CCUT_NET_UDPSOCKET_HPP__

#include <cstdint>
#include <memory>

#include <netinet/udp.h>

#include "Addr.hpp"
#include "Message.hpp"
#include "Socket.hpp"

namespace ccut {
namespace net {

struct UDPMessage : public Message
{
    using Message::Message;

    /**
     * @brief Promote an Message to UDPMessage
     * @param message the message to promote
     */
    // cppcheck-suppress noExplicitConstructor
    UDPMessage(const Message &message) : Message(message) {}
};

/**
 * @brief IPv4 UDP server implementation
 * @details runs its own thread and emits messages event, do not forget to
 * start the thread
 *
 */
class UDPSocket : public Socket<UDPMessage>
{
public:
    explicit UDPSocket(const std::string &name = "UDPSocket") : Socket(name) {}
    virtual ~UDPSocket() = default;

protected:
    int makeSocket(bool v6) override;
};

} // namespace net
} // namespace ccut

#endif
