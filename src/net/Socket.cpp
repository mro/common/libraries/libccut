/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T13:52:15
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "Socket.hpp"

#include <arpa/inet.h>

namespace ccut {
namespace net {

BaseSocket::BaseSocket(const std::string &name) :
    ccut::Thread(name),
    m_socket{-1},
    m_bufferSize{defaultBufferSize}
{}

net::address_t BaseSocket::addr() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_addr;
}

void BaseSocket::setBufferSize(size_t size)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_bufferSize = size;
}

size_t BaseSocket::getBufferSize() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_bufferSize;
}

void BaseSocket::wake()
{
    m_wakePipe.write("X");
    Thread::wake();
}

void BaseSocket::bind(uint16_t port, bool localhost)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_socket >= 0)
    {
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              m_name + " already bound");
    }

    m_socket = makeSocket(false);
    if (m_socket < 0)
        throw ccut::make_errno_exception();

    m_addr = net::address_t(AF_INET,
                            localhost ? "127.0.0.1" : "255.255.255.255", port);
    if (::bind(m_socket, m_addr.addr(), m_addr.size()) < 0)
    {
        ccut::Exception err = ccut::make_errno_exception();
        m_addr = net::address_t();
        ::close(m_socket);
        m_socket = -1;
        logger::error("ccut:net:socket") << "failed to bind: " << err.what();
        throw err;
    }
    m_addr.detach();
    socklen_t len = m_addr.size();
    getsockname(m_socket, const_cast<sockaddr *>(m_addr.addr()), &len);
    lock.unlock();
    wake();
}

void BaseSocket::bind6(uint16_t port, bool localhost)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_socket >= 0)
    {
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              m_name + " already bound");
    }

    m_socket = makeSocket(true);
    if (m_socket < 0)
        throw ccut::make_errno_exception();

    m_addr = net::address_t(AF_INET6, localhost ? "::1" : "::", port);
    if (::bind(m_socket, m_addr.addr(), m_addr.size()) < 0)
    {
        ccut::Exception err = ccut::make_errno_exception();
        m_addr = net::address_t();
        ::close(m_socket);
        m_socket = -1;
        logger::error("ccut:net:socket") << "failed to bind: " << err.what();
        throw err;
    }
    m_addr.detach();
    socklen_t len = m_addr.size();
    getsockname(m_socket, const_cast<sockaddr *>(m_addr.addr()), &len);
    lock.unlock();
    wake();
}

} // namespace net
} // namespace ccut
