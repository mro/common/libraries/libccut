/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-21T18:12:24
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "RawIPMessage.hpp"

#include <cstdint>
#include <cstring>

#include <netinet/in.h>

#include "../../Exception.hpp"

namespace ccut {
namespace net {

#define IPVERS_OFFSET 0
#define IPV4_TOTAL_LENGTH_OFFSET 2
#define IPV4_TTL_OFFSET 8
#define IPV4_PROTO_OFFSET 9
#define IPV4_CKSUM_OFFSET 10
#define IPV4_SRC_OFFSET 12
#define IPV4_DEST_OFFSET 16

#define IPV6_PAYLOAD_LENGTH_OFFSET 4
#define IPV6_NEXT_HEADER_OFFSET 6
#define IPV6_HOP_LIMIT_OFFSET 7
#define IPV6_SRC_OFFSET 8
#define IPV6_DEST_OFFSET 24

static const std::string s_logCat{"ccut:net:raw"};
constexpr size_t RawIPv4Message::MinDataSize;

std::string to_string(Protocol proto)
{
    switch (proto)
    {
    case Protocol::ICMP: return "ICMP";
    case Protocol::IGMP: return "IGMP";
    case Protocol::IPinIP: return "IPinIP";
    case Protocol::TCP: return "TCP";
    case Protocol::IGP: return "IGP";
    case Protocol::UDP: return "UDP";
    case Protocol::RDP: return "RDP";
    case Protocol::IPv6: return "IPv6";
    case Protocol::ICMPv6: return "ICMPv6";
    case Protocol::RAW: return "RAW";
    default: return std::string();
    }
}

const logger::Logger &operator<<(const logger::Logger &logger, Protocol proto)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(proto)};
    if (value.empty())
        return (logger << "Protocol{" << logger::hex(enum_cast(proto), true)
                       << "}");
    else
        return (logger << value);
}

std::string to_string(RawIPv6Message::ECN ecn)
{
    switch (ecn)
    {
    case RawIPv6Message::ECN::NoECT: return "NoECT";
    case RawIPv6Message::ECN::ECNCapableTransport1:
        return "ECNCapableTransport1";
    case RawIPv6Message::ECN::ECNCapableTransport2:
        return "ECNCapableTransport2";
    case RawIPv6Message::ECN::CongestionExperienced:
        return "CongestionExperienced";
    default: return std::string();
    }
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 RawIPv6Message::ECN ecn)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(ecn)};
    if (value.empty())
        return (logger << "ECN{" << logger::hex(enum_cast(ecn), true) << "}");
    else
        return (logger << value);
}

std::string to_string(RawIPv6Message::DSCP dscp)
{
    switch (dscp)
    {
    case RawIPv6Message::DSCP::DefaultForwarding: return "DefaultForwarding";
    case RawIPv6Message::DSCP::ExpeditedForwarding:
        return "ExpeditedForwarding";
    case RawIPv6Message::DSCP::CS1: return "CS1";
    case RawIPv6Message::DSCP::CS2: return "CS2";
    case RawIPv6Message::DSCP::CS3: return "CS3";
    case RawIPv6Message::DSCP::CS4: return "CS4";
    case RawIPv6Message::DSCP::CS5: return "CS5";
    case RawIPv6Message::DSCP::CS6: return "CS6";
    case RawIPv6Message::DSCP::CS7: return "CS7";
    case RawIPv6Message::DSCP::AF11: return "AF11";
    case RawIPv6Message::DSCP::AF12: return "AF12";
    case RawIPv6Message::DSCP::AF13: return "AF13";
    case RawIPv6Message::DSCP::AF21: return "AF21";
    case RawIPv6Message::DSCP::AF22: return "AF22";
    case RawIPv6Message::DSCP::AF23: return "AF23";
    case RawIPv6Message::DSCP::AF31: return "AF31";
    case RawIPv6Message::DSCP::AF32: return "AF32";
    case RawIPv6Message::DSCP::AF33: return "AF33";
    case RawIPv6Message::DSCP::AF41: return "AF41";
    case RawIPv6Message::DSCP::AF42: return "AF42";
    case RawIPv6Message::DSCP::AF43: return "AF43";
    default: return std::string();
    }
}

const logger::Logger &operator<<(const logger::Logger &logger,
                                 RawIPv6Message::DSCP dscp)
{
    if (!logger.isEnabled())
        return logger;
    const std::string &value{to_string(dscp)};
    if (value.empty())
        return (logger << "DSCP{" << logger::hex(enum_cast(dscp), true) << "}");
    else
        return (logger << value);
}

RawIPv4Message::RawIPv4Message(const Addr &from,
                               const Addr &to,
                               Protocol proto,
                               size_t payloadSize) :
    Message(from, to)
{
    init(payloadSize);
    setProtocol(proto);
}

RawIPv4Message::RawIPv4Message(const Addr &to,
                               Protocol proto,
                               size_t payloadSize) :
    RawIPv4Message(Addr(), to, proto, payloadSize)
{}

RawIPv4Message::RawIPv4Message(Protocol proto, size_t payloadSize) :
    RawIPv4Message(Addr(), Addr(), proto, payloadSize)
{}

RawIPv4Message RawIPv4Message::load(const data_t &buffer)
{
    RawIPv4Message ret;
    ret.data = buffer;
    if (ret)
    {
        ret.from = ret.ipSrc();
        ret.to = ret.ipDest();
    }
    return ret;
}

size_t getRawIPVersion(const Message::data_t &buffer)
{
    if (buffer && buffer.size() > 1)
        return (buffer.read(0) >> 4) & 0xF;
    else
        return 0;
}

void RawIPv4Message::prepare()
{
    if (data && data.size() >= MinDataSize)
    {
        setTotalLength(data.size());
        setIpSrc(from);
        setIpDest(to);
        data.write<uint16_t>(IPV4_CKSUM_OFFSET, 0);
        uint16_t sum = net::cksum(make_const(data).data(),
                                  ipHeaderLength() * sizeof(uint32_t));
        data.write<uint16_t>(IPV4_CKSUM_OFFSET, sum,
                             (BYTE_ORDER == LITTLE_ENDIAN) ? false : true);
    }
}

uint8_t RawIPv4Message::ipVersion() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return (data.read(IPVERS_OFFSET) >> 4) & 0xF;
}

uint8_t RawIPv4Message::ipHeaderLength() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read(IPVERS_OFFSET) & 0xF;
}
uint8_t RawIPv4Message::differentiatedServicesCodePoint() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return (data.read(1) >> 2) & 0x3F;
}

uint8_t RawIPv4Message::explicitCongestionNotification() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return (data.read(1)) & 0x3;
}

uint16_t RawIPv4Message::totalLength() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read<uint16_t>(IPV4_TOTAL_LENGTH_OFFSET, true);
}

RawIPv4Message &RawIPv4Message::setTotalLength(uint16_t totalLength)
{
    init();

    data.write<uint16_t>(IPV4_TOTAL_LENGTH_OFFSET, totalLength, true);
    return *this;
}

uint16_t RawIPv4Message::ipIdentification() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read<uint16_t>(4, true);
}

RawIPv4Message &RawIPv4Message::setIpIdentification(uint16_t identification)
{
    init();

    data.write<uint16_t>(4, identification, true);
    return *this;
}

RawIPv4Message::IPFlags RawIPv4Message::ipFlags() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return IPFlags((data.read(6) >> 5) & 0x7);
}

RawIPv4Message &RawIPv4Message::setIpFlags(IPFlags flags)
{
    init();

    uint8_t value = ((flags.bits() << 5) & 0xE0) |
        (data.read<uint8_t>(6) & 0x1F);
    data.write<uint8_t>(6, value);
    return *this;
}

uint16_t RawIPv4Message::ipFragmentOffset() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read<uint16_t>(6, true) & 0x1FFF;
}

RawIPv4Message &RawIPv4Message::setIpFragmentOffset(uint16_t offset)
{
    init();

    uint16_t value = (offset & 0x1FFF) | (data.read<uint16_t>(6, true) & 0xE000);
    data.write<uint16_t>(6, value, true);
    return *this;
}

uint8_t RawIPv4Message::timeToLive() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read(IPV4_TTL_OFFSET);
}

RawIPv4Message &RawIPv4Message::setTimeToLive(uint8_t ttl)
{
    init();

    data.write<uint8_t>(IPV4_TTL_OFFSET, ttl);
    return *this;
}

RawIPv4Message::Protocol RawIPv4Message::protocol() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return static_cast<Protocol>(data.read(IPV4_PROTO_OFFSET));
}

RawIPv4Message &RawIPv4Message::setProtocol(Protocol proto)
{
    init();

    data.write<uint8_t>(IPV4_PROTO_OFFSET, enum_cast(proto));
    return *this;
}

uint16_t RawIPv4Message::ipHeaderChecksum() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return data.read<uint16_t>(IPV4_CKSUM_OFFSET, true);
}

Addr RawIPv4Message::ipSrc() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return Addr::fromBuffer(data.sub(IPV4_SRC_OFFSET, 4));
}

RawIPv4Message &RawIPv4Message::setIpSrc(const Addr &from)
{
    init();

    if (!from.isValid())
        data.write<uint32_t>(IPV4_SRC_OFFSET, 0, true);
    else if (from.isV6())
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Can't set an IPv6 address in IPv4 message");
    else
    {
        memcpy(
            &data[IPV4_SRC_OFFSET],
            &reinterpret_cast<const sockaddr_in *>(from.addr())->sin_addr.s_addr,
            4);
    }
    return *this;
}

Addr RawIPv4Message::ipDest() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv4 message");
    return Addr::fromBuffer(data.sub(IPV4_DEST_OFFSET, 4));
}

RawIPv4Message &RawIPv4Message::setIpDest(const Addr &from)
{
    init();

    if (!from.isValid())
        data.write<uint32_t>(IPV4_DEST_OFFSET, 0, true);
    else if (from.isV6())
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Can't set an IPv6 address in IPv4 message");
    else
    {
        memcpy(
            &data[IPV4_DEST_OFFSET],
            &reinterpret_cast<const sockaddr_in *>(from.addr())->sin_addr.s_addr,
            4);
    }
    return *this;
}

bool RawIPv4Message::isValid() const
{
    if (!Message::isValid())
        return false;
    else if (!data || data.size() < MinDataSize || ipVersion() != 4)
        return false;
    else
    {
        uint16_t ret = cksum(data.data(), ipHeaderLength() * 4);
        if (ret != 0)
        {
            logger::warning(s_logCat) << "invalid IPv4 checksum: " << ret;
            return false;
        }
    }
    return true;
}

Message::data_t RawIPv4Message::payload() const
{
    return data.sub(ipHeaderLength() * 4);
}

void RawIPv4Message::init(size_t payloadSize)
{
    if (!data)
        data = make_cow<buffer_t>(MinDataSize + payloadSize);
    else if (data.size() < MinDataSize + payloadSize)
    {
        data.buffer()->resize(MinDataSize + payloadSize);
        data.reset(data.buffer());
    }
    else
        return;

    data.write<uint8_t>(IPVERS_OFFSET, 0x45); // VERS=4 IHL=5
    data.write<uint8_t>(IPV4_TTL_OFFSET, 0xFF);
}

RawIPv6Message::RawIPv6Message(const Addr &from,
                               const Addr &to,
                               Protocol proto,
                               size_t payloadSize) :
    Message(from, to)
{
    init(payloadSize);
    setNextHeader(proto);
}

RawIPv6Message::RawIPv6Message(const Addr &to,
                               Protocol proto,
                               size_t payloadSize) :
    RawIPv6Message(Addr(), to, proto, payloadSize)
{}

RawIPv6Message::RawIPv6Message(Protocol proto, size_t payloadSize) :
    RawIPv6Message(Addr(), Addr(), proto, payloadSize)
{}

RawIPv6Message RawIPv6Message::load(const data_t &buffer)
{
    RawIPv6Message ret;
    ret.data = buffer;
    if (ret)
    {
        ret.from = ret.ipSrc();
        ret.to = ret.ipDest();
    }
    return ret;
}

void RawIPv6Message::prepare()
{
    if (data && data.size() >= MinDataSize)
    {
        setPayloadLength(data.size() - MinDataSize);
        setIpSrc(from);
        setIpDest(to);
    }
}

uint8_t RawIPv6Message::ipVersion() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return (data.read(IPVERS_OFFSET) >> 4) & 0xF;
}

RawIPv6Message::ECN RawIPv6Message::explicitCongestionNotification() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return static_cast<ECN>((data.read(1) >> 4) & 0x3);
}

RawIPv6Message &RawIPv6Message::setExplicitCongestionNotification(ECN ecn)
{
    init();

    uint8_t value = data.read(1);
    value = ((enum_cast(ecn) << 4) & 0x30) | (value & ~0x30);

    data.write<uint8_t>(1, value);
    return *this;
}

RawIPv6Message::DSCP RawIPv6Message::differentiatedServicesCodePoint() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return static_cast<DSCP>((data.read<uint16_t>(0, true) >> 6) & 0x3F);
}

RawIPv6Message &RawIPv6Message::setDifferentiatedServicesCodePoint(DSCP dscp)
{
    init();

    uint16_t value = data.read<uint16_t>(0, true);
    value = ((enum_cast(dscp) << 6) & 0x0FC0) | (value & ~0x0FC0);

    data.write<uint16_t>(0, value, true);
    return *this;
}

uint8_t RawIPv6Message::trafficClass() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return (data.read<uint16_t>(IPVERS_OFFSET, true) >> 4) & 0xFF;
}

uint32_t RawIPv6Message::flowLabel() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return data.read<uint32_t>(IPVERS_OFFSET, true) & 0xFFFFF;
}

RawIPv6Message &RawIPv6Message::setFlowLabel(uint32_t flowLabel)
{
    init();
    uint32_t value = data.read<uint32_t>(0, true);
    value = (value & 0xFFF00000) | (flowLabel & 0xFFFFF);

    data.write<uint32_t>(0, value, true);
    return *this;
}

uint16_t RawIPv6Message::payloadLength() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return data.read<uint16_t>(IPV6_PAYLOAD_LENGTH_OFFSET, true);
}

RawIPv6Message &RawIPv6Message::setPayloadLength(uint16_t value)
{
    init();

    data.write<uint16_t>(4, value, true);
    return *this;
}

Protocol RawIPv6Message::nextHeader() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return static_cast<Protocol>(data.read(IPV6_NEXT_HEADER_OFFSET));
}

RawIPv6Message &RawIPv6Message::setNextHeader(Protocol proto)
{
    init();

    data.write<uint8_t>(IPV6_NEXT_HEADER_OFFSET, enum_cast(proto));
    return *this;
}

uint8_t RawIPv6Message::hopLimit() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return data.read(IPV6_HOP_LIMIT_OFFSET);
}

RawIPv6Message &RawIPv6Message::setHopLimit(uint8_t limit)
{
    init();

    data.write<uint8_t>(IPV6_HOP_LIMIT_OFFSET, limit);
    return *this;
}

Addr RawIPv6Message::ipSrc() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return Addr::fromBuffer(data.sub(IPV6_SRC_OFFSET, 16));
}

RawIPv6Message &RawIPv6Message::setIpSrc(const Addr &from)
{
    init();

    if (!from.isValid())
        memset(&data[IPV6_SRC_OFFSET], 0, 16);
    else if (!from.isV6())
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Can't set an IPv4 address in IPv6 message");
    else
    {
        memcpy(&data[IPV6_SRC_OFFSET],
               &reinterpret_cast<const sockaddr_in6 *>(from.addr())->sin6_addr,
               16);
    }
    return *this;
}

Addr RawIPv6Message::ipDest() const
{
    if (!data || data.size() < MinDataSize)
        throw ccut::Exception(ccut::ErrorCode::Runtime, "Invalid IPv6 message");
    return Addr::fromBuffer(data.sub(IPV6_DEST_OFFSET, 16));
}

RawIPv6Message &RawIPv6Message::setIpDest(const Addr &from)
{
    init();

    if (!from.isValid())
        memset(&data[IPV6_DEST_OFFSET], 0, 16);
    else if (!from.isV6())
        throw ccut::Exception(ccut::ErrorCode::Runtime,
                              "Can't set an IPv4 address in IPv6 message");
    else
    {
        memcpy(&data[IPV6_DEST_OFFSET],
               &reinterpret_cast<const sockaddr_in6 *>(from.addr())->sin6_addr,
               16);
    }
    return *this;
}

bool RawIPv6Message::isValid() const
{
    if (!Message::isValid())
        return false;
    else if (!data || data.size() < MinDataSize || ipVersion() != 6)
        return false;

    return true;
}

Message::data_t RawIPv6Message::payload() const
{
    return data.sub(MinDataSize);
}

void RawIPv6Message::init(size_t payloadSize)
{
    if (!data)
        data = make_cow<buffer_t>(MinDataSize + payloadSize);
    else if (data.size() < MinDataSize + payloadSize)
    {
        data.buffer()->resize(MinDataSize + payloadSize);
        data.reset(data.buffer());
    }
    else
        return;

    data.write<uint8_t>(IPVERS_OFFSET, 0x60); // VERS=6
    data.write<uint8_t>(IPV6_HOP_LIMIT_OFFSET, 0xFF);
}

} // namespace net
} // namespace ccut