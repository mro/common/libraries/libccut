/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-21T17:55:46
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef RAWIPSOCKET_HPP__
#define RAWIPSOCKET_HPP__

#include "../Socket.hpp"
#include "RawIPMessage.hpp"

namespace ccut {
namespace net {

/**
 * @brief raw socket for IPv4 communication
 * @details providing a protocol is required to receive messages, Protocol::RAW
 * makes the socket write only (see man-page raw(7)).
 *
 */
class RawIPv4Socket : public Socket<RawIPv4Message>
{
public:
    explicit RawIPv4Socket(Protocol proto,
                           const std::string &name = "IPv4Socket");
    virtual ~RawIPv4Socket() = default;

protected:
    /**
     * @param v6 must be false
     */
    int makeSocket(bool v6 = false) override;

    Message processMessage(const address_t &from,
                           const address_t &to,
                           const cow_ptr<buffer_t> &buffer) override;

    const Protocol m_proto;
};

/**
 * @brief raw socket for IPv6 communication
 *
 */
class RawIPv6Socket : public Socket<RawIPv6Message>
{
public:
    explicit RawIPv6Socket(Protocol proto,
                           const std::string &name = "IPv6Socket");
    virtual ~RawIPv6Socket() = default;

protected:
    /**
     * @param v6 must be false
     */
    int makeSocket(bool v6 = false) override;

    Message processMessage(const address_t &from,
                           const address_t &to,
                           const cow_ptr<buffer_t> &buffer) override;

    const Protocol m_proto;
};

} // namespace net
} // namespace ccut

#endif