/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-21T22:20:54
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "RawIPSocket.hpp"

#include <netinet/in.h>
#include <sys/socket.h>

#include "../../Exception.hpp"
#include "RawIPMessage.hpp"

namespace ccut {
namespace net {

static const std::string s_logCat{"ccut:net:raw"};

RawIPv4Socket::RawIPv4Socket(Protocol proto, const std::string &name) :
    Socket(name),
    m_proto(proto)
{}

int RawIPv4Socket::makeSocket(bool ipv6)
{
    if (ipv6)
        throw Exception(ErrorCode::Runtime,
                        "can't bind a v4 socket using bind6");

    int ret = socket(AF_INET, SOCK_RAW, enum_cast(m_proto));
    if (ret >= 0)
    {
        int value = 1;
        setsockopt(ret, IPPROTO_IP, IP_HDRINCL, &value, sizeof(value));
    }
    else if (errno == EPERM)
    {
        logger::error(s_logCat) << "failed to open RAW socket";
        logger::info(s_logCat) << "try `setcap cap_net_raw=ep <exe>` on "
                                  "run this process as root";
    }

    return ret;
}

RawIPv4Socket::Message RawIPv4Socket::processMessage(
    const address_t &from,
    const address_t &to,
    const cow_ptr<buffer_t> &buffer)
{
    logger::debug(s_logCat) << "(raw) message received " << from << " -> " << to
                            << " size:" << (buffer ? buffer->size() : -1);
    return Message(from, to, buffer);
}

RawIPv6Socket::RawIPv6Socket(Protocol proto, const std::string &name) :
    Socket(name),
    m_proto(proto)
{}

int RawIPv6Socket::makeSocket(bool ipv6)
{
    if (!ipv6)
        throw Exception(ErrorCode::Runtime,
                        "can't bind a v6 socket using bind4");

#ifndef IPV6_HDRINCL
        /* see
         * https://github.com/torvalds/linux/commit/715f504b118998c41a2079a17e16bf5a8a114885
         * for details
         */
#ifndef STATIC_ANALYSIS
#warning RawIPv6Socket not supported, kernel too old
#endif

    throw Exception(ErrorCode::NotSupported,
                    "Raw IPv6 not supported, kernel headers must be >=4.5");

#else
    int ret = socket(AF_INET6, SOCK_RAW, enum_cast(m_proto));
    if (ret >= 0)
    {
        int value = 1;
        setsockopt(ret, IPPROTO_IPV6, IPV6_HDRINCL, &value, sizeof(value));
    }
    else if (errno == EPERM)
    {
        logger::error(s_logCat) << "failed to open RAW socket";
        logger::info(s_logCat) << "try `setcap cap_net_raw=ep <exe>` on "
                                  "run this process as root";
    }

    return ret;
#endif
}

RawIPv6Socket::Message RawIPv6Socket::processMessage(
    const address_t &from,
    const address_t &to,
    const cow_ptr<buffer_t> &buffer)
{
    logger::debug(s_logCat) << "(raw) message received " << from << " -> " << to
                            << " size:" << (buffer ? buffer->size() : -1);
    /* even with IPV6_HDRINCL it seems that header is not present when receiving
     * packets, let's reconstruct (until a better solution is found)
     * FIXME: one should use recvmsg to better reconstruct this (adding hoplimit
     * and other fields) */
    Message ret(from, to);
    ret.data = make_cow<buffer_t>(RawIPv6Message::MinDataSize + buffer->size());
    ret.setNextHeader(m_proto);
    std::copy(buffer->cbegin(), buffer->cend(),
              ret.data.data() + RawIPv6Message::MinDataSize);
    ret.prepare();
    return ret;
}

} // namespace net
} // namespace ccut