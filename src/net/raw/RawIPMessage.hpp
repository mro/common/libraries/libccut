/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-21T18:09:00
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef RAWIPMESSAGE_HPP__
#define RAWIPMESSAGE_HPP__

#include <string>

#include "../../BitMask.hpp"
#include "../Message.hpp"

namespace ccut {
namespace net {

/**
 * @brief IP protocols enum
 */
enum class Protocol
{
    ICMP = 1,
    IGMP = 2,
    IPinIP = 4,
    TCP = 6,
    IGP = 9,
    UDP = 17,
    RDP = 27,
    IPv6 = 41, /* 6in4 and 6to4 */
    ICMPv6 = 58,
    RAW = 255
};

std::string to_string(Protocol proto);

const logger::Logger &operator<<(const logger::Logger &logger, Protocol proto);

size_t getRawIPVersion(const Message::data_t &buffer);

struct RawIPv4Message : public Message
{
public:
    typedef ccut::net::Protocol Protocol;

    using Message::Message;

    RawIPv4Message() = default;
    RawIPv4Message(const Addr &from,
                   const Addr &to,
                   Protocol proto,
                   size_t payloadSize);
    RawIPv4Message(const Addr &to, Protocol proto, size_t payloadSize);
    RawIPv4Message(Protocol proto, size_t payloadSize);

    enum class IPFlag
    {
        Reserved0 = 0x1,
        DontFragment = 0x2,
        MoreFragments = 0x4
    };
    typedef BitMask<IPFlag> IPFlags;

    static RawIPv4Message load(const data_t &buffer);

    bool isValid() const override;

    /**
     * @brief prepares the message for sending
     * @details write addresses and computes checksum
     */
    virtual void prepare();

    uint8_t ipVersion() const;
    uint8_t ipHeaderLength() const;
    uint8_t differentiatedServicesCodePoint() const;
    uint8_t explicitCongestionNotification() const;
    uint16_t totalLength() const;
    uint16_t ipIdentification() const;
    IPFlags ipFlags() const;
    uint16_t ipFragmentOffset() const;
    uint8_t timeToLive() const;
    Protocol protocol() const;
    uint16_t ipHeaderChecksum() const;
    Addr ipSrc() const;
    Addr ipDest() const;

    RawIPv4Message &setTotalLength(uint16_t length);
    RawIPv4Message &setIpIdentification(uint16_t identification);
    RawIPv4Message &setIpFlags(IPFlags flags);
    RawIPv4Message &setIpFragmentOffset(uint16_t offset);
    RawIPv4Message &setTimeToLive(uint8_t ttl);
    RawIPv4Message &setProtocol(Protocol proto);
    RawIPv4Message &setIpSrc(const Addr &from);
    RawIPv4Message &setIpDest(const Addr &to);

    /* FIXME: add IP options support later-on */

    /**
     * @brief get the message's payload
     *
     * @return data_t
     */
    data_t payload() const;

    /**
     * @brief construct a message for the associated payload
     * @details returned message may be invalid (depending on protocol)
     */
    template<typename Message>
    Message message()
    {
        return Message(from, to, payload());
    }

    static constexpr size_t MinDataSize = 20;

protected:
    void init(size_t payloadSize = 0);
};

struct RawIPv6Message : public Message
{
public:
    typedef ccut::net::Protocol Protocol;

    using Message::Message;

    RawIPv6Message() = default;
    RawIPv6Message(const Addr &from,
                   const Addr &to,
                   Protocol proto,
                   size_t payloadSize);
    RawIPv6Message(const Addr &to, Protocol proto, size_t payloadSize);
    RawIPv6Message(Protocol proto, size_t payloadSize);

    /**
     * @brief Explicit Congestion Notification (ECN) bits
     */
    enum class ExplicitCongestionNotification : uint8_t
    {
        NoECT = 0,
        ECNCapableTransport1 = 0x01,
        ECT1 = ECNCapableTransport1,
        ECNCapableTransport2 = 0x02,
        ECT2 = ECNCapableTransport2,
        CongestionExperienced = 0x3,
        CE = CongestionExperienced
    };
    typedef ExplicitCongestionNotification ECN;

    enum class DifferentiatedServicesCodePoint : uint8_t
    {
        DefaultForwarding = 0,
        DF = DefaultForwarding,
        ExpeditedForwarding = 46,
        EF = ExpeditedForwarding,
        CS0 = 0,
        CS1 = 8,
        CS2 = 16,
        CS3 = 24,
        CS4 = 32,
        CS5 = 40,
        CS6 = 48,
        CS7 = 56,
        AF11 = 10, // CS1 Assured Forwarding, low drop-rate
        AF12 = 12, // CS1 Assured Forwarding, medium drop-rate
        AF13 = 14, // CS1 Assured Forwarding, high drop-rate
        AF21 = 18, // CS2 Assured Forwarding, low drop-rate
        AF22 = 20, // CS2 Assured Forwarding, medium drop-rate
        AF23 = 22, // CS2 Assured Forwarding, high drop-rate
        AF31 = 26, // CS3 Assured Forwarding, low drop-rate
        AF32 = 28, // CS3 Assured Forwarding, medium drop-rate
        AF33 = 30, // CS3 Assured Forwarding, high drop-rate
        AF41 = 34, // CS4 Assured Forwarding, low drop-rate
        AF42 = 36, // CS4 Assured Forwarding, medium drop-rate
        AF43 = 38  // CS4 Assured Forwarding, high drop-rate
    };
    typedef DifferentiatedServicesCodePoint DSCP;

    /**
     * @brief Get the Class Selector level from DSCP
     */
    static inline uint8_t getClassSelector(DSCP dscp)
    {
        return (enum_cast(dscp) >> 3) & 0x7;
    }

    static RawIPv6Message load(const data_t &buffer);

    bool isValid() const override;

    /**
     * @brief prepares the message for sending
     * @details write addresses and computes checksum
     */
    virtual void prepare();

    uint8_t ipVersion() const;
    ECN explicitCongestionNotification() const;
    DSCP differentiatedServicesCodePoint() const;

    /**
     * @brief short-hand for DSCP + ECN fields
     */
    uint8_t trafficClass() const;
    uint32_t flowLabel() const;
    uint16_t payloadLength() const;

    Protocol nextHeader() const;
    uint8_t hopLimit() const;

    Addr ipSrc() const;
    Addr ipDest() const;

    RawIPv6Message &setExplicitCongestionNotification(ECN value);
    RawIPv6Message &setDifferentiatedServicesCodePoint(DSCP value);
    RawIPv6Message &setFlowLabel(uint32_t label);
    RawIPv6Message &setPayloadLength(uint16_t length);
    RawIPv6Message &setNextHeader(Protocol proto);
    RawIPv6Message &setHopLimit(uint8_t limit);
    RawIPv6Message &setIpSrc(const Addr &from);
    RawIPv6Message &setIpDest(const Addr &to);

    /* FIXME: add IP options support later-on */

    /**
     * @brief get the message's payload
     *
     * @return data_t
     */
    data_t payload() const;

    /**
     * @brief construct a message for the associated payload
     * @details returned message may be invalid (depending on protocol)
     */
    template<typename Message>
    Message message()
    {
        return Message(from, to, payload());
    }

    static constexpr size_t MinDataSize = 40;

protected:
    void init(size_t payloadSize = 0);
};

std::string to_string(RawIPv6Message::ExplicitCongestionNotification ecn);

const logger::Logger &operator<<(
    const logger::Logger &logger,
    RawIPv6Message::ExplicitCongestionNotification ecn);

std::string to_string(RawIPv6Message::DifferentiatedServicesCodePoint dscp);

const logger::Logger &operator<<(
    const logger::Logger &logger,
    RawIPv6Message::DifferentiatedServicesCodePoint dscp);

} // namespace net
} // namespace ccut

template<>
struct ccut_enable_bitmask<ccut::net::RawIPv4Message::IPFlag> : std::true_type
{};

#endif