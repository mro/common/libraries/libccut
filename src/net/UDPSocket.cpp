/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-12-20T16:22:21
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "UDPSocket.hpp"

#include <algorithm>
#include <chrono>
#include <cstring>
#include <thread>

#include <arpa/inet.h>
#include <logger/Logger.hpp>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../Exception.hpp"

namespace ccut {
namespace net {

int UDPSocket::makeSocket(bool ipv6)
{
    if (ipv6)
        return socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    else
        return socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
}

} // namespace net
} // namespace ccut
