/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T12:03:48
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef IPSOCKET_HPP__
#define IPSOCKET_HPP__

#include <cstdint>
#include <string>

#include "../Buffer.hpp"
#include "../Cow.hpp"
#include "../Pipe.hpp"
#include "../Signal.hpp"
#include "../Thread.hpp"
#include "Addr.hpp"

namespace ccut {

namespace net {

/**
 * @brief base class for sockets
 * @details do use the `Socket<Message>` template helper to implement your own
 * socket
 */
class BaseSocket : public ccut::Thread
{
public:
    BaseSocket(const std::string &name = "BaseSocket");

    void wake() override;

    /**
     * @brief bind the ICMPSocket
     * @details starts the processing thread on success
     *
     * @param port port to bind to (default: 0 == random)
     * @param localhost host to bind to (default loopback)
     * @throws ccut::Exception if already listening or on error
     */
    virtual void bind(uint16_t port = 0, bool localhost = true);
    virtual void bind6(uint16_t port = 0, bool localhost = true);

    /**
     * @brief current server address
     *
     * @return listening address, not valid if not listening
     */
    net::address_t addr() const;

    void setBufferSize(size_t size);
    size_t getBufferSize() const;
    int getSocket() const { return m_socket; }

    static constexpr size_t defaultBufferSize = 2048;

protected:
    virtual int makeSocket(bool v6 = false) = 0;

    ccut::Pipe m_wakePipe;
    int m_socket;
    net::address_t m_addr;
    size_t m_bufferSize;
};

/**
 * @brief Socket template helper
 */
template<typename M>
class Socket : public BaseSocket
{
public:
    explicit Socket(const std::string &name = "Socket");
    virtual ~Socket();

    typedef M Message;

    virtual void send(const Message &msg);

    ccut::Signal<const Message &> message;

protected:
    void thread_func() override;

    virtual Message processMessage(const address_t &from,
                                   const address_t &to,
                                   const cow_ptr<buffer_t> &buffer);
};

} // namespace net
} // namespace ccut

#include "Socket.hxx"

#endif