/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-14T15:34:26
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef MESSAGE_HPP__
#define MESSAGE_HPP__

#include <vector>

#include "../Buffer.hpp"
#include "../Cow.hpp"
#include "Addr.hpp"

namespace ccut {
namespace net {

/**
 * @brief base message for IP based communication
 *
 */
struct Message
{
    typedef buffer_view_t<cow_ptr<buffer_t>> data_t;

    Message() = default;
    explicit Message(const net::address_t &to) : to{to} {}

    Message(const net::address_t &from, const net::address_t &to) :
        from{from},
        to{to}
    {}
    Message(const Message &) = default;
    Message &operator=(const Message &) = default;
    virtual ~Message() = default;

    Message(const net::address_t &from,
            const net::address_t &to,
            const data_t &data) :
        from{from},
        to{to},
        data{data}
    {}

    template<typename iterator>
    Message(const net::address_t &from,
            const net::address_t &to,
            iterator start,
            iterator end) :
        from{from},
        to{to},
        data{make_cow<buffer_t>(start, end)}
    {}
    template<typename iterator>
    Message(const net::address_t &to, iterator start, iterator end) :
        Message(address_t(), to, start, end)
    {}

    inline Message makeReply() const { return Message{to, from}; }
    inline Message makeReply(const char *data, size_t len) const
    {
        return Message{to, from, make_cow<buffer_t>(data, data + len)};
    }

    /**
     * @brief check message's sanity
     * @details Messages are valid when both from/to are also valid
     */
    virtual bool isValid() const;
    inline operator bool() const { return isValid(); }

    static size_t getRawIPVersion(const Message::data_t &buffer);

    net::address_t from;
    net::address_t to;
    data_t data;
};

/**
 * @brief compute internet checksum over data.
 * @details as described in RFC1071
 * @param[in] data data to compute checksum on
 * @param[in] len data length in bytes
 * @param[in] initial initial cksum value
 * @return uint16_t checksum
 */
uint16_t cksum(const void *data, size_t len, uint16_t initial = 0xFFFF);

/**
 * @brief convenience function for internet checksum
 */
template<typename iterator>
uint16_t cksum(iterator begin, iterator end, uint16_t initial = 0xFFFF)
{
    return cksum(&(*begin), end - begin, initial);
}

} // namespace net
} // namespace ccut

#endif