/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-14T16:52:00+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include "utils.hpp"

#include <algorithm>
#include <cstdint>
#include <sstream>

#include <endian.h>
#include <logger/Logger.hpp>

#include "local-config.h"

namespace ccut {

static inline char to_hexdigit(int8_t num)
{
    if (num >= 10)
        return num - 10 + 'A';
    return num + '0';
}

static inline uint8_t from_hexdigit(char c)
{
    if (c >= 'A' && c <= 'Z')
        return c + 10 - 'A';
    else if (c >= 'a' && c <= 'z')
        return c + 10 - 'a';
    else if (c >= '0' && c <= '9')
        return c - '0';
    return 0xFF;
}

std::string ccutVersion(bool extraLong, bool log)
{
    std::string ret{PROJECT_VERSION};
    if (extraLong)
    {
        ret += "-";
        ret += GIT_SHORT;
        ret += GIT_DIRTY;
    }
    if (log)
        logger::info("ccut") << "libccut: " << ret;
    return ret;
}

std::vector<std::string> &split(const std::string &str,
                                char separator,
                                std::vector<std::string> &out)
{
    for (std::string::size_type pos = 0; pos != std::string::npos;)
    {
        std::string::size_type end = str.find(separator, pos);
        if (end == std::string::npos)
        {
            out.emplace_back(str, pos);
            pos = end;
        }
        else
        {
            out.emplace_back(str, pos, end - pos);
            pos = end + 1;
        }
    }
    return out;
}

std::vector<std::string> &split(const std::string::const_iterator &begin,
                                const std::string::const_iterator &end,
                                char separator,
                                std::vector<std::string> &out)
{
    if (end < begin)
        return out;

    std::string::const_iterator pos = begin;
    do
    {
        std::string::const_iterator it = std::find(pos, end, separator);
        if (it == end)
        {
            out.emplace_back(pos, end);
            pos = end;
        }
        else
        {
            out.emplace_back(pos, it);
            pos = ++it;
            if (pos == end)
                out.push_back(std::string());
        }
    } while (pos != end);
    return out;
}

std::string trim(const std::string &str)
{
    const std::string::size_type begin = str.find_first_not_of(" \t\r\n");
    if (begin == std::string::npos)
        return std::string();

    const std::string::size_type end = str.find_last_not_of(" \t\r\n");

    return str.substr(begin,
                      (end == std::string::npos) ? std::string::npos :
                                                   (end - begin + 1));
}

std::string urlencode(const std::string &str)
{
    std::ostringstream oss;
    for (char c : str)
    {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||
            (c >= '0' && c <= '9') || (c == '-') || (c == '_') || (c == '.') ||
            (c == '~'))
            oss << c;
        else
            oss << '%' << to_hexdigit((c >> 4) & 0x0F) << to_hexdigit(c & 0x0F);
    }
    return oss.str();
}

std::string urldecode(const std::string &str)
{
    std::ostringstream oss;
    for (std::size_t i = 0; i < str.size(); ++i)
    {
        char c = str[i];
        if (c == '%' && (i + 2 < str.size()))
        {
            int decoded = from_hexdigit(str[i + 1]) << 4;
            decoded += from_hexdigit(str[i + 2]);
            if (decoded <= 0x7F)
            {
                i += 2;
                c = static_cast<char>(decoded);
            }
        }
        oss << c;
    }
    return oss.str();
}

} // namespace ccut
