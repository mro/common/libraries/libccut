/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-25T11:07:41+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef CCUT_YML_HPP__
#define CCUT_YML_HPP__

#include <type_traits>

#include "Exception.hpp"
#include "ryml_internals.hpp"

namespace ccut {
namespace yml {

using namespace ryml;
using ryml::NodeRef;

inline Tree parse(const std::string &str)
{
    return yml::parse_in_arena(to_csubstr(str));
}

/**
 * @brief initialize rapidyaml library
 * @details sets proper callbacks
 */
void init();

namespace {
template<class T, class V>
struct default_to_
{
    T &val;
    const V def;
};
} // namespace
template<class T, class V>
inline default_to_<T, V> default_to(T &val, V v)
{
    return default_to_<T, V>{val, v};
}

template<class T, class V>
struct default_to_key_
{
    yml::Key<T> val;
    const V def;
};
template<class T, class V>
inline default_to_key_<T, V> default_to(yml::Key<T> val, V v)
{
    return default_to_key_<T, V>{val, v};
}

template<typename V>
typename std::enable_if<std::is_integral<V>::value, NodeRef>::type get(
    const NodeRef &n,
    V c)
{
    return (n.valid() && !n.is_seed() && n.is_seq()) ? n.child(c) : NodeRef();
}

template<typename V>
typename std::enable_if<!std::is_integral<V>::value, NodeRef>::type get(
    const NodeRef &n,
    V c)
{
    return (n.valid() && !n.is_seed() && n.is_map()) ?
        n.find_child(to_csubstr(c)) :
        NodeRef();
}

template<typename V, typename... T>
NodeRef get(const NodeRef &n, V arg, T... args)
{
    if (!n.valid())
        return NodeRef();
    return get(get(n, arg), args...);
}

Exception make_error(const Parser &parser,
                     const NodeRef &ref,
                     const std::string &message);

} // namespace yml
} // namespace ccut

template<class T, class V>
const ccut::yml::NodeRef &operator>>(const ccut::yml::NodeRef &ref,
                                     const ccut::yml::default_to_<T, V> &wrapper)
{
    if (ref.is_seed() || !ref.valid() || ref.get() == nullptr)
        wrapper.val = wrapper.def;
    else if (!read(ref, &wrapper.val))
        wrapper.val = wrapper.def;
    return ref;
}

template<class T, class V>
const ccut::yml::NodeRef &operator>>(
    const ccut::yml::NodeRef &ref,
    const ccut::yml::default_to_key_<T, V> &wrapper)
{
    if (ref.is_seed() || !ref.valid() || ref.get() == nullptr)
        wrapper.val.k = wrapper.def;
    else if (!from_chars(ref.key(), &wrapper.val.k))
        wrapper.val.k = wrapper.def;
    return ref;
}

#endif
