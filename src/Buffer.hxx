/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T11:12:51
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef BUFFER_HXX__
#define BUFFER_HXX__

#include "Buffer.hpp"

namespace ccut {

template<typename T>
Buffer &Buffer::write(size_t offset, const T &value, bool be)
{
    T *ptr = reinterpret_cast<T *>(data() + offset);
    if (be)
        *ptr = htobe(value);
    else
        *ptr = htole(value);
    return *this;
}

template<typename T>
T Buffer::read(size_t offset, bool be) const
{
    const T *ptr = reinterpret_cast<const T *>(data() + offset);
    if (be)
        return betoh(*ptr);
    else
        return letoh(*ptr);
}

// } // namespace ccut

template<typename T>
typename std::enable_if<
    std::is_same<ccut::buffer_t, typename std::decay<T>::type>::value,
    const logger::Logger &>::type
    operator<<(const logger::Logger &logger, const T &data)
{
    if (!logger.isEnabled())
        return logger;

    logger << "{";
    for (size_t i = 0; i < data.size(); ++i)
    {
        if (i)
            logger << " ";
        logger << logger::hex(data[i]);
        if (i > 20)
        {
            logger << "...";
            break;
        }
    }
    return logger << "}";
}

template<typename T>
typename std::enable_if<
    std::is_same<ccut::buffer_view_t<typename T::shared_buffer_type>,
                 typename std::decay<T>::type>::value,
    const logger::Logger &>::type
    operator<<(const logger::Logger &logger, const T &data)
{
    if (!logger.isEnabled())
        return logger;

    if (!data)
        return logger << "{invalid}";

    logger << "{";
    for (size_t i = 0; i < data.size(); ++i)
    {
        if (i)
            logger << " ";
        logger << logger::hex(data[i]);
        if (i > 20)
        {
            logger << "...";
            break;
        }
    }
    return logger << "}";
}

} // namespace ccut

namespace logger {

template<typename T>
struct exclude_default<ccut::buffer_view_t<T>> : std::true_type
{};

template<>
struct exclude_default<ccut::buffer_t> : std::true_type
{};

} // namespace logger

#endif