/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-15T11:35:20
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef COW_HPP__
#define COW_HPP__

#include <memory>
#include <type_traits>

#include <logger/Logger.hpp>

#include "utils.hpp" // convenience, to provide `make_const`

namespace ccut {

/**
 * @brief copy-on-write shared_ptr
 * @details de-referencing the object with non-const operartors will duplicate
 * the object, this object can be used in multi-threaded applications, making
 * the wrapped object thread-safe (with a non-blocking access type).
 * @details object must have a copy-constructor or override `Cow::copy` or
 * `Cow::detach`.
 * @details this object is really close to `std::shared_ptr` but it can't be
 * converted to it (thus the protected inheritance).
 */
template<typename T>
class Cow : protected std::shared_ptr<T>
{
public:
    using typename std::shared_ptr<T>::element_type;
    using std::shared_ptr<T>::use_count;
    using std::shared_ptr<T>::operator bool;
    using std::shared_ptr<T>::reset;
    using std::shared_ptr<T>::swap;

    Cow() : std::shared_ptr<T>{}, m_isCow{true} {}
    Cow(const Cow &other) : std::shared_ptr<T>{other}, m_isCow{true} {}
    explicit Cow(T *ptr) : std::shared_ptr<T>{ptr}, m_isCow{true} {}

    template<typename Deleter>
    Cow(T *ptr, Deleter d) : std::shared_ptr<T>{ptr, d}, m_isCow{true}
    {}

    bool operator==(const Cow<T> &other) const { return get() == other.get(); }
    bool operator!=(const Cow<T> &other) const { return get() != other.get(); }

    // cppcheck-suppress CastIntegerToAddressAtReturn
    const T *get() const noexcept { return std::shared_ptr<T>::get(); }
    T *get() noexcept
    {
        if (m_isCow)
            detach();
        // cppcheck-suppress CastIntegerToAddressAtReturn
        return std::shared_ptr<T>::get();
    }

    T *operator->()
    {
        if (m_isCow)
            detach();
        // cppcheck-suppress CastIntegerToAddressAtReturn
        return std::shared_ptr<T>::operator->();
    }

    // cppcheck-suppress CastIntegerToAddressAtReturn
    const T *operator->() const { return std::shared_ptr<T>::operator->(); }

    T &operator*()
    {
        if (m_isCow)
            detach();
        // cppcheck-suppress returnTempReference
        return std::shared_ptr<T>::operator*();
    }

    // cppcheck-suppress returnTempReference
    const T &operator*() const { return std::shared_ptr<T>::operator*(); }

    void detach()
    {
        if (!this->operator bool() || this->use_count() <= 1)
            return;
        this->reset(copy(*std::shared_ptr<T>::get()));
    }

    /**
     * @brief Enable/disable the COW feature
     * @details this applies only to this Cow object, it makes the whole chain
     * not thread-safe anymore (use with caution).
     */
    Cow &setCopyOnWrite(bool enable = true)
    {
        m_isCow = enable;
        return *this;
    }

    bool isCopyOnWrite() const { return m_isCow; }

protected:
    static T *copy(const T &t) { return new T(t); }

    bool m_isCow;
};

template<typename T>
using cow_ptr = Cow<T>;

/**
 * @brief construct a cow object, similar to `std::make_shared`
 */
template<typename T, typename... Args>
inline cow_ptr<T> make_cow(Args &&...args)
{
    return cow_ptr<T>(new T(args...));
}

template<typename T>
const logger::Logger &operator<<(const logger::Logger &logger,
                                 const ccut::cow_ptr<T> &ptr)
{
    if (logger.isEnabled())
    {
        if (ptr)
            return logger << *ptr;
        else
            return logger << "nullptr";
    }
    return logger;
}

} // namespace ccut

namespace logger {

template<typename T>
struct exclude_default<ccut::cow_ptr<T>> : std::true_type
{};

} // namespace logger

#endif