/*
** Copyright (C) 2012 Fargier Sylvain <fargier.sylvain@free.fr>
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Singleton.hpp
**
**        Created on: Nov 12, 2012
**   Original Author: Sylvain Fargier <fargier.sylvain@free.fr>
**
*/

#ifndef __SINGLETON_HPP__
#define __SINGLETON_HPP__

#include <memory>
#include <mutex>

namespace ccut {
/**
 * @addtogroup Threading
 * @{
 */

/**
 * @brief singleton creation mutex.
 *
 */
class SingletonMutex
{
public:
    SingletonMutex();
    ~SingletonMutex();

protected:
    static std::recursive_mutex m_lock;
};

enum class SingletonType
{
    standard, /**< legacy basic singleton */
    shared,   /**< shared pointer singleton */
    automatic /**< singleton is automatically destroyed if nobody uses it */
};

/**
 * @brief legacy implementation
 * @details once created singleton is always available
 * one may call `destroy()` after ensuring all instances have been destroyed
 */
template<class C, SingletonType T = SingletonType::standard>
class Singleton
{
public:
    static C &instance();

    static void destroy();

protected:
    static C *m_instance;
};

template<class C>
class Singleton<C, SingletonType::automatic>
{
public:
    typedef std::shared_ptr<C> Shared;

    static std::shared_ptr<C> instance();

protected:
    static std::weak_ptr<C> m_instance;
};

template<class C>
class Singleton<C, SingletonType::shared>
{
public:
    typedef std::shared_ptr<C> Shared;

    static std::shared_ptr<C> instance();

    static void destroy();

protected:
    static std::shared_ptr<C> m_instance;
};

/** @} */
} // namespace ccut

#endif // __SINGLETON_HPP__
