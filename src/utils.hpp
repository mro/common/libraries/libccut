/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-14T16:47:14+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#ifndef CCUT_UTILS_HPP__
#define CCUT_UTILS_HPP__

#include <algorithm>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>

namespace ccut {
/**
 * @addtogroup utils utils: various utilities
 * @{
 */

/**
 * @brief get current library version
 * @param[in] extraLong display only semvar or add scm related info
 * @param[in] log log version on console
 */
std::string ccutVersion(bool extraLong = true, bool log = true);

/**
 * @brief split string according to separator
 * @param  str string to split
 * @param  separator separator to look for
 * @param  out output vector
 * @return out argument
 */
std::vector<std::string> &split(const std::string &str,
                                char separator,
                                std::vector<std::string> &out);

/**
 * @brief split substring according to separator
 * @param  begin start of substring
 * @param  end end of substring (excluded)
 * @param  separator separator to look for
 * @param  out output vector
 * @return out argument
 */
std::vector<std::string> &split(const std::string::const_iterator &begin,
                                const std::string::const_iterator &end,
                                char separator,
                                std::vector<std::string> &out);

/**
 * @brief startsWith implementation
 * @details waiting for C++20
 * @param  str string to check
 * @param  prefix string to search for
 * @return true if str starts with prefix
 */
inline bool startsWith(const std::string &str, const std::string &prefix)
{
    return str.compare(0, prefix.size(), prefix) == 0;
}

/**
 * @brief trim heading and trailing blank characters
 * @details includes [ ' ', '\n', '\r', '\t' ]
 * @param  str string to trim
 * @return trimmed string
 */
std::string trim(const std::string &str);

/**
 * @brief url-encode the provided string
 *
 * @param[in] str the string to encode
 * @return url-encoded (or percent-encoded) std::string
 */
std::string urlencode(const std::string &str);

/**
 * @brief url-decode the provided string
 *
 * @param[in] str the string to decode
 * @return string with percent-encoding decoded
 */
std::string urldecode(const std::string &str);

/**
 * @brief flip a pair
 * @details mainly used as an unary-operator
 */
template<typename A>
typename std::enable_if<
    std::is_same<std::pair<typename A::first_type, typename A::second_type>,
                 A>::value,
    std::pair<typename A::second_type, typename A::first_type>>::type

    flip(const A &pair)
{
    return std::pair<typename A::second_type, typename A::first_type>(
        pair.second, pair.first);
}

/**
 * @brief flip a map
 * @details useful for introspection objects (to_string/from_string)
 * implementation.
 */
template<typename A>
typename std::enable_if<
    std::is_same<std::map<typename A::key_type, typename A::mapped_type>, A>::value,
    std::map<typename A::mapped_type, typename A::key_type>>::type

    flip(const A &map)
{
    std::map<typename A::mapped_type, typename A::key_type> ret;
    std::transform(map.cbegin(), map.cend(), std::inserter(ret, ret.begin()),
                   flip<typename A::value_type>);
    return ret;
}

/**
 * @brief flip an unordered_map
 * @details useful for introspection objects (to_string/from_string)
 * implementation.
 */
template<typename A>
typename std::enable_if<
    std::is_same<std::unordered_map<typename A::key_type, typename A::mapped_type>,
                 A>::value,
    std::unordered_map<typename A::mapped_type, typename A::key_type>>::type

    flip(const A &map)
{
    std::unordered_map<typename A::mapped_type, typename A::key_type> ret;
    std::transform(map.cbegin(), map.cend(), std::inserter(ret, ret.begin()),
                   flip<typename A::value_type>);
    return ret;
}

/**
 * @brief retrieve map keys in a set
 *
 * @tparam A a map type
 * @return std::set<keys>
 */
template<typename A>
typename std::enable_if<
    std::is_same<std::map<typename A::key_type, typename A::mapped_type>, A>::value ||
        std::is_same<
            std::unordered_map<typename A::key_type, typename A::mapped_type>,
            A>::value,
    std::set<typename A::key_type>>::type

    keys(const A &map)
{
    std::set<typename A::key_type> ret;
    std::transform(
        map.cbegin(), map.cend(), std::inserter(ret, ret.begin()),
        [](const typename A::value_type &value) { return value.first; });
    return ret;
}

/**
 * @brief convert an enum to its underlying type
 *
 * @tparam T enum type
 * @param t enum value to convert
 * @return constexpr std::underlying_type<T>::type
 */
template<typename T>
inline constexpr typename std::underlying_type<T>::type enum_cast(T t)
{
    return static_cast<typename std::underlying_type<T>::type>(t);
}

/**
 * @brief add const qualifier on object
 * @details useful with `ccut::cow_t` objects to ensure it won't be detached
 * @tparam T const or non-const object
 * @param c value to const cast
 * @return std::add_const<T>::type&
 */
template<typename T>
inline typename std::add_const<T>::type &make_const(T &c)
{
    return c;
}

/** @} */
} // namespace ccut

#endif
