/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#ifndef REGEX_HPP__
#define REGEX_HPP__

#include <functional>
#include <iterator>
#include <string>
#include <type_traits>
#include <vector>

#include <regex.h>

namespace ccut {

/**
 * @brief posix regex wrapper
 * @details useful with old libstdc++ that have buggy std::regex support
 */
class Regex
{
public:
    typedef std::vector<std::string> MatchGroup;

    /**
     * @param[in] re regex to compile
     * @param[in] flags
     * @throws std::invalid_parameter
     */
    explicit Regex(const std::string &re, int flags = Flags::Extended);
    ~Regex();
    Regex(const Regex &) = delete;
    Regex &operator=(const Regex &) = delete;

    /**
     * @brief execute regex
     * @param[out] matches matching parts
     * @details `MatchGroup's` capacity will be used to configure the match
     * count, MaxMatchDefault will be used by default
     * @return true if string matches
     */
    inline bool match(const std::string &str, MatchGroup &matches) const
    {
        size_t offset = 0;
        return match(str, offset, matches);
    }

    /**
     * @brief execute regex
     * @param[in] str string to run the regex on
     * @param[in,out] offset matching start offset in str
     * @param[out] matches matching parts
     * @return true if string matches
     *
     * @details for simple single match, do use the signature without `offset`
     * argument
     * @details
     *  - if `offset` is non-zero, the beginning-of-line (^) will not match,
     * - `offset` is set to the start of match in `str`
     * - (end of match is `offset + matches[0].size()`)
     */
    bool match(const std::string &str,
               size_t &offset,
               MatchGroup &matches) const;

    /**
     * @brief execute regex to test for match
     * @param[in] str string to run the regex on
     * @return true if string matches
     */
    bool search(const std::string &str) const;

    /**
     * @brief replace matches in string
     *
     * @param[in] str string to run the regex on
     * @param[in] fun replace function (`start` and `end` are str match offsets)
     * @return std::string the string with replacements
     * @example ```
     * Regex r("\\w+");
     * r.replace("this is a sentence", []() { return "blah"; });
     * // "blah blah blah blah"
     * ```
     */
    std::string replace(
        const std::string &str,
        std::function<std::string(size_t start, size_t end)> fun) const;

    /**
     * @brief convenience function (no arguments)
     */
    inline std::string replace(const std::string &str,
                               std::function<std::string()> fun) const
    {
        return replace(str,
                       [&fun](size_t, size_t) -> std::string { return fun(); });
    }

    /**
     * @brief convenience function (matching string argument)
     */
    inline std::string replace(
        const std::string &str,
        std::function<std::string(const std::string &sub)> fun) const
    {
        return replace(str,
                       [&str, &fun](size_t start, size_t end) -> std::string {
                           return fun(str.substr(start, end - start));
                       });
    }

    /**
     * @brief advanced replace function
     *
     * @param[in] str string to run the regex on
     * @param[in] fun replace function
     * @param[in] maxMatch maximum match count (to reserve array)
     * @return std::string the string with replacements
     * @example ```
     * Regex r("[$][{](\\w+)[}]|[$](\\w+)");
     * r.replace("echo ${HOME}", [](Regex::MatchGroup &matches) -> std::string {
     *    const char *value = getenv(matches[1].c_str());
     *    return value ? value : "";
     * }, 2);
     * ```
     */
    std::string replace(const std::string &str,
                        std::function<std::string(const MatchGroup &matches)> fun,
                        size_t maxMatch = MaxMatchDefault) const;

    enum Flags
    {
        Extended = REG_EXTENDED,
        ICase = REG_ICASE,
        NewLine = REG_NEWLINE
    };

    static constexpr std::size_t MaxMatchDefault = 32;

protected:
    regex_t m_regex;
};

} /* namespace ccut */

#endif
