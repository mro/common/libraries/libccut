/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-09-27T09:14:20
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef ENUM_HPP__
#define ENUM_HPP__

#include <type_traits>
#include <utility>

#include <logger/Logger.hpp>

/**
 * @brief type_traits to enable bitmask on enum types
 */
template<typename T>
struct ccut_enable_bitmask : std::false_type
{};

namespace ccut {

/**
 * @brief BitMask type for enums
 * @details to enable BitMask support on an `Enum` type, do add the following
 * declaration:
 * ```template<> struct ccut_enable_bitmask<Enum> : std::true_type {};```
 */
template<typename T>
class BitMask
{
public:
    using underlying_type = typename std::underlying_type<T>::type;

    constexpr BitMask() noexcept : m_bits{0} {}
    explicit constexpr BitMask(underlying_type value) noexcept : m_bits{value}
    {}

    // cppcheck-suppress noExplicitConstructor
    constexpr BitMask(const T &bit) noexcept :
        m_bits{static_cast<underlying_type>(bit)}
    {
        static_assert(ccut_enable_bitmask<T>::value, "BitMask not enabled");
    }

    constexpr underlying_type bits() const noexcept { return m_bits; }
    constexpr operator bool() const noexcept { return bits() ? true : false; }

    constexpr BitMask<T> operator&(const T &bit) const noexcept
    {
        return BitMask(m_bits & static_cast<underlying_type>(bit));
    }

    constexpr BitMask<T> operator|(const T &bit) const noexcept
    {
        return BitMask(m_bits | static_cast<underlying_type>(bit));
    }

    BitMask<T> &operator&=(const T &bit) noexcept
    {
        m_bits &= static_cast<underlying_type>(bit);
        return *this;
    }

    BitMask<T> &operator|=(const T &bit) noexcept
    {
        m_bits |= static_cast<underlying_type>(bit);
        return *this;
    }

    constexpr BitMask<T> operator&(const BitMask<T> &mask) const noexcept
    {
        return BitMask(m_bits & mask.bits());
    }

    constexpr BitMask<T> operator|(const BitMask<T> &mask) const noexcept
    {
        return BitMask(m_bits | mask.bits());
    }

    BitMask<T> &operator&=(const BitMask<T> &mask) noexcept
    {
        m_bits &= mask.bits();
        return *this;
    }

    BitMask<T> &operator|=(const BitMask<T> &mask) noexcept
    {
        m_bits |= mask.bits();
        return *this;
    }

    constexpr bool operator==(const BitMask<T> &mask) const noexcept
    {
        return m_bits == mask.bits();
    }

    constexpr bool operator!=(const BitMask<T> &mask) const noexcept
    {
        return m_bits != mask.bits();
    }

protected:
    underlying_type m_bits;
};

/**
 * @brief logger operator for BitMask<T>
 *
 * @param mask the mask to display
 * @details this logging operator requires enum value to have their own logging
 * operator.
 */
template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, const logger::Logger &>::type
    operator<<(const logger::Logger &logger, const ccut::BitMask<T> &mask);

} // namespace ccut

/**
 * @brief operator to transform enum to bitmask
 *
 * @tparam T enum type
 * @param v1 enum value1
 * @param v2 enum value2
 * @return ccut::BitMask<T>
 */
template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, ccut::BitMask<T>>::type
    operator|(const T &v1, const T &v2);

/**
 * @brief operator to transform enum to bitmask
 *
 * @tparam T enum type
 * @param v1 enum value1
 * @param v2 enum value2
 * @return ccut::BitMask<T>
 */
template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, ccut::BitMask<T>>::type
    operator&(const T &v1, const T &v2);

#include "BitMask.hxx"

#endif