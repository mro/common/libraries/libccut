/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T10:34:40
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef PIPE_HPP__
#define PIPE_HPP__

#include <string>

namespace ccut {

/**
 * @brief small wrapper on unix pipe objects
 * @details created pipe is always non-blocking
 *
 */
class Pipe
{
public:
    Pipe();
    ~Pipe();

    inline int in() const { return m_fd[0]; }
    inline int out() const { return m_fd[1]; }

    void write(const std::string &msg);

    /**
     * @brief write bytes of data
     *
     * @param[in] data data to write
     * @param[in] size data size
     * @throws ccut::Exception on read error
     */
    size_t write(const void *data, size_t size);

    /**
     * @brief read up to size bytes of data
     * @return the number of bytes read
     * @throws ccut::Exception on read error
     */
    size_t read(void *data, size_t size);

    /**
     * @brief flushes all buffered data
     */
    void flush();

protected:
    int m_fd[2];
};

} // namespace ccut

#endif