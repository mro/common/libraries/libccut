/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-09-27T10:04:30
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef BITMASK_HXX__
#define BITMASK_HXX__

#include "BitMask.hpp"

namespace ccut {

template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, const logger::Logger &>::type
operator<<(const logger::Logger &logger,
                                 const ccut::BitMask<T> &mask)
{
    if (logger.isEnabled())
    {
        bool first = true;
        logger << "(";
        for (size_t i = 0; i < sizeof(typename ccut::BitMask<T>::underlying_type) * 8;
             ++i)
        {
            typename ccut::BitMask<T>::underlying_type value(1 << i);
            if (mask.bits() & value)
            {
                if (!first)
                    logger << "|";
                logger << static_cast<T>(value);
                first = false;
            }
        }
        logger << ")";
    }
    return logger;
}

}

template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, ccut::BitMask<T>>::type operator|(
    const T &v1,
    const T &v2)
{
    return ccut::BitMask<T>(v1) | v2;
}

template<typename T>
typename std::enable_if<ccut_enable_bitmask<T>::value, ccut::BitMask<T>>::type operator&(
    const T &v1,
    const T &v2)
{
    return ccut::BitMask<T>(v1) & v2;
}

#endif