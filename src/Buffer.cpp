/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-16T11:47:05
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#include "Buffer.hpp"

namespace ccut {

#define GEN_ENDIAN_CONV(x)                            \
    template<>                                        \
    uint##x##_t htobe<uint##x##_t>(uint##x##_t value) \
    {                                                 \
        return htobe##x(value);                       \
    }                                                 \
    template<>                                        \
    uint##x##_t betoh<uint##x##_t>(uint##x##_t value) \
    {                                                 \
        return be##x##toh(value);                     \
    }                                                 \
    template<>                                        \
    uint##x##_t htole<uint##x##_t>(uint##x##_t value) \
    {                                                 \
        return htole##x(value);                       \
    }                                                 \
    template<>                                        \
    uint##x##_t letoh<uint##x##_t>(uint##x##_t value) \
    {                                                 \
        return le##x##toh(value);                     \
    }

GEN_ENDIAN_CONV(16)
GEN_ENDIAN_CONV(32)
GEN_ENDIAN_CONV(64)

#undef GEN_ENDIAN_CONV

template<>
uint8_t htobe<uint8_t>(uint8_t value)
{
    return value;
}

template<>
uint8_t htole<uint8_t>(uint8_t value)
{
    return value;
}

template<>
uint8_t letoh<uint8_t>(uint8_t value)
{
    return value;
}

template<>
uint8_t betoh<uint8_t>(uint8_t value)
{
    return value;
}

} // namespace ccut
