/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-20T16:42:00+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#ifndef SIGNAL_HPP__
#define SIGNAL_HPP__

#include <atomic>
#include <condition_variable>
#include <functional>
#include <list>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

namespace ccut {
/**
 * @addtogroup Signal Signal: signal/slot processing framework
 * @{
 */

class SignalData;
class Connection;

/**
 * @brief generic part of a listener
 * @details specialized part is defined in `Signal::Listener`
 */
struct BaseListener
{
    BaseListener();

    inline operator bool() const { return active; }
    std::atomic_bool active;
};

/**
 * @brief Signal class
 * @ingroup Signal
 */
template<typename... Args>
class Signal
{
public:
    Signal();

    /**
     * @details copy signals to duplicate its cache and optimize parallel signal
     * emission from multiple-threads.
     */
    Signal(const Signal &) = default;
    Signal &operator=(const Signal &) = default;

    /**
     * @brief emit the signal
     */
    void operator()(Args... args);

    /**
     * @brief connect a new method
     * @param  fun callback to connect
     * @return a connection object
     * @details signal is serviced until the Connection object is destroyed
     */
    Connection connect(std::function<void(Args...)> fun);

    /**
     * @brief connect a new method
     * @param[out] conn created connection pointer
     * @param[in]  fun
     * @details this method should be used if Connection is accessed from
     * signal handler
     */
    Connection &connect(Connection &conn, std::function<void(Args...)> fun);

    /**
     * @brief disconnects all listeners
     * @details called in signal's destructor
     * @details this method is re-entrant
     */
    void disconnect(bool wait = true);

    struct Listener : public BaseListener
    {
        explicit Listener(std::function<void(Args...)> fun);
        Listener(const Listener &other) = default;

        std::function<void(Args...)> call;
    };

protected:
    std::vector<std::weak_ptr<BaseListener>> m_cache;
    std::shared_ptr<SignalData> m_data;
};

/**
 * @brief signal connection class
 * @ingroup Signal
 */
class Connection
{
public:
    Connection();
    Connection(const Connection &) = delete;
    Connection &operator=(const Connection &) = delete;
#if __GNUC__ <= 4
    /* gcc 4.8.5 is buggy, won't swap the weak pointers */
    Connection(Connection &&other)
    {
        m_signal.swap(other.m_signal);
        m_listener.swap(other.m_listener);
    }

    Connection &operator=(Connection &&other)
    {
        if (&other != this)
        {
            disconnect();
            m_signal.swap(other.m_signal);
            m_listener.swap(other.m_listener);
        }
        return *this;
    }
#else
    Connection(Connection &&) = default;
    Connection &operator=(Connection &&) = default;
#endif

    ~Connection();

    /**
     * @brief swap connection with another
     */
    void swap(Connection &other);

    /**
     * @brief disconnect the signal
     * @param wait wait for any ongoing handlers to terminate
     * @details this method can be called from a handler (even with wait=true)
     * @details this method is re-entrant but only first caller can actually
     * wait (others will not wait).
     */
    void disconnect(bool wait = true);

    /**
     * @brief check if the associated signal is connected
     * @return true if notifications can be received
     */
    bool isConnected() const;

protected:
    template<typename... A>
    friend class Signal;
    Connection(const std::shared_ptr<SignalData> &sig,
               const std::shared_ptr<BaseListener> &listener);

    std::weak_ptr<SignalData> m_signal;
    std::weak_ptr<BaseListener> m_listener;
};

/** @} */
} // namespace ccut

#include "Signal.hxx"

#endif
