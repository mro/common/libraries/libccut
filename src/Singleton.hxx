/*
** Copyright (C) 2012 Fargier Sylvain <fargier.sylvain@free.fr>
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Singleton.hxx
**
**        Created on: Nov 12, 2012
**   Original Author: Sylvain Fargier <fargier.sylvain@free.fr>
**
*/

/**
 * @brief Template code Singleton class
 * @details this class is not automatically included since the template must be
 * instantiated with caution on windows like systems, see :
 * http://www.codesynthesis.com/~boris/blog/2010/01/18/dll-export-cxx-templates/
 */
#ifndef __SINGLETON_HXX__
#define __SINGLETON_HXX__

#include "Singleton.hpp"
#include <logger/Logger.hpp>

namespace ccut {

template<class C, SingletonType T>
C &Singleton<C, T>::instance()
{
    if (!m_instance)
    {
        SingletonMutex lock;
        if (!m_instance)
            m_instance = new C();
    }
    return *m_instance;
}

template<class C, SingletonType T>
void Singleton<C, T>::destroy()
{
    SingletonMutex lock;
    if (m_instance)
    {
        delete m_instance;
        m_instance = NULL;
    }
}

template<class C, SingletonType T>
C *Singleton<C, T>::m_instance = 0;

template<class C>
std::shared_ptr<C> Singleton<C, SingletonType::automatic>::instance()
{
    std::shared_ptr<C> ret = m_instance.lock();
    if (!ret)
    {
        SingletonMutex lock;
        ret = m_instance.lock();
        if (!ret)
        {
            ret.reset(new C());
            m_instance = ret;
        }
    }
    return ret;
}

template<class C>
std::weak_ptr<C> Singleton<C, SingletonType::automatic>::m_instance;

template<class C>
std::shared_ptr<C> Singleton<C, SingletonType::shared>::instance()
{
    std::shared_ptr<C> ret = m_instance;
    if (!ret)
    {
        SingletonMutex lock;
        if (!m_instance)
            m_instance.reset(new C());
        ret = m_instance;
    }
    return ret;
}

template<class C>
void Singleton<C, SingletonType::shared>::destroy()
{
    SingletonMutex lock;
    if (m_instance.use_count() > 1)
        logger::warning("ccut::singleton") << "destroying in-use singleton";
    m_instance.reset();
}

template<class C>
std::shared_ptr<C> Singleton<C, SingletonType::shared>::m_instance;

} // namespace ccut

#endif // __SINGLETON_HXX__
