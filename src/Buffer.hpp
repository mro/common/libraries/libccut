/*
** Copyright (C) 2023 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-12-14T18:45:41
** Author: Sylvain Fargier <fargier.sylvain@gmail.com>
*/

#ifndef SHAREDBUFFER_HPP__
#define SHAREDBUFFER_HPP__

#include <cstdint>
#include <vector>

#include <logger/Logger.hpp>

#include "Cow.hpp"
#include "utils.hpp"

namespace ccut {
/**
 * @addtogroup data data: data manipulation helpers
 * @{
 */

/**
 * @brief basic buffer with endianness aware primitives
 */
class Buffer : public std::vector<uint8_t>
{
public:
    using std::vector<uint8_t>::vector;

    template<typename T>
    Buffer &write(size_t offset, const T &value, bool be = false);

    template<typename T>
    T read(size_t offset, bool be = false) const;

    inline uint8_t read(size_t offset) const { return (data())[offset]; }
};

/** data-type typedef */
typedef Buffer buffer_t;

/**
 * @brief buffer-like object to access a sub-part of a buffer
 *
 * @tparam shared_buffer_t likely a cow_ptr<buffer_t>
 */
template<typename shared_buffer_t = cow_ptr<buffer_t>>
class BufferView
{
public:
    typedef shared_buffer_t shared_buffer_type;
    typedef typename shared_buffer_t::element_type::value_type value_type;
    typedef typename shared_buffer_t::element_type::iterator iterator;
    typedef
        typename shared_buffer_t::element_type::const_iterator const_iterator;

    BufferView(const shared_buffer_t &buffer, size_t offset, size_t len) :
        m_buffer(buffer),
        m_offset(offset),
        m_size(len)
    {}
    // cppcheck-suppress noExplicitConstructor
    BufferView(const shared_buffer_t &buffer, size_t offset = 0) :
        m_buffer(buffer),
        m_offset(offset),
        m_size(buffer ? (buffer->size() - offset) : 0)
    {}
    BufferView() : m_offset(0), m_size(0) {}
    BufferView(const BufferView &) = default;
    BufferView &operator=(const BufferView &) = default;
    BufferView &operator=(const shared_buffer_t &buffer)
    {
        if (buffer != m_buffer)
            m_buffer = buffer;
        m_offset = 0;
        m_size = buffer ? buffer->size() : 0;
        return *this;
    }

    inline void reset(const shared_buffer_t &buffer)
    {
        this->operator=(buffer);
    }

    inline void reset()
    {
        m_buffer.reset();
        m_offset = 0;
        m_size = 0;
    }

    operator bool() const
    {
        return bool(m_buffer) && (m_buffer->size() >= m_offset + m_size);
    }

    BufferView &setBuffer(const shared_buffer_t &buffer)
    {
        m_buffer = buffer;
        return *this;
    }

    inline size_t offset() const { return m_offset; }
    BufferView &setOffset(size_t offset)
    {
        if (offset >= m_offset + m_size)
            m_size = 0;
        else
            m_size -= offset;
        m_offset = offset;
        return *this;
    }

    inline size_t size() const { return m_size; }
    BufferView &setSize(size_t size)
    {
        m_size = size;
        return *this;
    }

    bool empty() const { return m_size == 0; }

    inline const value_type &operator[](size_t index) const
    {
        return (*m_buffer)[index + m_offset];
    }

    inline value_type &operator[](size_t index)
    {
        return (*m_buffer)[index + m_offset];
    }

    value_type *data() { return m_buffer->data() + m_offset; }
    value_type const *data() const { return m_buffer->data() + m_offset; }

    iterator begin() { return m_buffer->begin() + m_offset; }
    const_iterator begin() const { return m_buffer->cbegin() + m_offset; }
    const_iterator cbegin() const { return m_buffer->cbegin() + m_offset; }

    iterator end() { return m_buffer->begin() + m_offset + m_size; }
    const_iterator end() const
    {
        return m_buffer->cbegin() + m_offset + m_size;
    }
    const_iterator cend() const
    {
        return m_buffer->cbegin() + m_offset + m_size;
    }

    template<typename T>
    BufferView &write(size_t offset, const T &value, bool be = false)
    {
        m_buffer->template write<T>(offset + m_offset, value, be);
        return *this;
    }

    template<typename T>
    T read(size_t offset, bool be = false) const
    {
        return m_buffer->template read<T>(offset + m_offset, be);
    }

    inline uint8_t read(size_t offset) const { return read<uint8_t>(offset); }

    inline shared_buffer_t &buffer() { return m_buffer; }
    inline const shared_buffer_t &buffer() const { return m_buffer; }

    /**
     * @brief return sub-view from current view perspective
     *
     * @param offset new offset in current view
     * @param len length
     * @return BufferView<shared_buffer_t> new view object
     */
    BufferView sub(size_t offset, size_t len) const
    {
        return BufferView(buffer(), m_offset + offset, len);
    }

    BufferView sub(size_t offset) const
    {
        return BufferView(buffer(), m_offset + offset, m_size - offset);
    }

protected:
    shared_buffer_t m_buffer;
    size_t m_offset;
    size_t m_size;
};

template<typename shared_buffer_t = cow_ptr<buffer_t>>
using buffer_view_t = BufferView<shared_buffer_t>;

/**
 * @brief host to little-endian conversion
 *
 * @tparam T unsigned int 16, 32 or 64
 * @param[in] value value to convert
 * @return T converted value
 */
template<typename T>
T htole(T value);

/**
 * @brief host to big-endian conversion
 *
 * @tparam T unsigned int 16, 32 or 64
 * @param[in] value value to convert
 * @return T converted value
 */
template<typename T>
T htobe(T value);

/**
 * @brief little-endian to host conversion
 *
 * @tparam T unsigned int 16, 32 or 64
 * @param[in] value value to convert
 * @return T converted value
 */
template<typename T>
T letoh(T value);

/**
 * @brief big-endian to host conversion
 *
 * @tparam T unsigned int 16, 32 or 64
 * @param[in] value value to convert
 * @return T converted value
 */
template<typename T>
T betoh(T value);

/** @} */
} // namespace ccut

#include "Buffer.hxx"

#endif