/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include "Regex.hpp"

#include <stdexcept>

namespace ccut {

Regex::Regex(const std::string &re, int flags)
{
    int code = regcomp(&m_regex, re.c_str(), flags);
    if (code != 0)
    {
        size_t sz = regerror(code, &m_regex, nullptr, 0);
        std::vector<char> err(sz + 1);

        regerror(code, &m_regex, err.data(), err.size());
        throw std::invalid_argument(std::string("invalid regex: ") + err.data());
    }
}

Regex::~Regex()
{
    ::regfree(&m_regex);
}

bool Regex::match(const std::string &str,
                  size_t &offset,
                  MatchGroup &matches) const
{
    std::vector<regmatch_t> pmatch(matches.capacity() ? matches.capacity() :
                                                        Regex::MaxMatchDefault);
    matches.resize(0);
    if (regexec(&m_regex, str.c_str() + offset, pmatch.size(), pmatch.data(),
                offset ? REG_NOTBOL : 0) != 0)
        return false;

    for (const regmatch_t &m : pmatch)
    {
        if (m.rm_so >= 0 && m.rm_eo >= 0)
        {
            matches.push_back(str.substr(m.rm_so + offset, m.rm_eo - m.rm_so));
        }
    }
    offset = pmatch[0].rm_so + offset;
    return true;
}

std::string Regex::replace(
    const std::string &str,
    std::function<std::string(size_t start, size_t end)> fun) const
{
    regmatch_t matched;
    std::string res;
    res.reserve(str.size());

    for (size_t off = 0; off < str.size();)
    {
        if (regexec(&m_regex, str.c_str() + off, 1, &matched,
                    (off != 0) ? REG_NOTBOL : 0) == 0)
        {
            if (matched.rm_so != 0)
                res.insert(res.end(), str.begin() + off,
                           str.begin() + off + matched.rm_so);
            res += fun(matched.rm_so + off, matched.rm_eo + off);
            if (matched.rm_eo == 0) // empty match, consume a character
            {
                res += str[off];
                ++off;
            }
            else
                off += matched.rm_eo;
        }
        else
        {
            res.insert(res.end(), str.begin() + off, str.end());
            off = str.size();
        }
    }

    return res;
}

std::string Regex::replace(
    const std::string &str,
    std::function<std::string(const MatchGroup &matches)> fun,
    size_t maxMatch) const
{
    MatchGroup matches(maxMatch ? maxMatch : Regex::MaxMatchDefault);
    size_t prev = 0;
    std::string res;
    res.reserve(str.size());

    for (size_t offset = 0; offset < str.size(); prev = offset)
    {
        if (match(str, offset, matches))
        {
            if (offset != prev)
                res.insert(res.end(), str.begin() + prev, str.begin() + offset);
            res += fun(matches);
            if (matches[0].empty())
            {
                res += str[offset];
                ++offset;
            }
            else
                offset += matches[0].size();
        }
        else
        {
            res.insert(res.end(), str.begin() + offset, str.end());
            offset = str.size();
        }
    }

    return res;
}

bool Regex::search(const std::string &str) const
{
    return regexec(&m_regex, str.c_str(), 0, nullptr, 0) == 0;
}

} /* namespace ccut */
