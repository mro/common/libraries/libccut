/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef CCUT_MACROS_HPP
#define CCUT_MACROS_HPP

// TODO when switching to c++14, use [[deprecated]] instead
#ifndef DEPRECATED
#ifdef __GNUC__
#define DEPRECATED(MSG) __attribute__((deprecated(#MSG)))
#elif defined(_MSC_VER)
#define DEPRECATED(MSG) __declspec(deprecated(#MSG))
#else
#define DEPRECATED(MSG)
#endif
#endif

#define RVALUE_ONLY(T) \
    class = typename std::enable_if<!std::is_lvalue_reference<T>::value>::value

#endif // CCUTUTILS_MACROS_HPP
