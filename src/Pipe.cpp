/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-11-29T10:48:00
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "Pipe.hpp"

#include <vector>

#include <fcntl.h>
#include <unistd.h>

#include "Exception.hpp"

namespace ccut {

Pipe::Pipe()
{
    ::pipe(m_fd);
    for (size_t i = 0; i < 2; ++i)
    {
        ::fcntl(m_fd[i], F_SETFL, ::fcntl(m_fd[i], F_GETFL, 0) | O_NONBLOCK);
        ::fcntl(m_fd[i], F_SETFD, FD_CLOEXEC);
    }
}

Pipe::~Pipe()
{
    ::close(m_fd[0]);
    ::close(m_fd[1]);
}

void Pipe::flush()
{
    std::vector<char> buffer{10};
    while (read(buffer.data(), buffer.size()) == buffer.size())
        ;
}

size_t Pipe::write(const void *data, size_t size)
{
    ssize_t sz = ::write(m_fd[1], data, size);
    if (sz < 0)
        throw ccut::make_errno_exception();
    return size_t(sz);
}

size_t Pipe::read(void *data, size_t size)
{
    ssize_t sz = ::read(m_fd[0], data, size);
    if (sz < 0)
    {
        if (errno == EAGAIN)
            sz = 0;
        else
            throw ccut::make_errno_exception();
    }
    return size_t(sz);
}

void Pipe::write(const std::string &msg)
{
    if (write(msg.data(), msg.size()) != msg.size())
        throw Exception(ErrorCode::Runtime, "failed to write on pipe");
}

} // namespace ccut