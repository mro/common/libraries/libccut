/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "Exception.hpp"

#include <cerrno>
#include <cstring>
#include <sstream>
#include <vector>

namespace ccut {

const std::string Exception::Category = "CCUT";

Exception::Exception(std::error_code ev) : m_errorCode{ev} {}

Exception::Exception(std::error_code ev, const std::string &what) :
    m_errorCode{ev},
    m_what(what)
{}

const char *Exception::what() const noexcept
{
    if (m_what.empty())
        m_what = m_errorCode.message();
    return m_what.c_str();
}

struct ErrorCategory : std::error_category
{
    const char *name() const noexcept override;
    std::string message(int ev) const override;
};

const char *ErrorCategory::name() const noexcept
{
    return Exception::Category.c_str();
}

std::string ErrorCategory::message(int ev) const
{
    if (ev == 0)
        return "Ok";
    else if (ev < 1000)
        return ::strerror(ev);

    switch (static_cast<ErrorCode>(ev))
    {
    case ErrorCode::InternalError: return "Internal error";
    default: return "Unknown error";
    }
}

const ErrorCategory errorCategory{};

std::error_code make_error_code(ErrorCode e)
{
    return {static_cast<int>(e), errorCategory};
}

Exception make_errno_exception()
{
    std::vector<char> buf(256);
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
    ::strerror_r(errno, buf.data(), 256);
    const char *error = buf.data();
#else
    const char *error = ::strerror_r(errno, buf.data(), 256);
#endif

    return Exception(static_cast<ErrorCode>(errno), error);
}

} // namespace ccut
