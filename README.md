# libccut - Controls C++ Utilities Toolkit

Library with utilities for Controls

## Build

To build this project it is recommended to install [docker-builder](https://gitlab.cern.ch/mro/common/tools/x-builder) script.

To build the application:
```bash
docker-builder

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Documentation

Doxygen documentation is avialble [here](https://mro-dev.web.cern.ch/ci/mro/common/libraries/libccut/doc)

## Testing

To run unit-tests:
```bash
docker-builder ./build/tests/test_all
```

To run linters:
```bash
docker-builder make -C build lint
```

## Debugging

To compile this component for gdb/lldb:
```bash
docker-builder

cd build && cmake3 -DCMAKE_BUILD_TYPE=Debug ..
```
